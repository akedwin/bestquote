#coding: utf-8
import sys
from .paths import root
from django.contrib.messages import constants as message_constants
from FQ.conf import settings

# use redis as FQ backend engine
settings.TRANSPORT = 'redis'

# ensure that root exist
if not root() in sys.path:
    sys.path.append(root())

# Django settings for dip project.

DEBUG = True
TEMPLATE_DEBUG = DEBUG
THUMBNAIL_DEBUG = DEBUG

ADMINS = ()

MANAGERS = ADMINS

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': root('../dip-dev.sqlite'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

# Custom User model
AUTH_USER_MODEL = 'core.User'

# delate session at browser close
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

# cookies lifetime
SESSION_COOKIE_AGE = 1200 # 20 min

# custom serializer that support decimal in session storage
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Africa/Accra'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-GH'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = root('../media/')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = ''
STATICFILES_DIRS = (root('../static/'),)
# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'lnrksel*y1yekls^q+ra)m@rnfd9(@ye38*u!%5g(wfxiv0m!x'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)
# Folder where template is storage

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'dip.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'dip.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    root('../templates'),
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.messages.context_processors.messages',
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'dip.utils.context_processors.country',
)

# this section needs to be filled in country specific config file!!!
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.formtools',
    'django.contrib.humanize',
    'bootstrap_toolkit',
    'sorl.thumbnail',
    'django_tables2',
    'dip.admin',
    'dip.core',
    'dip.workers',
    'bootstrap3_datetime',
    'django_countries',
   # 'bootstrap3',
   # 'bootstrap3_datepicker',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

MESSAGE_TAGS = {
    message_constants.ERROR: 'danger',
}

LOGIN_URL = '/account/signin'
LOGOUT_URL = '/account/logout/'

#PHANTOMJS bin and dir location
PHANTOMJS_BIN = root('../bin/phantomjs')
PHANTOMJS_SCRIPT = root('utils/render_pdf.js')

# sorl settings
#THUMBNAIL_ENGINE = "sorl.thumbnail.engines.convert_engine.Engine"
THUMBNAIL_PREFIX = "CACHE/thumbs/"
THUMBNAIL_COLORSPACE = None
THUMBNAIL_FORMAT = 'PNG'
THUMBNAIL_DEBUG = True

# EMAIL SETTINGS SECTION
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'testnachwile@gmail.com'
EMAIL_HOST_PASSWORD = 'zse4xdr5cft6'
EMAIL_PORT = 587
DEFAULT_FROM_EMAIL = 'noreply@dip.ch'

# PAYALL WEB PAYMENT MODULE URL
PAYALL_WEB_PAYMENT = 'https://test.pay.ghana.payall.ch/new-payment'

# WEB PAYMENT MODULE CREDENTIALS
DIP_STORE_ID = 'ba9b56a6be334ed98e6ea65843a89fa0'
DIP_CLIENT_SALT = '9e3a27a9928b79654b5edc60e286e55e8689fdaa9bd0879c686b2c8a'
DIP_SERVER_SALT = '43ae9db09b52ec83bb2c57302e2e49ceb3e4850e3c383c8baa992e5c'

#MIGS CREDENTIALS
MIGS_ACCESS_CODE = '53378107'
migs_Merchant = 'GTB109153A01'
migs_Secret = '6A4497DDFFFD6BF8F1919C8C6C4181DB'
migs_Command = 'pay'
migs_Locale = 'en'
migs_Version = '1'



# COVER NOTE
COVER_NOTE_SALT = '3rygz4%&$@56GYy*%Y#&ty3%M*#reuvhge'

# PASSWORD VALIDATIONS RULES
PASSWORD_RULES = {
    'LENGTH': 8, # minimum length of password
    'NUMBERS': 1, # specify how many digits password must contain
    'UPPERCASE': 1, # specify how many uppercase password must contain
}

# If exist - overwrite defaut Django Localization (L10N)
FORMAT_MODULE_PATH = 'dip.formats'

# Salt used by hashids library
HASHIDS_SALT = "yux2tnWEKJG@R$t24i5xv28xr$!#q726e&^(5cv16"

# Alphabet used by hashids library for generating order_id
ALPHABET_UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"

# sendfile configuration
SENDFILE_BACKEND = 'sendfile.backends.nginx'
SENDFILE_ROOT = root('../for_download')
SENDFILE_URL = '/protected'
