# -*- coding: utf-8 -*-
from django import template
from django.core import urlresolvers


register = template.Library()


@register.simple_tag(takes_context=True)
def active(context, url_name, return_value=' class="active"', **kwargs):
    matches = active_url_equals(context, url_name, **kwargs)
    return return_value if matches else ''


def active_url_equals(context, url_name, **kwargs):
    path = context.get('request').path
    resolved = False
    while not resolved or resolved.url_name != url_name:
        try:
            resolved = urlresolvers.resolve(path)
        except:
            break
        else:
            path = path.rstrip('/').rsplit('/', 1)[0] + '/'
            if len(path) < 2:
                break
    matches = resolved and resolved.url_name == url_name
    if matches and kwargs:
        for key in kwargs:
            kwarg = kwargs.get(key)
            resolved_kwarg = resolved.kwargs.get(key)
            if not resolved_kwarg or kwarg != resolved_kwarg:
                return False
    return matches


@register.simple_tag(takes_context=True)
def breadcrumbs(context, path, **kwargs):
    parts = [c.split(',') for c in path.split('|')]
    parts.insert(0, ['Home', 'index'])
    return breadcrumbs_get_html(context, parts, **kwargs)


def breadcrumbs_get_html(context, parts, **kwargs):
    last = parts.pop()[0]
    breadcrumbs = []
    for part in parts:
        breadcrumbs.append('<a href="%s">%s</a> <i class="icon-caret-right"></i>' % (urlresolvers.reverse(part[1]), part[0]))
    breadcrumbs.append(last)
    return ''.join(breadcrumbs)
