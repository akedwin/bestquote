from django import template
register = template.Library()

@register.filter(name='addclass')
def addclass(field, cls):
    if field.field.widget.__class__.__name__ in ['RadioUnstyledSelect', 'RadioSelect']:
        return field
    else:
        return field.as_widget(attrs={
            'class': cls,
        })