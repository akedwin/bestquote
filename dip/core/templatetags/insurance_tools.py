#coding: utf-8
from __future__ import unicode_literals
from django import template
from dip.core.models.invoice import INSURANCE_TYPE_MAP, INVOICE_STATUSES_MAP
from dip.utils import YES_NO_MAP

register = template.Library()

@register.filter(name='category_name')
def category_name(value):
    """Converts a category id into a human readable name"""
    return INSURANCE_TYPE_MAP[value]


@register.filter(name='status_name')
def status_name(value):
    """Converts a status id into a human readable name"""
    return INVOICE_STATUSES_MAP[value]


@register.filter(name='percent')
def percent(value):
    """Converts float representation into percent"""
    return "{:.2%}".format(value)

@register.filter(name='yesnomap')
def yesnomap(value):
    """Converts (0, 1)[True/False] into YES/NO"""
    return YES_NO_MAP[value]