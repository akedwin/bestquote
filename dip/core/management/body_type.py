from django.conf import settings


# This tuple is used only by car_generator.
# Placed here to avoid listing in manage.py options list

CAR_BODY_TYPE = (
    'coupe',
    'crossover',
    'dual cowl',
    'fastback',
    'hardtop',
    'hatchback',
    'cabriolet',
    'combi',
    'combivan',
    'liftback',
    'limousine',
    'microvan',
    'minivan',
    'notchback',
    'pick-up',
    'roadster',
    'saloon' if settings.COUNTRY == 'ghana' else 'sedan',
    'suv',
    'targa',
    'van',
)

