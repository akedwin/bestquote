#coding: utf-8
from __future__ import unicode_literals

# This tuple is used only by car_generator.
# Placed here to avoid listing in manage.py options list

# List from http://en.wikipedia.org/wiki/List_of_current_automobile_manufacturers_by_country
MAKE_OF_VEHICLE = (
    # Argentina
    "Donto",

    # Australia
    "Amuza",
    "Bolwell",
    "Carbontech",
    "E-Vade",
    "Elfin",
    "Ford Australia",
    "JOSS",
    "Nota",

    # Austria
    "KTM",

    # Belgium
    "Gillet",
    "Imperia",

    # Brazil
    "BRM Buggy",
    "Lobini",
    "Rossin-Bertin",
    "Troller",

    # Canada
    "HTT Technologies",

    # Czech Republic
    "Kaipan",
    "Škoda",

    # China
    "Beijing Automobile Works",
    "Brilliance / HuaChen",
    "BYD",
    "Chang'an",
    "Changhe",
    "Chery",
    "Dongfeng",
    "Emgrand",
    "Englon",
    "FAW",
    "Foday",
    "Geely",
    "Gleagle",
    "Gonow",
    "Great Wall / Changcheng",
    "Hafei",
    "Hongqi",
    "JAC",
    "Jonway",
    "Landwind",
    "Lifan",
    "Mycar",
    "Polarsun",
    "Roewe",
    "SAIC-GM-Wuling",
    "Shanghai Maple",
    "Trumpchi",
    "Wheego",
    "ZX Auto",

    # Croatia
    "Rimac",

    # Denmark
    "Zenvo",

    # Egypt
    "Egy-Tech Engineering",

    # Finland
    "Electric Raceabout",

    # France
    "Bugatti",
    "Citroën",
    "De La Chapelle",
    "Exagon",
    "Peugeot",
    "P.G.O.",
    "Renault",

    # Germany

    "Artega",
    "Audi",
    "BMW",
    "Ford Germany",
    "Gumpert",
    "Isdera",
    "Jetcar",
    "Lotec",
    "Mercedes-Benz",
    "MINI",
    "Opel",
    "Porsche",
    "Roding",
    "Smart",
    "Volkswagen",
    "Wiesmann",
    "YES!",

    # Ghana
    "Kantanka",

    # India
    "Bajaj",
    "Force",
    "Hindustan",
    "ICML",
    "Mahindra",
    "REVA",
    "San",
    "Tata",

    # Indonesia
    "Kiat",
    "Tawon",

    # Iran
    "Iran Khodro/IKCO",
    "Kish Khodro",
    "SAIPA",

    # Italy
    "Alfa Romeo",
    "B Engineering",
    "Bertone",
    "Covini",
    "Effedi",
    "Faralli & Mazzanti",
    "Ferrari",
    "Fiat",
    "Fornasari",
    "Lamborghini",
    "Lancia",
    "Maserati",
    "Pagani",
    "Puritalia",

    # Japan
    "Acura",
    "ASL",
    "Daihatsu",
    "Honda",
    "Infiniti",
    "Isuzu",
    "Lexus",
    "Mazda",
    "Mitsubishi",
    "Mitsuoka",
    "Nissan",
    "Subaru",
    "Suzuki",
    "Toyota",

    # Lebanon
    "W Motors",

    # Liechtenstein
    "Orca",

    # Malaysia
    "Bufori",
    "Perodua",
    "Proton",
    "TD Cars",
    # Mexico
    "Mastretta",
    # Monaco
    "Venturi",

    # The Netherlands
    "Donkervoort",
    "Spyker",
    "Vencer",

    # New Zealand
    "Chevron",
    "Hulme",
    "Saker",

    # Norway
    "Buddy",

    # Poland
    "Arrinera",
    "Leopard",

    # Portugal
    "Asterio",

    # Romania
    "Dacia",

    # Russia
    "Lada",
    "Marussia",
    "ZiL",

    # Slovenia
    "Tushek",

    # South Africa
    "Optimal Energy",
    "Perana",
    "Uri",

    # South Korea
    "Hyundai",
    "Kia",
    "Proto / Oullim",
    "Samsung",
    "Ssangyong",

    # Spain
    "A.D. Tramontana",
    "GTA Motor",
    "IFR",
    "SEAT",
    "Tauro",

    # Sri Lanka
    "Micro",

    # Sweden
    "Koenigsegg",
    "Saab",
    "Volvo",

    # Switzerland
    "LeBlanc",
    "Rinspeed",
    "Sbarro",

    # Taiwan
    "Luxgen",

    #Tunisia
    "Wallyscar",

    # Turkey
    "Diardi",
    "Etox",

    # Ukraine
    "VEPR",

    # United Kingdom
    "AC",
    "Arash",
    "Ariel",
    "Ascari",
    "Aston Martin",
    "BAC",
    "Bentley",
    "Bristol",
    "Brooke",
    "Caparo",
    "Connaught",
    "Eterniti",
    "Gibbs",
    "Ginetta",
    "Grinnall",
    "Invicta",
    "Jaguar",
    "Jeep",
    "Land Rover",
    "Lotus Cars",
    "McLaren",
    "MG",
    "Morgan",
    "Noble",
    "Radical",
    "Rolls-Royce",
    "Trident",
    "TVR",
    "Ultima",
    "Westfield",

    # United States
    "AC Propulsion",
    "Anteros",
    "Buick",
    "Cadillac",
    "Chevrolet",
    "Chrysler",
    "Commuter Cars",
    "DMC",
    "Devon",
    "Dodge",
    "Fisker",
    "Ford",
    "GMC",
    "Lincoln",
    "Lucra Cars",
    "Panoz",
    "Rossion",
    "Shelby SuperCars",
    "Tanom",
    "Tesla",
    "Vector",
)