from django.core.management.base import NoArgsCommand
from django.core.management.base import CommandError
from django.conf import settings

from dip.core.models import Invoice
from dip.utils.common import hash_decode

from glob import glob
import os


class Command(NoArgsCommand):
    """
    This command is intended to be run by cron once a day at 1.00 am,
    to remove all expired cover note pdfs from server hdd.
    """
    help = 'Remove all cover note pdfs that expired.'

    def handle_noargs(self, **options):
        files = glob(settings.SENDFILE_ROOT+'/*.pdf')
        invoices_map = {}
        for _file in files:
            # split by "/", get last item, remove extension;
            order_id = _file.split('/')[-1].rstrip('.pdf')
            # decode hash into invoice pk, map pk and location of covernote.
            _pk = hash_decode(order_id, min_length=10, alphabet=settings.ALPHABET_UPPER)
            # in case that dir contains other pdfs where its name is not a valid hash
            try:
                invoices_map[_pk[0]] = _file
            except IndexError:
                pass
        # get invoices from db to check if they expired
        qset = Invoice.objects.filter(pk__in=invoices_map.keys())
        for item in qset:
            if item.quote_expired():
                path = invoices_map[item.pk]
                os.remove(path)
                print('%s expired! Removing cover note (%s)!' % (item.order_id, path))



                    