from optparse import make_option
from importlib import import_module
from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.db import transaction
from django.conf import settings
from dip.core.management.body_type import CAR_BODY_TYPE
from dip.core.management.make_of_vehicle import MAKE_OF_VEHICLE


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('-b',
            dest='body_type_model',
            help='Fulfil database with car body type names using model name provided.'
        ),
        make_option('-m',
            dest='make_of_vehicle_model',
            help='Fulfil database with make of vehicle names using model name provided.',
        )
    )
    help = 'Fulfill database with car body types or make of vehicles.'

    def handle(self, **options):
        if options['body_type_model']:
            self.body_type(settings.COUNTRY, model_name=options['body_type_model'])
        if options['make_of_vehicle_model']:
            self.make_of_vehicle(settings.COUNTRY, model_name=options['make_of_vehicle_model'])

    @transaction.atomic()
    def body_type(self, country, model_name=None):
        try:
            models = import_module('dip.{}.models'.format(country))
            model = getattr(models, model_name)
        except AttributeError:
            raise CommandError('{} does not have "{}" model'.format(country, model_name))
        for cbt in CAR_BODY_TYPE:
            obj, created = model.objects.get_or_create(name=cbt)
            if created:
                print('{}: created {}'.format(model.__name__, obj))
            else:
                print('{}: {} already exists'.format(model.__name__, obj))


    @transaction.atomic()
    def make_of_vehicle(self, country, model_name=None):
        try:
            models = import_module('dip.{}.models'.format(country))
            model = getattr(models, model_name)
        except AttributeError:
            raise CommandError('{} app not have "{}" model'.format(country, model_name))
        for mov in MAKE_OF_VEHICLE:
            obj, created = model.objects.get_or_create(name=mov)
            if created:
                print('{}: created {}'.format(model.__name__, obj))
            else:
                print('{}: {} already exists'.format(model.__name__, obj))