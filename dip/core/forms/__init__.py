from .user import RegisterForm
from .user import EditPersonalDataForm
from .user import AccountActivationForm