#coding: utf-8
from django import forms
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from importlib import import_module
from dip.core.models import User
from dip.utils.forms_tools import PasswordStrengthValidator


class RegisterForm(forms.Form):
    firstname = forms.CharField(max_length=30)
    lastname = forms.CharField(max_length=30)
    email1 = forms.EmailField()
    email2 = forms.EmailField()
    password1 = forms.CharField(
        max_length=30,
        min_length=settings.PASSWORD_RULES.get('LENGTH', 1),
        validators=[PasswordStrengthValidator()],
        widget=forms.TextInput(attrs={'type': 'password'}),
    )
    password2 = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'type': 'password'}))

    def clean(self):
        data = super(RegisterForm, self).clean()
        email = data.get('email1', '')
        cemail = data.get('email2', '')
        password = data.get('password1', '')
        cpassword = data.get('password2', '')
        try:
            User.objects.get(email=email)
        except ObjectDoesNotExist:
            pass
        else:
            raise forms.ValidationError('This email address is already registered')
        if email != cemail:
            raise forms.ValidationError("Email address and confirmation doesn't match")
        if password != cpassword:
            raise forms.ValidationError("Password and confirmation doesn't match")
        return data


# dip system required some 'static' elements in country modules,
# one of this static element is a Customer form that should be used
# in every WizardForm in every country!
CountryForms = import_module('dip.%s.forms' % settings.COUNTRY)

class EditPersonalDataForm(CountryForms.Customer):
    def __init__(self, *args, **kwargs):
        super(EditPersonalDataForm, self).__init__(*args, **kwargs)
        # email address is not editable because it is used as username!
        if self.fields.get('email', None):
            del self.fields['email']


class AccountActivationForm(forms.Form):
    email = forms.EmailField(label="Email", max_length=254)