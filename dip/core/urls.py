#coding: utf-8
from django.conf.urls import patterns, url, include
from dip.utils.forms_tools import CustomSetPasswordForm


layout_pattern = patterns('dip.core.views',
    url(r'^$', 'home', name='index'),
    url(r'^about$', 'about', name='about_page'),
    url(r'^faq$', 'faq', name='faq_page'),
    url(r'^tou$', 'tou', name='tou_page'),
    url(r'^success$', 'success', name='sucess_page'),
    url(r'^contact$', 'contact', name='contact_page'),
    url(r'^account/$', 'account', name='account'),
    url(r'^account/signin/$', 'signin', name='signin'),
    url(r'^account/register/$', 'register', name='register'),
    url(r'^account/logout/$', 'logout_user', name='logout'),
    url(r'^account/profile/$', 'profile', name='profile'),
    url(r'^account/profile/edit/$', 'profile_edit', name='edit_profile'),
    url(r'^account/activate/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', 'activate_account', name='activate_acc'),
    url(r'^account/activate/send/$', 'send_activation_mail', name='activation_mail'),
    url(r'^reflink/(?P<token>[0-9A-Za-z]{15})$', 'reflink', name='reflink'),
)


password_management_pattern = patterns('',
    url(r'^account/password/reset/$', 'django.contrib.auth.views.password_reset', {
        'post_reset_redirect': '/account/password/reset/done/',
        'template_name': 'account/password_reset.html',
        'email_template_name': 'account/password_reset_email.html',
        },
        name='forgotpassword'),
    url(r'^account/password/reset/done/$', 'django.contrib.auth.views.password_reset_done', {
        'template_name': 'account/password_reset_done.html'
        }),
    url(r'^account/password/reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm', {
        'post_reset_redirect': '/account/password/done/',
        'template_name': 'account/password_reset_confirm.html',
        'set_password_form': CustomSetPasswordForm,
        }),
    url(r'^account/password/done/$', 'django.contrib.auth.views.password_reset_complete', {
        'template_name': 'account/password_reset_complete.html'
        }),
    url(r'^account/password/change/$', 'django.contrib.auth.views.password_change', {
        'template_name': 'account/profile/password_change.html',
        'post_change_redirect': '/account/password/change/done/',
        },
        name='change_password'),
    url(r'^account/password/change/done/$', 'django.contrib.auth.views.password_change_done', {
        'template_name': 'account/profile/profile.html',
        'extra_context': {'data_change': 'Password change successful!'}
        }),
)


invoice_pattern = patterns('dip.core.views',
    url(r'^confirm_payment/$', 'change_invoice_status'),
    url(r'^quote/details/(?P<order_id>[A-Z0-9]{10})$', 'invoice_details', name='quote_details'),
)


insurance_pattern = patterns('dip.core.views',
    url(r'^account/insurances/$', 'insurances', name='insurances'),
    url(r'^account/proposals/$', 'proposals', name='proposals'),
)


urlpatterns = patterns('',
    url(r'^', include(layout_pattern)),
    url(r'^', include(password_management_pattern)),
    url(r'^', include(invoice_pattern)),
    url(r'^', include(insurance_pattern)),
)
