#coding: utf-8
from importlib import import_module

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth import logout
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.tokens import default_token_generator
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.forms.models import model_to_dict
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.cache import never_cache
from django.utils.http import urlsafe_base64_decode
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404

from dip import __version__
from dip.core.models import User
from dip.core.models import Company
from dip.core.forms import RegisterForm
from dip.core.forms import EditPersonalDataForm
from dip.core.forms import AccountActivationForm
from dip.utils import no_admin_allowed
from dip.utils.forms_tools import CustomSetPasswordForm
from dip.utils.common import hash_decode

import FQ


def home(req):
    """
    Show home page
    """
    cc = RequestContext(req)
    if req.user.is_staff:
        # if user has admin rights we should redirect him to admin section, not index page
        return redirect('admin_page')
    return render_to_response("index.html", cc)


def about(req):
    """
    Show info about DIP and actual version
    """
    cc = RequestContext(req)
    cc['version'] = __version__
    return render_to_response("about.html", cc)


def contact(req):
    """
    Show contact data
    """
    cc = RequestContext(req)
    return render_to_response("contact.html", cc)

def faq(req):
    """
    Show frequently asked questions
    """
    cc = RequestContext(req)
    return render_to_response("faq.html", cc)

def tou(req):
    """
    Show terms of use data
    """
    cc = RequestContext(req)
    return render_to_response("tou.html", cc)

def success(req):
    """
    Show transaction success page
    """
    cc = RequestContext(req)
    return render_to_response("success.html", cc)


def signin(req):
    """
    Login page
    """
    cc = RequestContext(req)
    cc['username'] = ''

    if req.method == 'POST':
        user = req.POST.get('username')
        password = req.POST.get('password')
        signin_type = req.POST.get('signin_type')

        # if user want register new acc, redirect to apropriate url (save putted email in session)
        if signin_type == 'registration':
            req.session['first_step_email'] = user
            return redirect('register')
        # if user want to login, validate the username and pass
        customer = authenticate(username=user, password=password)
        if customer is not None:
            if customer.is_active:
                login(req, customer)
                if customer.is_staff:
                    return redirect('admin_page')
                return redirect('account')
            else:
                messages.error(req, 'Account is disabled')
        else:
            messages.error(req, 'Wrong email address or password. Please try again.')
        cc['username'] = user

    return render_to_response("account/signin.html", cc)


def register(req):
    """
    Create new user account
    """
    cc = RequestContext(req)

    if req.method == 'POST':
        form = RegisterForm(req.POST)
        if form.is_valid():
            data = form.cleaned_data
            email = data['email1']
            password = data['password1']
            name = data['firstname']
            last = data['lastname']
            # create user in database if form is valid
            user = User.objects.create_user(
                email=email,
                password=password,
                first_name=name,
                last_name=last
            )
            # create user blank profile!!!
            country_user_model = import_module('dip.%s.models' % settings.COUNTRY)
            user_info = getattr(country_user_model, 'CustomerInfo')()
            user_info.user = user
            user_info.save()

            # delate email gather in first step from session
            del req.session['first_step_email']
            # login user before redirect
            customer = authenticate(username=email,password=password)
            login(req, customer)
            return redirect('account')
    else:
        # put inital email address from first step (signin)
        form = RegisterForm(initial={'email1': req.session.get('first_step_email', None)})

    cc['form'] = form
    return render_to_response("account/register.html", cc)


@login_required
@no_admin_allowed
def account(req):
    return redirect('insurances')


def logout_user(req):
    logout(req)
    messages.success(req, 'You have been successfully logged out.')
    return redirect(signin)


@login_required
@no_admin_allowed
def profile(req):
    cc = RequestContext(req)
    cc['user'] = req.user
    if req.session.get('data_change'):
        cc['data_change'] = req.session.get('data_change')
        del req.session['data_change']
    return render_to_response("account/profile/profile.html", cc)


@login_required
@no_admin_allowed
def profile_edit(req):
    cc = RequestContext(req)
    # this needst to be here since it is used in post or get method!
    user_info = req.user.profile

    if req.method == 'POST':
        form = EditPersonalDataForm(req.POST)
        if form.is_valid():
            data = form.cleaned_data
            user = req.user
            user.first_name = data.get('first_name')
            user.last_name = data.get('last_name')
            user.save()
            del data['first_name'], data['last_name']
            for field in data.iterkeys():
                setattr(user_info, field,  data[field])
            user_info.save()
            req.session['data_change'] = 'Personal data change successful!'
            return redirect('profile')
    else:
        # get initial data for profile based on existing info in model
        initail_data = {}
        for key, value in model_to_dict(req.user, fields=['first_name', 'last_name']).iteritems():
            initail_data.update({key: value})
        for key, value in model_to_dict(user_info, exclude=['id', 'user']).iteritems():
            initail_data.update({key: value})
        # fulfil the form with initial info base on existing profile
        form = EditPersonalDataForm(initial=initail_data)

    cc['form'] = form
    return render_to_response("account/profile/edit_profile.html", cc)


@sensitive_post_parameters()
@never_cache
def activate_account(req, uidb64=None, token=None):
    cc = RequestContext(req)

    assert uidb64 is not None and token is not None  # checked by URLconf
    try:
        uid = urlsafe_base64_decode(uidb64)
        user = User._default_manager.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and default_token_generator.check_token(user, token):
        validlink = True
        if req.method == 'POST':
            form = CustomSetPasswordForm(user, req.POST)
            if form.is_valid():
                form.save()
                user.is_active = True
                user.save()
                return redirect('/account/password/done/')
        else:
            messages.info(req, 'To activate your account, first you need to setup your password')
            form = CustomSetPasswordForm(None)
    else:
        validlink = False
        form = None

    cc['validlink'] = validlink
    cc['form'] = form

    return render_to_response('account/password_reset_confirm.html', cc)


def send_activation_mail(req):
    cc = RequestContext(req)
    if req.method == 'POST':
        form = AccountActivationForm(req.POST)
        if form.is_valid():
            messages.info(req, 'Please check your Email and proceed with the instructions.')
            email = form.cleaned_data['email']
            try:
                user = User.objects.get(email=email, is_active=False)
                FQ.async.mail.send_mail(
                    'activation', 
                    email=user.email, 
                    host=req.get_host(), 
                    url=user.get_activation_url(req)
                ),
            except ObjectDoesNotExist:
                pass
    else:
        form = AccountActivationForm()
    cc['form'] = form
    return render_to_response('account/profile/activation_link.html', cc)


@no_admin_allowed
def reflink(req, token):
    """
    If customer enter to site using reflink, all his choices will be limited 
    to company described in token. This is valid only per one session.
    """
    company_pk = hash_decode(token, min_length=15)[0]
    # validate if token describe valid company.
    company = get_object_or_404(Company, pk=company_pk)
    # save reflink in user session.
    req.session['reflink'] = company.pk
    return redirect('/')