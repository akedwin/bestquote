# coding: utf-8
from __future__ import unicode_literals

from django.conf import settings
from django.http import HttpResponse
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt


from dip.core.models import Invoice
from dip.core.models.invoice import INSURANCE_TYPE_MAP
from dip.utils import checksum
from dip.utils import no_admin_allowed
from dip.utils.common import hash_decode
from datetime import datetime, timedelta

import FQ
from uuid import uuid4
from sendfile import sendfile



@csrf_exempt
def change_invoice_status(req):
    """
    Simple api-like view to change status of invoice.
    Required proper order-id and amount and of course checksum.
    Also perform validation of host and post structure request.
    If everything is ok, sent also the cover note to user assigned to invoice.
    """

    if req.method == 'POST':
        post = req.POST
        for field in ('order-id', 'amount', 'checksum', 'title', 'salt', 'currency', 'status', 'txn-id'):
            param = post.get(field)
            if not param:
                print 'No param'
                raise Http404

        _order = post.get('order-id')
        _amount = post.get('amount').replace(',', '')
        _pk = hash_decode(_order, min_length=10, alphabet=settings.ALPHABET_UPPER)[0]
        invoice = get_object_or_404(Invoice, pk=_pk, amount=_amount)

        _checksum = post.get('checksum')
        _params = {
            'title': post.get('title'),
            'order-id': _order,
            'amount': _amount,
            'curr': post.get('currency'),
            'salt': post.get('salt'),
        }

        if _checksum != checksum(_params, settings.DIP_SERVER_SALT, invoice=True):
            print 'wrong checksum'
            raise Http404

        _status = post.get('status')

        #get Payall internal tranzaction ID
        _txn = post.get('txn-id')

        invoice.status = _status
        invoice.txn_id = _txn
        invoice.expiration = datetime.now()+timedelta(days=14)
        invoice.save()

        if int(_status) == 2:
            user = invoice.customer
            FQ.async.mail.send_covernote(
                host=req.get_host(),
                email=user.email, 
                order_id=str(invoice.order_id), 
                amount=str(invoice.amount.amount),
            )
        return HttpResponse('OK')

    #if no post give 404
    print 'No POST'
    raise Http404


@login_required
@no_admin_allowed
def insurances(req):
    cc = RequestContext(req)
    cc['customer'] = req.user.profile
    return render_to_response('account/insurances/insurances.html', cc)


@login_required
@no_admin_allowed
def proposals(req):
    cc = RequestContext(req)
    cc['customer'] = req.user.profile
    return render_to_response('account/insurances/proposals.html', cc)


@login_required
def invoice_details(req, order_id):
    """
    Shared view for users and admins to see the quote details.
    """
    _pk = hash_decode(order_id, min_length=10, alphabet=settings.ALPHABET_UPPER)[0]
    invoice = get_object_or_404(Invoice, pk=_pk)
    if not invoice.can_view(req.user):
        raise Http404

    # download cover note if requested
    if req.GET.get('download', False):
        return sendfile(req, '%s/%s.pdf' % (settings.SENDFILE_ROOT, order_id))
        
    cc = RequestContext(req)
    
    base = "account/base.html"
    if req.user.is_staff:
        base = "admin/base.html"

    if req.user.has_perm('admin.company_admin') and not req.user.is_superuser:
        cc['company_name'] = req.user.company_admin.get().company.short_name

    cc['base'] = base
    cc['salt'] = uuid4().hex
    cc['invoice'] = invoice
    cc['store_id'] = settings.DIP_STORE_ID
    cc['section'] = INSURANCE_TYPE_MAP[invoice.category]
    cc['payall_payment'] = settings.PAYALL_WEB_PAYMENT
    cc['title'] = 'BestQuote - Invoice %s' % invoice.order_id

    checksum_content = {
        'order-id': invoice.order_id,
        'amount': invoice.amount.amount,
        'curr': invoice.amount.currency,
        'title': cc['title'],
        'salt': cc['salt']
    }

    cc['checksum'] = checksum(checksum_content, settings.DIP_CLIENT_SALT, invoice=True)
    return render_to_response('common/quote_details.html', cc)
