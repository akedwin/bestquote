# coding: utf-8
from django.db import models


class CompanyAddressAbstract(models.Model):

    class Meta:
        abstract = True
        verbose_name_plural = 'addresses'

    company = models.OneToOneField('core.Company', related_name='company_address')
    address = models.TextField(blank=True)
    email = models.EmailField(max_length=255, blank=True)
    fax = models.CharField(max_length=255, blank=True)
    telephone = models.CharField(max_length=255, blank=True)
    website = models.URLField(max_length=255, blank=True)

    def __unicode__(self):
        return self.company.name