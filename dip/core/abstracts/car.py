from django.db import models


class CarBodyTypeAbstract(models.Model):
    class Meta:
        abstract = True

    name = models.CharField(max_length=20, verbose_name='Type of Body')

    def __unicode__(self):
        return self.name


class MakeOfVehicleAbstract(models.Model):
    class Meta:
        abstract = True

    name = models.CharField(max_length=25, verbose_name='Make of Motor Vehicle')

    def __unicode__(self):
        return self.name