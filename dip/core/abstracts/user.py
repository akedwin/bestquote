#coding: utf-8
from django.db import models

class CustomerAbstract(models.Model):
    class Meta:
        abstract = True

    #user = models.OneToOneField('core.User', related_name='profile')
    #create_user = models.ForeignKey(User, related_name='%(class)s_requests_created')
    user = models.OneToOneField('core.User', related_name='%(app_label)s_%(class)s_related')

    @property
    def invoice_order_id(self):
        invoice = self.customer_invoice.get(customer=self.pk)
        return invoice.order_id

    @property
    def confirmed_insurances(self):
        insurances = self.user.invoice.filter(status__gte=2)
        for item in insurances:
            yield item

    @property
    def proposals(self):
        position = self.user.invoice.filter(status__lt=2)
        for item in position:
            yield item


    def data_with_empty_val(self):
        for name, value in self.customer_data:
            if value or value == 0:
                yield (name, value)
            else:
                yield (name, '-')

    def customer_data(self):
        # customer data needs to be implemented in inherit class
        # in country specific customer model!!!
        raise NotImplementedError('Subclasses must implement this method.')
