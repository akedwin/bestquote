from .user import CustomerAbstract

from .car import CarBodyTypeAbstract
from .car import MakeOfVehicleAbstract

from .company import CompanyAddressAbstract