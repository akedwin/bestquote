from django.db import models
from sorl.thumbnail.fields import ImageField


class Company(models.Model):

    class Meta:
        app_label = 'core'
        verbose_name_plural = 'companies'

    name = models.CharField(max_length=100, verbose_name='Company Name')
    short_name = models.CharField(max_length=15, verbose_name='Short Name')
    logo = ImageField(upload_to='companies_logos/', default='companies_logos/logo_placeholder.png', verbose_name='Company logo')

    def __unicode__(self):
        return self.name

    @property
    def address_data(self):
        try:
            return self.company_address
        except AttributeError:
            return None