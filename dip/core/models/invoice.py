#coding: utf-8
from __future__ import unicode_literals
from django.db import models
from django.conf import settings
from django.utils import timezone
from djmoney.models.fields import MoneyField
from dip.utils.common import hash_encode


INVOICE_STATUSES_MAP = {
    0: 'Registered',
    1: 'Paid',
    2: 'Confirmed',
    3: 'Settled',
}

INSURANCE_TYPE_MAP = {
    1: 'Motor',
    2: 'Travel',
    3: 'Estate',
    4: 'Liability',
    5: 'Life',
}

class Invoice(models.Model):

    class Meta:
        app_label = 'core'

    customer = models.ForeignKey('core.User', related_name='invoice')
    company = models.ForeignKey('core.Company')
    time = models.DateTimeField(auto_now_add=True)
    amount = MoneyField(max_digits=10, decimal_places=2, default_currency=settings.CURRENCY)
    status = models.IntegerField(choices=INVOICE_STATUSES_MAP.iteritems(), default=0)
    txn_id = models.CharField(max_length=32, blank=True, verbose_name='Payall transaction ID')
    category = models.IntegerField(choices=INSURANCE_TYPE_MAP.iteritems(), verbose_name='Insurance category')
    expiration = models.DateTimeField(null=True)

    def __unicode__(self):
        return '%s - %s' % (self.company, self.order_id)


    def can_view(self, user):
        """
        Check if user is allowed to see invoice details.
        If user is not assigned to invoice, or user is admin 
        of different company than invoice was registered to, 
        return False.
        """
        answer = False
        if user.has_perm('admin.dip_admin'):
            answer = True
        elif user.has_perm('admin.company_admin'):
            if user.company_admin.get().company == self.company:
                answer = True
        elif user == self.customer:
            answer = True
        return answer 

    @property
    def order_id(self):
        return hash_encode(self.pk, min_length=10, alphabet=settings.ALPHABET_UPPER)
    
    def quote_expired(self):
        """
        Check if quote has expired.
        """
        if timezone.now() > self.expiration:
            return True
        return False
