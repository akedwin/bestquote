from .insure import checksum
from .insure import cubic_capacity
from .insure import years_generator

from .mail import account_creation_mail
from .mail import admin_account_creation_mail
from .mail import CoverNoteSender
from .mail import new_proposal_mail

from .admin_tools import no_admin_allowed
from .admin_tools import check_company_perms


# Used in models
YES_NO_MAP = {
    0: 'Yes',
    1: 'No',
}
