from hashids import Hashids
from django.conf import settings


def hash_encode(obj, *args, **kwargs):
	"""
	Encode integer into hash string.
	"""
	hashids = Hashids(salt=settings.HASHIDS_SALT, *args, **kwargs)
	return hashids.encode(obj)


def hash_decode(obj, *args, **kwargs):
	"""
	Decode hash string. This funtion return tuple.
	"""
	hashids = Hashids(salt=settings.HASHIDS_SALT, *args, **kwargs)
	return hashids.decode(obj)
