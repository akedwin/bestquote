#coding: utf-8
from django.http import Http404
from django.shortcuts import redirect
from django.contrib import messages


def check_company_perms(func):
    def check(req, *args, **kwargs):
        if req.user.has_perm('admin.dip_admin'):
            pass
        elif req.user.has_perm('admin.company_admin'):
            user_company = req.user.company_admin.get().company.short_name
            request_company = kwargs['company']
            if user_company != request_company:
                raise Http404
        else:
            raise Http404
        return func(req, *args, **kwargs)
    return check


def no_admin_allowed(func):
    def check(req, *args, **kwargs):
        if req.user.is_staff:
            messages.warning(
                req,
                'As an user with administrator rights, you cannot use sections dedicated to normal users.'
            )
            return redirect('admin_page')
        return func(req, *args, **kwargs)
    return check