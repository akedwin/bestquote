var page = require('webpage').create();
var system = require("system");

page.viewportSize = { width: 600, height: 600 };
page.paperSize = { format: 'A4', orientation: 'portrait', margin: '2cm' };

var address = system.args[1];
var target = system.args[2];

page.open(address, function (status) {
    window.setTimeout(function () {
        page.render(target, { format: 'pdf' });
        phantom.exit();
    }, 200);
});
