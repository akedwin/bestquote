#coding: utf-8
from __future__ import unicode_literals
from django.forms.widgets import MultiWidget, Select, TextInput
from django.contrib.auth.forms import SetPasswordForm
from django.utils.translation import ugettext as _
from django.conf import settings
from django import forms
import re

class SelectPhoneWidget(MultiWidget):
    """
    This is custom widget used to split telephone into two fields:
        1. directional part of telephone as a select fields,
        2. main part of telephone number as a text field.
    Widget required prefix variable as a list or tuple:
    ('023', '024', '050', etc.)
    """

    def __init__(self, attrs=None, prefix=None, required=True):
        self.prefix_attrs = {'style': 'width:5em'}
        if attrs:
            self.prefix_attrs.update(attrs)
        self.attrs = attrs or {}
        self.required = required
        if prefix:
            self.prefix = [(x, x) for x in prefix]
            self.prefix.insert(0, ('', '---'))
        else:
            raise ValueError('prefix variable missing')
        _widgets = (
            Select(attrs=self.prefix_attrs, choices=self.prefix),
            TextInput(attrs=self.attrs),
        )
        super(SelectPhoneWidget, self).__init__(_widgets, self.attrs)

    def value_from_datadict(self, data, files, name):
        return [widget.value_from_datadict(data, files, name + '_%s' % i) for i, widget in enumerate(self.widgets)]

    def decompress(self, value):
        if value:
            return [value[:3] , value[3:]]
        return [None, None]



class PrefixPhoneNumber(forms.MultiValueField):
    def __init__(self, prefix=None, *args, **kwargs):
        fields = (
            forms.CharField(),
            forms.CharField(),
        )
        widget = SelectPhoneWidget(prefix=prefix)
        super(PrefixPhoneNumber, self).__init__(fields=fields, widget=widget, *args, **kwargs)

    def compress(self, value):
        return value[0]+value[1]


class PasswordStrengthValidator(object):
    """
    Strength Password Validator. It use settings to determine against what rules password
    should be checked. Rise ValidationError if password do not match requirements.

    Validations:
    1. Contains numbers (how many);
    2. Contains uppercase letters (how many);
    """

    message = "Password should have at least {}"
    code = "complexity"

    def __init__(self, *args, **kwargs):
        self.numbers = settings.PASSWORD_RULES.get('NUMBERS', None)
        self.uppercase = settings.PASSWORD_RULES.get('UPPERCASE', None)

    def __call__(self, value):
        if self.numbers:
            how_many_numbers = len(re.findall(r'[0-9]', value))
            if not how_many_numbers >= self.numbers:
                msg = '{} number(s) (it has {}).'.format(self.numbers, how_many_numbers)
                raise forms.ValidationError(self.message.format(msg), code=self.code)

        if self.uppercase:
            how_many_uppercase = len(re.findall(r'[A-Z]', value))
            if not how_many_uppercase >= self.uppercase:
                msg = '{} uppercase (it has {}).'.format(self.uppercase, how_many_uppercase)
                raise forms.ValidationError(self.message.format(msg), code=self.code)


class CustomSetPasswordForm(SetPasswordForm):
    """
    Overwrite the default SetPasswordForm to add custom validators, but preserve rest django logic.
    """
    new_password1 = forms.CharField(
        label=_("New password"),
        widget=forms.PasswordInput,
        max_length=30,
        min_length=settings.PASSWORD_RULES.get('LENGTH', 1),
        validators=[PasswordStrengthValidator()],
    )

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(SetPasswordForm, self).__init__(*args, **kwargs)
