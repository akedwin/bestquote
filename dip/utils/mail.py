#coding: utf-8
from email.MIMEImage import MIMEImage
from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.template import Context
from django.template.loader import render_to_string
from django.utils import timezone
from django.conf import settings
from dip.paths import root
from dip.utils import checksum
from subprocess import call
import os


def format_mail(title, text_content, html_content):
    """
    Common part of all mail functions.
    """
    email_title = '[BestQuote] %s' % title
    email = EmailMultiAlternatives(email_title, text_content)
    email.attach_alternative(html_content, "text/html")
    # By default, the content type is set to “multipart/alternative”. 
    # But this resulted in the images just being displayed as attachments.
    # Setting the content type to “multipart/related” do the trick and
    # images are embedded in message.
    email.mixed_subtype = 'related'
    logo_path = root('../static/img/bq_mail.png')
    country_logo_path = root('../static/img/bq_mail_%s.png' % settings.COUNTRY.lower())
    if os.path.exists(country_logo_path):
        logo_path = country_logo_path

    with open(logo_path, 'rb') as f:
        msg_img = MIMEImage(f.read())
        msg_img.add_header('Content-ID', '<bq_mail.png>')
        email.attach(msg_img)
    return email

 
class CoverNoteSender(object):
    def __init__(self, host, user, order_id, amount):
        self.host = host
        self.order = order_id
        self.user = user
        self.amount = amount
        self.sub = 'Payment Confirmed'
       
    def __render_pdf(self):
        # the param order must be the same as in dip.ghana.motor.view cover function
        params = [self.order, self.amount, self.user.email]
        token = checksum(params, settings.COVER_NOTE_SALT)
        view_url = reverse('cover_note', kwargs={'order_id': self.order, 'token': token})
        url = 'http://%s%s' % (self.host, view_url)
        phantomjs = settings.PHANTOMJS_BIN
        engine = settings.PHANTOMJS_SCRIPT
        # all covernotes are created using the order_id as a name
        target = '%s/%s.pdf' % (settings.SENDFILE_ROOT, self.order)
        # check if covernote exist (in case this function is called 
        # again and the pdf is already created)
        if not os.path.exists(target):
            call([phantomjs, '--ssl-protocol=any', '--ignore-ssl-errors=true', engine, url, target])
        pdf = open(target, 'rb')
        # we returning the fileobject, close method will be called later
        return pdf

    def send(self):
        c = Context({
            'full_user_name': self.user.get_full_name(),
            'order_id': self.order,
            'start_time': timezone.now().strftime('%d/%m/%Y %X'),
            'address': self.user.profile.address,
            'town': self.user.profile.town,
            'country': settings.COUNTRY,
        })
        text_content = render_to_string('mails/cover_note.txt', c)
        html_content = render_to_string('mails/cover_note.html', c)
        email = format_mail(self.sub, text_content, html_content)
        email.to = [self.user.email]
        pdf = self.__render_pdf()
        email.attach('CoverNote.pdf', pdf.read(), 'application/pdf')
        email.send()
        # close the file opened during rendering pdf, to free the memory.
        pdf.close()


def account_creation_mail(host, user, order_id, url):
    c = Context({
        'full_user_name': user.get_full_name(),
        'order_id': order_id,
        'activation_url': url,
        'domain': host,
        'country': settings.COUNTRY,
    })
    text_content = render_to_string('mails/account_creation.txt', c)
    html_content = render_to_string('mails/account_creation.html', c)
    email = format_mail('New proposal registered', text_content, html_content)
    email.to = [user.email]
    email.send()


def new_proposal_mail(host, user, order_id, url):
    c = Context({
        'full_user_name': user.get_full_name(),
        'order_id': order_id,
        'url': url,
        'domain': host,
        'country': settings.COUNTRY,
    })
    text_content = render_to_string('mails/new_proposal.txt', c)
    html_content = render_to_string('mails/new_proposal.html', c)
    email = format_mail('New proposal reqistered', text_content, html_content)
    email.to = [user.email]
    email.send()


def admin_account_creation_mail(host, user, url):
    c = Context({
        'full_user_name': user.get_full_name(),
        'activation_url': url,
        'domain': host,
        'country': settings.COUNTRY,
    })
    text_content = render_to_string('mails/admin_account_creation.txt', c)
    html_content = render_to_string('mails/admin_account_creation.html', c)
    email = format_mail('Account Created', text_content, html_content)
    email.to = [user.email]
    email.send()


def account_activation_mail(host, user, url):
    c = Context({
        'full_user_name': user.get_full_name(),
        'activation_url': url,
        'domain': host,
        'country': settings.COUNTRY,
    })
    text_content = render_to_string('mails/account_activation.txt', c)
    html_content = render_to_string('mails/account_activation.html', c)
    email = format_mail('Account Activation', text_content, html_content)
    email.to = [user.email]
    email.send()
