#coding: utf-8
from __future__ import unicode_literals
from datetime import datetime
from hashlib import sha224
from moneyed import Money


def years_generator(start_year=1970):
    """
    Generate a list of [id, value] where id and value is a year.
    Used for SelectField in forms.
    """
    for year in xrange(datetime.today().year, start_year-1, -1):
        yield (year, year)


def cubic_capacity():
    """
    Generator that return list of values from 100 to 10000 in 100 steps.
    Used in SelectField forms
    """
    for cubic in xrange(500, 10100, 100):
        yield (cubic, '%s %s' %(cubic, 'cc'))


def checksum(params, salt=None, invoice=False):
    """
    Create checksum hash for values passed in list or tuple.
    If salt is provided also updates this hash by salt
    """
    _checksum = sha224()
    if invoice:
        for field in ('order-id', 'amount', 'curr', 'title', 'salt'):
            _param = params.get(field, '')
            if isinstance(_param, Money):
                _param = _param.amount
            _checksum.update(str(_param))
    else:
        for field in params:
            if isinstance(field, Money):
                field = field.amount
            _checksum.update(str(field))
    if salt:
        _checksum.update(str(salt))
    return _checksum.hexdigest()
