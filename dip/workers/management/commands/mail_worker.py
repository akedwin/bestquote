from django.core.management.base import BaseCommand
from FQ import Worker


class Command(BaseCommand):
	def handle(self, *args, **kwargs):
		from dip.workers.mail import send_mail, send_covernote
		w = Worker('mail')
		w.start()

