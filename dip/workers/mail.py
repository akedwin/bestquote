from FQ import task

from dip.core.models import User
from dip.utils.mail import account_activation_mail
from dip.utils.mail import admin_account_creation_mail
from dip.utils.mail import new_proposal_mail
from dip.utils.mail import account_creation_mail
from dip.utils.mail import CoverNoteSender


TEMPLATE_MAP = {
	'activation': account_activation_mail,
	'proposal': new_proposal_mail,
	'account': account_creation_mail,
	'admin_account': admin_account_creation_mail,
}


def get_user_model(email):
	"""
	Get user model using email (username).
	"""
	return User.objects.get(email=email)


@task()
def send_mail(template, *args, **kwargs):
	kwargs['user'] = get_user_model(kwargs['email'])
	del kwargs['email']
	template = TEMPLATE_MAP.get(template, None)
	template(**kwargs)


@task()
def send_covernote(host, email, order_id, amount):
	user = get_user_model(email)
	cns = CoverNoteSender(host, user, order_id, amount)
	cns.send()
