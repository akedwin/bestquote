import django_tables2 as tables
from django_tables2.utils import A
from django.contrib.auth import get_user_model
from dip.core.models import Company
from dip.core.models import Invoice
from django.utils.safestring import mark_safe
from django.utils.html import escape
from django.core.urlresolvers import reverse


class UsersTable(tables.Table):
    id = tables.Column(verbose_name=' ', orderable=False)

    class Meta:
        model = get_user_model()
        exclude = ['password', 'is_active', 'username', 'is_staff', 'is_superuser']
        sequence = ('first_name', 'last_name', 'email', 'date_joined', 'last_login', 'id')
        attrs = {'class': 'table'}
        empty_text = u'No data found'

    def render_id(self, value):
        customer = reverse('customer_info', args=[value])
        return mark_safe('<a href="%s" class="btn btn-primary btn-xs"><span class="fa fa-info"></span> Details</a>' % customer)


class CompanyTable(tables.Table):
    name = tables.LinkColumn('company_admin', args=[A('short_name')], verbose_name='Name')
    logo = tables.Column(orderable=False, verbose_name='Logo')

    class Meta:
        model = Company
        exclude = ['id']
        sequence = ('logo', 'name', 'short_name')
        attrs = {'class': 'table table-striped'}
        empty_text = u'No data found'

    def render_name(self, record, value):
        link = reverse('company_admin', args=[record.short_name])
        return mark_safe('<h3><a href="%s">%s</a></h3>' % (link, escape(value),))

    def render_logo(self, record, value):
        link = reverse('company_admin', args=[record.short_name])
        return mark_safe('<a href="%s"><img src="/media/%s" /></a>' % (link, escape(value)))


class InvoiceTable(tables.Table):
    id = tables.Column(verbose_name='Order Id', orderable=False)
     
    class Meta:
        model = Invoice
        attrs = {"class": "table"}
        empty_text = u'No data found'

    def __init__(self, data, user=None, *args, **kwargs):
        super(InvoiceTable, self).__init__(data, *args, **kwargs)
        self.user = user

    def render_id(self, record):
        link = reverse('quote_details', args=[record.order_id])
        return mark_safe('<a href="%s">%s</a>' % (link, record.order_id))

    def render_customer(self, record, value):
        if self.user and self.user.has_perm('admin.company_admin') and not self.user.is_superuser:
            return value
        link = reverse('customer_info', args=[record.customer_id])
        return mark_safe('<a href="%s">%s</a>' % (link, value))


class CompanyInsurancesTable(InvoiceTable):

    class Meta:
        exclude = ['company','txn_id', 'status', 'amount_currency']
        

class CompanyProposalsTable(InvoiceTable):

    class Meta:
        exclude = ['company','txn_id', 'expiration', 'amount_currency']


class AdminsTable(tables.Table):
    role = tables.Column(accessor='pk')

    class Meta:
        model = get_user_model()
        exclude = ['id', 'password', 'is_active', 'username', 'is_staff', 'is_superuser']
        sequence = ('first_name', 'last_name', 'email', 'role', 'date_joined', 'last_login')
        attrs = {'class': 'table'}
        empty_text = u'No data found'

    def render_role(self, record):
        role = None
        if record.is_superuser:
            role = 'BQ Super Admin'
        elif record.has_perm('admin.dip_admin'):
            role = 'BestQuote Admin'
        elif record.has_perm('admin.company_admin'):
            role = '%s Admin' % record.company_admin.get().company.short_name
        return role
