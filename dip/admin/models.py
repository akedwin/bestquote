from django.db import models

class CompanyAdmin(models.Model):
    class Meta:
        app_label = 'admin'
        permissions = (
            ('dip_admin', 'BestQuote Administrator'),
            ('company_admin', 'Insurance Company Administrator'),
        )

    user = models.ForeignKey('core.User', related_name='company_admin')
    company = models.ForeignKey('core.Company')

    def __unicode__(self):
        return '%s - %s admin' % (self.user.get_full_name(), self.company)