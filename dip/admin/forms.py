#coding: utf-8
from __future__ import unicode_literals
from django import forms
from django.core.exceptions import ObjectDoesNotExist
from dip.core.models import Company
from dip.core.models import User
from dip.core.models.invoice import INVOICE_STATUSES_MAP


class CreateAdminForm(forms.Form):
    firstname = forms.CharField(max_length=30)
    lastname = forms.CharField(max_length=30)
    email = forms.EmailField()
    cemail = forms.EmailField(label='Confirm email')
    c_admin = forms.ChoiceField(
        label='Assign user to company',
        required=False,
        help_text='If you want to create BestQuote Admin, leave this field unselected.'
    )

    def __init__(self, user, *args, **kwargs):
        super(CreateAdminForm, self).__init__(*args, **kwargs)
        c_list = list(Company.objects.values_list('pk', 'name').order_by('pk'))
        c_list.insert(0, [0, '---'])
        self.fields['c_admin'].choices = c_list
        self.user = user

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            self.user.objects.get(email=email)
        except ObjectDoesNotExist:
            return email
        raise forms.ValidationError('Email address already registered.')

    def clean(self):
        data = super(CreateAdminForm, self).clean()
        email = data.get('email', None)
        cemail = data.get('cemail', None)
        if email and cemail and email != cemail:
            raise forms.ValidationError('"Email" and "Confirm Email" are not identical.')
        if cemail:
            del data['cemail']
        return data


class QuoteFiltersForm(forms.Form):
    customer = forms.EmailField(required=False)
    order_id = forms.CharField(required=False)
    status = forms.ChoiceField(required=False)

    def __init__(self, *args, **kwargs):
        super(QuoteFiltersForm, self).__init__(*args, **kwargs)
        status_list = INVOICE_STATUSES_MAP.items()
        status_list.insert(0, (99, "Any"))
        self.fields['status'].choices = status_list 

    def clean_customer(self):
        email =  self.cleaned_data['customer']
        try:
            user = User.objects.get(email=email)
        except ObjectDoesNotExist:
            user = None
        return user

    def clean_status(self):
        status = self.cleaned_data['status']
        try:
            status = int(status)
        except ValueError:
            status = None
        return status if status != 99 else None

    def clean_order_id(self):
        order_id = self.cleaned_data['order_id']
        try:
            order_id = int(order_id)
        except ValueError:
            order_id = 0 if len(order_id) > 0 else None 
        return order_id
