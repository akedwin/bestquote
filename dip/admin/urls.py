#coding: utf-8
from django.conf.urls import patterns, url

# urls for ghana views
urlpatterns = patterns('dip.admin.views',
    url(r'^admin/$', 'admin_page', name='admin_page'),
    url(r'^admin/users/$', 'users_list', name='users_list'),
    url(r'^admin/users/registered/$', 'users_registered', name='users_registered'),
    url(r'^admin/users/admins/$', 'users_admins', name='users_admins'),
    url(r'^admin/users/admins/create/$', 'add_admin', name='add_admin'),
    url(r'^admin/users/anonymous$', 'users_anonymous', name='users_anonymous'),
    url(r'^admin/users/profile/(\d+)/$', 'user_profile', name='customer_info'),
    url(r'^admin/companies/$', 'company_list', name='company_list'),
    url(r'^admin/quotes/$', 'quote_list', name='quote_list'),
    url(r'^admin/(?P<company>\w+)/$', 'company_admin_page', name='company_admin'),
    url(r'^admin/(?P<company>\w+)/insurances/$', 'company_insurances', name='company_insurances'),
    url(r'^admin/(?P<company>\w+)/proposals/$', 'company_proposals', name='company_proposals'),
    url(r'^admin/(?P<company>\w+)/discounts/$', 'company_discounts', name='company_discounts'),
    url(r'^admin/(?P<company>\w+)/discounts/(?P<model>\w+)/$', 'company_discounts_set', name='company_discounts_set'),
    url(r'^admin/(?P<company>\w+)/reflink/$', 'company_reflink', name='company_reflink'),
)
