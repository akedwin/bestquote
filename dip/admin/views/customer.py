#coding: utf-8
from __future__ import unicode_literals
from importlib import import_module

from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.contrib.auth.models import Permission
from django.conf import settings
from django_tables2 import RequestConfig
from django.contrib.auth.decorators import permission_required

from dip.admin.tables import UsersTable, AdminsTable
from dip.admin.forms import CreateAdminForm
from dip.admin.models import CompanyAdmin
from dip.core.models import Company
from dip.core.models import User

from django.core.exceptions import ObjectDoesNotExist

import FQ

cmodule = import_module('dip.%s.models' % settings.COUNTRY)


@permission_required('admin.dip_admin')
def users_list(req):
    return redirect('users_registered')


@permission_required('admin.dip_admin')
def users_registered(req):
    cc = RequestContext(req)
    table = RequestConfig(req, paginate={"per_page": 25})
    table_users = UsersTable(User.objects.filter(is_active=True).exclude(is_staff=True))
    table.configure(table_users)
    cc['table_users'] = table_users
    return render_to_response('admin/user/user_registered.html', cc)


@permission_required('admin.dip_admin')
def users_anonymous(req):
    cc = RequestContext(req)
    table = RequestConfig(req, paginate={"per_page": 25})
    table_ausers = cmodule.AnonymousCustomerTable(cmodule.AnonymousCustomer.objects.all())
    table.configure(table_ausers)
    cc['table_ausers'] = table_ausers
    return render_to_response('admin/user/user_anonymous.html', cc)


@permission_required('admin.dip_admin')
def users_admins(req):
    cc = RequestContext(req)
    table = RequestConfig(req, paginate={"per_page": 25})
    table_admins = AdminsTable(User.objects.filter(is_active=True, is_staff=True))
    table.configure(table_admins)
    cc['table_admins'] = table_admins
    return render_to_response('admin/user/user_admins.html', cc)


@permission_required('admin.dip_admin')
def user_profile(req, _id):
    customer = get_object_or_404(User, pk=_id)
    cc = RequestContext(req)
    cc['customer'] = customer
    try:
        profile = customer.profile
    except ObjectDoesNotExist:
        profile = None
    cc['profile'] = profile
    return render_to_response('admin/user/user_profile.html', cc)


@permission_required('admin.dip_admin')
def add_admin(req):
    cc = RequestContext(req)

    if req.method == 'POST':
        form = CreateAdminForm(User, req.POST)
        if form.is_valid():
            data = form.cleaned_data
            email = data.get('email')
            firstname = data.get('firstname')
            lastname = data.get('lastname')
            c_admin = int(data.get('c_admin'))
            user = User.objects.create_user(email=email, first_name=firstname, last_name=lastname)
            user.is_active=False
            user.save()
            if c_admin > 0:
                cadmin_model = CompanyAdmin()
                cadmin_model.user = user
                cadmin_model.company = Company.objects.get(pk=c_admin)
                cadmin_model.save()
                c_admin = Permission.objects.get(codename='company_admin')
                user.user_permissions.add(c_admin)
            else:
                dip_admin = Permission.objects.get(codename='dip_admin')
                user.user_permissions.add(dip_admin)
            user.is_staff = True
            user.save()
            # sent mail to owner
            FQ.async.mail.send_mail(
                'admin_account', 
                email=user.email,
                host=req.get_host(),
                url=user.get_activation_url(req),
            )
            cc['created_user'] = user
            form = CreateAdminForm(User)
    else:
        form = CreateAdminForm(User)

    cc['form'] = form
    return render_to_response('admin/user/create_user.html', cc)
