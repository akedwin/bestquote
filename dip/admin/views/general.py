#coding: utf-8
from __future__ import unicode_literals

from django.contrib.auth.decorators import permission_required, user_passes_test

from django.template import RequestContext
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django_tables2 import RequestConfig

from dip.admin.tables import CompanyTable
from dip.admin.tables import InvoiceTable
from dip.admin.forms import QuoteFiltersForm
from dip.core.models import Company
from dip.core.models import Invoice
from dip.core.models import User
from dip.utils import check_company_perms



@user_passes_test(lambda u: u.is_staff)
def admin_page(req):
    if req.user.has_perm('admin.company_admin') and not req.user.is_superuser:
        c = req.user.company_admin.get().company.short_name
        return redirect('company_admin', company=c)
    return redirect('company_list')


@check_company_perms
def company_admin_page(req, company):
    return redirect('company_insurances', company)


@permission_required('admin.dip_admin')
def company_list(req):
    cc = RequestContext(req)
    table = RequestConfig(req, paginate={"per_page": 25})
    table_company = CompanyTable(Company.objects.all())
    table.configure(table_company)
    cc['table'] = table_company
    return render_to_response('admin/company_list.html', cc)


@permission_required('admin.dip_admin')
def quote_list(req):
    cc = RequestContext(req)
    filters = None

    form = QuoteFiltersForm(req.GET)
    if form.is_valid():
        filters = form.cleaned_data
        # return initialized filters with passed data
        form = QuoteFiltersForm(initial=req.GET)
    else:
        form = QuoteFiltersForm()

    qset = Invoice.objects.all()
    if filters:
        for key, value in filters.iteritems():
            if value is not None:
                qset = qset.filter(**{key:value})

    table = RequestConfig(req, paginate={"per_page": 25})
    invoice_table = InvoiceTable(qset, exclude=('amount_currency', "txn_id", "expiration"))
    table.configure(invoice_table)
    cc['table'] = invoice_table
    cc['form'] = form
    return render_to_response('admin/quotes.html', cc)
