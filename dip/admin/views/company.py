#coding: utf-8
from __future__ import unicode_literals
from importlib import import_module

from django.contrib.auth.decorators import permission_required
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.conf import settings
from django_tables2 import RequestConfig

from dip.admin.tables import CompanyInsurancesTable, CompanyProposalsTable
from dip.core.models import Invoice
from dip.core.models import Company
from dip.utils import check_company_perms
from dip.utils.common import hash_encode

import copy


# import tables from country module, key is a table class, value is a model!
discount_tables = import_module('dip.%s.tables' % settings.COUNTRY)


@check_company_perms
def company_insurances(req, company):
    cc = RequestContext(req)
    _company = Company.objects.get(short_name=company)
    table = RequestConfig(req, paginate={"per_page": 25})
    table_insurances = CompanyInsurancesTable(Invoice.objects.filter(company=_company.pk, status__gte=2), user=req.user)
    table.configure(table_insurances)
    cc['insuraces_table'] = table_insurances
    cc['company'] = _company
    return render_to_response('admin/company/insurances.html', cc)


#proposals should be visable only by dip admins
@permission_required('admin.dip_admin')
def company_proposals(req, company):
    cc = RequestContext(req)
    _company = Company.objects.get(short_name=company)
    table = RequestConfig(req, paginate={"per_page": 25})
    table_proposals = CompanyProposalsTable(Invoice.objects.filter(company=_company.pk, status__lt=2))
    table.configure(table_proposals)
    cc['proposals_table'] = table_proposals
    cc['company'] = _company
    return render_to_response('admin/company/proposals.html', cc)


@check_company_perms
def company_discounts(req, company):
    cc = RequestContext(req)
    _company = Company.objects.get(short_name=company)
    table = RequestConfig(req, paginate={"per_page": 25})
    # dynamically configure tables
    tables_list = []
    for nr, keyvalue in enumerate(discount_tables.DiscountTablesDict):
        _table = keyvalue[0]
        _model = keyvalue[1]
        #print _table
        _table_ = _table(_model.objects.filter(company=_company.pk).order_by('pk'), prefix=nr)
        table.configure(_table_)
        tables_list.append({'table': _table_, 'model': _model.__name__})
    cc['sections'] = tables_list
    cc['company'] = _company
    return render_to_response('admin/company/discounts.html', cc)


@check_company_perms
def company_discounts_set(req, company, model):
    # we need to copy the POST to allow editing and remove the crf token
    post = copy.deepcopy(req.POST)
    del post['csrfmiddlewaretoken']

    cmodels = import_module('dip.%s.models' % settings.COUNTRY)
    discount_model = getattr(cmodels, model)
    try:
        discount_model.update_discounts(company, post)
        return HttpResponse(content='Data saved successfully', status=200)
    except:
        return HttpResponse(content='Error! One of the entered value is invalid. Please check and try again.', status=400)


@check_company_perms
def company_reflink(req, company):
    cc = RequestContext(req)
    _company = Company.objects.get(short_name=company)
    reflink_token = hash_encode(_company.pk, min_length=15)
    link = req.build_absolute_uri(reverse('reflink', kwargs={'token': reflink_token}))
    cc['reflink'] = link
    cc['company'] = _company
    return render_to_response('admin/company/reflink.html', cc)
    