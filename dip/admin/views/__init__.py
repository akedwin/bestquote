from .customer import users_list
from .customer import users_registered
from .customer import users_anonymous
from .customer import users_admins
from .customer import user_profile
from .customer import add_admin

from general import admin_page
from general import company_admin_page
from general import company_list
from general import quote_list

from company import company_insurances
from company import company_proposals
from company import company_discounts
from company import company_discounts_set
from company import company_reflink