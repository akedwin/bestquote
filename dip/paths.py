#coding: utf-8
import os

# this variable is used to define main folder of django project structure (e.g.  place sqlite database file etc.)
root = lambda * x: os.path.abspath( os.path.join(os.path.dirname(__file__), *x) )