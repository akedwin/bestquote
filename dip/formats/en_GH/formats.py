# Overwrite default english formatting for Ghana and specify 
# correct formats with official goverment instructions 

DATE_FORMAT = ('d/m/Y')
DATE_INPUT_FORMAT = ('%d/%m/%Y')

DATETIME_FORMAT = ('d/m/Y H:i:s')
DATETIME_INPUT_FORMAT = ('%d/%m/%Y %H:%M:%S')

TIME_FORMAT = ('H:i:s')
TIME_INPUT_FORMATS = ('%H:%M:%S')

THOUSAND_SEPARATOR = ','
DECIMAL_SEPARATOR = '.'
NUMBER_GROUPING = 3

FIRST_DAY_OF_WEEK = 1