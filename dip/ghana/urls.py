#coding: utf-8
from django.conf.urls import patterns, url
from django.views.generic import TemplateView

# urls for ghana views
urlpatterns = patterns('dip.ghana.views',
    url(r'^motor/$', 'motor_start', name='motor'),
    url(r'^travel/$', 'travel_start', name='travel'),
    #url(r'^travel/$', TemplateView.as_view(template_name='ghana/travel.html'), name='travel'),
    url(r'^property/$', TemplateView.as_view(template_name='ghana/property.html'), name='property'),
    url(r'^motor/step1$', 'motor_step1', name='motor_step1'),
    url(r'^motor/step2$', 'motor_step2', name='motor_step2'),
    url(r'^motor/step3$', 'motor_step3', name='motor_step3'),
    url(r'^motor/step4$', 'motor_step4', name='motor_step4'),
    url(r'^motor/step5$', 'motor_step5', name='motor_step5'),
    url(r'^motor/cover_note/(?P<order_id>[A_Z0-9]{10})-(?P<token>\w{56})$', 'cover_note', name='cover_note'),
    url(r'^travel/step1$', 'travel_step1', name='travel_step1'),
    url(r'^travel/step2$', 'travel_step2', name='travel_step2'),
    #url(r'^travel/step3$', 'travel_step3', name='travel_step3'),
    url(r'^travel/step3_t$', 'travel_step3_t', name='travel_step3_t'),
    url(r'^travel/step4$', 'travel_step4', name='travel_step4'),
)
