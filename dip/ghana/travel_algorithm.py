#coding: utf-8
from decimal import Decimal as d
#from dip.ghana.models.vehicle import TYPE_OF_USAGE_MAP
from dip.core.models import Company
from django.utils import timezone
from django.conf import settings
from moneyed import Money
from django.db.models import F
from models.travel_premiums import travel_premium
from dip.core.models import Company

from datetime import datetime, timedelta, date


from dip.ghana.models.trip import DESTINATION_MAP
import math
from dateutil.relativedelta import *




def getTQuote(req):
	#get variables from session array
	trip = req.session['trip']
	print("this is the trip",trip)
	cover_type = trip['cover_type']
	
	traveller_type = trip['traveller_type']
	#destination = DESTINATION_MAP[trip['destination']]
	
	dob = trip['dob']
	traveller_age = date.today().year - dob.year

	if(cover_type == 2) or (cover_type == 3) :
		trip_start = trip['trip_start']
		trip_end = trip['trip_end']
		#calculate difference between dates
		period = trip_end - trip_start
		time = relativedelta(trip_end,trip_start)
		all_days = int(period.days)
		trip_days = int(time.days)
		trip_months = int(time.months)
		if(trip_months == 0):
		#check for premiums with only days usually under a month
			#available_policies = travel_premium.objects.filter(min_days__lt=trip_days,max_days__gte=trip_days,destinations_covered=destination,age_limit__gte=traveller_age,min_months=0,max_months=0)
			available_policies = travel_premium.objects.filter(min_days__lt=trip_days,max_days__gte=trip_days,age_limit__gte=traveller_age,min_months=0,max_months=0)

		else:
		#check for premiums a month and over. Those quoted in months and weeks (converted to days) and those quoted in only weeks(converted to days)
			#available_policies = travel_premium.objects.filter(min_months__lt=trip_months,max_months__gte=trip_months,destinations_covered=destination,age_limit__gte=traveller_age,min_days=trip_days,max_days=trip_days) | travel_premium.objects.filter(min_days__lt=all_days,max_days__gte=all_days,destinations_covered=destination,age_limit__gte=traveller_age,min_months=0,max_months=0)
			available_policies = travel_premium.objects.filter(min_months__lt=trip_months,max_months__gte=trip_months,age_limit__gte=traveller_age,min_days=trip_days,max_days=trip_days) | travel_premium.objects.filter(min_days__lt=all_days,max_days__gte=all_days,age_limit__gte=traveller_age,min_months=0,max_months=0)

	if(cover_type == 1):
		trip_start = trip['trip_start']
		available_policies = travel_premium.objects.filter(annual=1,age_limit__gte=traveller_age)

	
	#include an if for the available policy query..... for case with no months and case with months and days

	#obtain number of days
	#duration = int(period.days)
	#print(duration)
	# print("days: ")
	# print(trip_days)
	# print("months: ")
	# print(trip_months)
	# print("all days: ")
	# print(all_days)




	#query for policies based on duraton, detination and age limit
	#destination1 to be for companies with duration quoted in days only
	#destination2 to be for companies with duration quoted in months and days

	#available_policies = travel_premium.objects.filter(duration=duration,destinations_covered=destination,age_limit__gte=traveller_age) | travel_premium.objects.filter(duration=duration2)
	#available_policies = travel_premium.objects.filter(min_days__gt=duration1,max_days__lte=duration1,destinations_covered=destination,age_limit__gte=traveller_age)
	
	


#min_days>7>max_days
#min_days<7<=max_days
#min_month<1<=max_month

	policy_list = []
	policy_dictionary={}
	comp_list = []
	company_queryset = Company.objects.all()
	#print(available_policies)
	
	for t in available_policies:
		if available_policies:

			rec_id = t.id
			comp_id = t.company_id
			company = company_queryset.get(pk=comp_id).name
			#print(company)
			if traveller_age < 18 and comp_id == 3:
				premium = t.travel_premium * .7
				pType = t.policy_name + " (30% discount for traveller under 18yrs)"

			elif traveller_age > 65 and traveller_age < 71 and comp_id == 3:
				premium = t.travel_premium * 1.5
				pType = t.policy_name + " (+50% for traveller between 66 and 70yrs)"

			elif traveller_age > 70 and traveller_age < 76 and comp_id == 3:
				premium = t.travel_premium * 2.5
				pType = t.policy_name + " (+150% for traveller between 71 and 75yrs)"

			elif traveller_age > 75 and traveller_age < 86 and comp_id == 3:
				premium = t.travel_premium * 3.5
				pType = t.policy_name + " (+250% penalty for traveller between 71 and 75yrs)"

			#50% discount for 18yrs - NSIA
			elif traveller_age < 18 and comp_id == 13:
				premium = t.travel_premium * .7
				pType = t.policy_name + " (50% discount for traveller under 18yrs)"

			#50% increase for 66 - 75yrs - NSIA
			elif traveller_age > 65 and traveller_age < 76 and comp_id == 13:
				premium = t.travel_premium * 1.5
				pType = t.policy_name + " (+50%  for traveller between 66 and 75yrs)"

			#50% increase for 76 - 80yrs - NSIA
			elif traveller_age > 75 and traveller_age < 81 and comp_id == 13:
				premium = t.travel_premium * 2
				pType = t.policy_name + " (+100% for traveller between 76 and 80yrs)"

			#300% increase for 81yrs - NSIA
			elif traveller_age > 75 and traveller_age < 81 and comp_id == 13:
				premium = t.travel_premium * 3
				pType = t.policy_name + " (+200% for 81yr old traveller )"

			else:
				premium = t.travel_premium
				pType = t.policy_name
			
			#print(premium,type(premium))
			MaxCover = t.medical_cover
			Policyexcess = t.excess
			

			#Build list of companies
			#comp_list.append((comp_id,company))

			policy_list.append((
				comp_id,
				#company,
				premium,
				MaxCover,
				Policyexcess,
				pType,
				rec_id,
				#destinations_covered
				))

	        #using record id to identify policies as multiple policies will appear for each company making multiple same company ids in the dictionary
	        policy_dictionary.update({rec_id:(
	            premium,
	            MaxCover,
	            Policyexcess,
	            pType,
	            comp_id,
	            #rec_id,
	            #destinations_covered,
	            )})
		req.session['policy_details'] = policy_dictionary
	#print("policy dict")
	#print(policy_dictionary)
		
		
		#sort policies retrived smallest to largest
	policy_list = sorted(policy_list, key=lambda value: value[1])
	#print(policy_list)



	#query for company names in order of sorted policies
	for pk,value1,value2,value3,value4,value5 in policy_list:
		#don't know why I have this
		#company = company_queryset.get(pk=pk).name

		#print(company)
		#appending policy id from premium table and company name in company list array
		comp_list.append((value5,company))
	


	#print(comp_list)
	return comp_list, policy_list

#list out policy details for step 2 of form
def policy_details(values_list):
	policy_values = []
	#print(values_list, type(values_list))

	for item in values_list:
	 	policy_values.append(({
			'nr': item[0],
	# 		'company' : item[1],
	 		'premium' : item[1],
	 		'max_cover' : item[2],
	 		'excess' : item[3],
	 		'logo' : Company.objects.get(pk=item[0]).logo,
	 		'ptype' : item[4]
	 		}))
	# #print(policy_values, type(policy_values))
	return policy_values


# def quote_values(values_list):
#     values = []
#     for item in values_list:
#         values.append(({
#             'nr': item[0], # this is needed, because two company needs to have disabled one value (done in template)
#             'quote': item[1],
#             'quote_excess': item[2],
#             'logo': Company.objects.get(pk=item[0]).logo
#         }))
#     return values

