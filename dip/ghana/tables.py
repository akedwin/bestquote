import django_tables2 as tables
from dip.ghana.models import NoClaimsDiscount, ExtraNoClaimsDiscount, FleetDiscount, OtherDiscount
from decimal import Decimal


def calc_percent(value):
    # value is a decimal object
    value = value * 100
    return value.quantize(Decimal('1.00'))


class ENCDTable(tables.Table):
    private = tables.Column()
    commercial = tables.Column()
    motorcycle = tables.Column()
    ambulance = tables.Column()
    goods = tables.Column()
    hduty = tables.Column()

    class Meta:
        model = ExtraNoClaimsDiscount
        exclude = ['company', 'id']
        empty_text = u'No data found'
        attrs = {"class": "table"}

    def render_private(self, value):
        return calc_percent(value)

    def render_commercial(self, value):
        return calc_percent(value)

    def render_motorcycle(self, value):
        return calc_percent(value)

    def render_ambulance(self, value):
        return calc_percent(value)

    def render_goods(self, value):
        return calc_percent(value)

    def render_hduty(self, value):
        return calc_percent(value)

    @property
    def name(self):
        return u'Extra No Claims Discount'


class NCDTable(tables.Table):
    private = tables.Column()
    commercial = tables.Column()
    motorcycle = tables.Column()
    ambulance = tables.Column()
    goods = tables.Column()
    hduty = tables.Column()

    class Meta:
        model = NoClaimsDiscount
        exclude = ['company', 'id']
        empty_text = u'No data found'
        attrs = {"class": "table"}

    def render_private(self, value):
        return calc_percent(value)

    def render_commercial(self, value):
        return calc_percent(value)

    def render_motorcycle(self, value):
        return calc_percent(value)

    def render_ambulance(self, value):
        return calc_percent(value)

    def render_goods(self, value):
        return calc_percent(value)

    def render_hduty(self, value):
        return calc_percent(value)

    @property
    def name(self):
        return u'No Claims Discount'


class FDTable(tables.Table):
    class Meta:
        model = FleetDiscount
        exclude = ['company', 'id']
        empty_text = u'Set to 0% for now'
        attrs = {"class": "table"}

    @property
    def name(self):
        return u'Fleet Discount'


class ODTable(tables.Table):
    other = tables.Column()

    class Meta:
        model = OtherDiscount
        exclude = ['company', 'id']
        empty_text = u'No data found'
        attrs = {"class": "table"}

    def render_other(selfself, value):
        return calc_percent(value)

    @property
    def name(self):
        return u'Other Discount'


DiscountTablesDict = (
    (NCDTable, NoClaimsDiscount),
    (ENCDTable, ExtraNoClaimsDiscount),
    (FDTable, FleetDiscount),
    (ODTable, OtherDiscount),
)
