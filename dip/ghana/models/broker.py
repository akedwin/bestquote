#coding: utf-8
from __future__ import unicode_literals
from decimal import Decimal as d

from django.db import models
from django.db import transaction
from django.conf import settings
from djmoney.models.fields import MoneyField


class OverLevelPercentage(Exception):
    pass


class NoClaimsMixin(object):
    @classmethod
    def update_discounts(cls, company, post):
        """
        This needs to be changed in something more efficient!
        """
        with transaction.atomic():
            for name, value in post.iteritems():
                _name_, index,  = name[:-1], name[-1:]
                obj = cls.objects.get(years=index, company__short_name=company)
                _value_ = d(value) / 100
                if _value_ > d(1):
                    raise OverLevelPercentage('Percentage cannot be bigger than 99%')
                setattr(obj, _name_, _value_)
                obj.save()

    def get_discount(self, tou):
        # tou - type of usage
        # the range is obtain from ghana.models.vehicle.TYPE_OF_USAGE_MAP
        if tou < 3:
            discount = self.private
        elif tou > 2 and tou < 7:
            discount = self.commercial
        elif tou == 7:
            discount = self.motorcycle
        elif tou == 8:
            discount = self.ambulance
        elif tou > 8 and tou < 12:
            discount = self.goods
        else:
            discount = self.hduty
        return discount


class NoClaimsDiscount(models.Model, NoClaimsMixin):
    class Meta:
        app_label = 'ghana'

    years = models.IntegerField(verbose_name='Years of no claims')
    private = models.DecimalField(max_digits=5, decimal_places=4, verbose_name='Private discount [%]')
    commercial = models.DecimalField(max_digits=5, decimal_places=4, verbose_name='Commercial discount [%]')
    motorcycle = models.DecimalField(max_digits=5, decimal_places=4, verbose_name='Motorcycle discount [%]')
    ambulance = models.DecimalField(max_digits=5, decimal_places=4, verbose_name='Ambulance/Hearse discount [%]')
    goods = models.DecimalField(max_digits=5, decimal_places=4, verbose_name='Goods discount [%]')
    hduty = models.DecimalField(max_digits=5, decimal_places=4, verbose_name='Heavy duty equipment discount [%]')
    company = models.ForeignKey('core.Company', related_name='noclaim_discount')

    def __unicode__(self):
        return self.company.name


class ExtraNoClaimsDiscount(models.Model, NoClaimsMixin):
    class Meta:
        app_label = 'ghana'

    years = models.IntegerField(verbose_name='Years of no claims')
    private = models.DecimalField(max_digits=5, decimal_places=4, verbose_name='Private discount [%]')
    commercial = models.DecimalField(max_digits=5, decimal_places=4, verbose_name='Commercial discount [%]')
    motorcycle = models.DecimalField(max_digits=5, decimal_places=4, verbose_name='Motorcycle discount [%]')
    ambulance = models.DecimalField(max_digits=5, decimal_places=4, verbose_name='Ambulance/Hearse discount [%]')
    goods = models.DecimalField(max_digits=5, decimal_places=4, verbose_name='Goods discount [%]')
    hduty = models.DecimalField(max_digits=5, decimal_places=4, verbose_name='Heavy duty equipment discount [%]')
    company = models.ForeignKey('core.Company', related_name='extra_noclaim_discount')

    def __unicode__(self):
        return self.company.name


class FleetDiscount(models.Model):
    class Meta:
        app_label = 'ghana'

    start_nr = models.IntegerField(verbose_name='Starts from')
    fleet = models.DecimalField(max_digits=5, decimal_places=4, verbose_name='Fleet discount [%]')
    company = models.ForeignKey('core.Company', related_name='fleet_discount')

    def __unicode__(self):
        return self.company.name


class OtherDiscount(models.Model):
    class Meta:
        app_label = 'ghana'

    other = models.DecimalField(default=0, max_digits=5, decimal_places=4, verbose_name='Other discount [%]')
    company = models.ForeignKey('core.Company', related_name='other_discount')

    def __unicode__(self):
        return self.company.name

    def get_discount(self):
        return self.other

    @classmethod
    def update_discounts(cls, company, post):
        with transaction.atomic():
            for name, value in post.iteritems():
                _name_ = name[:-1]
                _value_ = d(value) / 100
                if _value_ > d(1):
                    raise OverLevelPercentage('Percentage cannot be bigger than 99%')
                obj = cls.objects.get(pk=1, company__short_name=company)
                setattr(obj, _name_, _value_)
                obj.save()


class BrokerCommission(models.Model):
    class Meta:
        app_label = 'ghana'

    company = models.ForeignKey('core.Company', related_name='broker_commission')
    private = models.DecimalField(default=0, max_digits=5, decimal_places=4, verbose_name='Private commission [%]')
    commercial = models.DecimalField(default=0, max_digits=5, decimal_places=4, verbose_name='Commercial commission [%]')
    motorcycle = models.DecimalField(default=0, max_digits=5, decimal_places=4, verbose_name='Motorcycle commission [%]')
    ambulance = models.DecimalField(default=0, max_digits=5, decimal_places=4, verbose_name='Ambulance/Hearse commission [%]')
    goods = models.DecimalField(default=0, max_digits=5, decimal_places=4, verbose_name='Goods commission [%]')
    hduty = models.DecimalField(default=0, max_digits=5, decimal_places=4, verbose_name='Heavy duty equipment commission [%]')

    def __unicode__(self):
        return self.company.name

    def get_commission(self, tou):
        # tou - type of usage
        # the range is obtain from ghana.models.vehicle.TYPE_OF_USAGE_MAP
        if tou < 3:
            discount = self.private
        elif tou > 2 and tou < 7:
            discount = self.commercial
        elif tou == 7:
            discount = self.motorcycle
        elif tou == 8:
            discount = self.ambulance
        elif tou > 8 and tou < 12:
            discount = self.goods
        else:
            discount = self.hduty
        return discount


class QuoteDetails(models.Model):
    class Meta:
        app_label = 'ghana'

    invoice = models.ForeignKey('core.Invoice', related_name='quote_details')
    premium = MoneyField(max_digits=10, decimal_places=2, default_currency=settings.CURRENCY, verbose_name='Premium value')
    premium_excess = MoneyField(max_digits=10, decimal_places=2, default_currency=settings.CURRENCY, verbose_name='Premium excess value')
    broker_discount = models.DecimalField(max_digits=5, decimal_places=4, verbose_name='Total discount [%]')
    ncd = models.DecimalField(max_digits=5, decimal_places=4, verbose_name='No claim discount [%]')
    od = models.DecimalField(max_digits=5, decimal_places=4, verbose_name='Other discount [%]')
    fd = models.DecimalField(max_digits=5, decimal_places=4, verbose_name='Fleet discount [%]')
    broker_commision = MoneyField(max_digits=10, decimal_places=2, default_currency=settings.CURRENCY, verbose_name='Broker commision value')
    vehicle = models.ForeignKey('ghana.Vehicle')
    ownership = models.ForeignKey('ghana.Ownership')
    misc = models.ForeignKey('ghana.Misc')
    tpl_value = MoneyField(max_digits=10, decimal_places=2, default_currency=settings.CURRENCY, verbose_name='TPL value')
    # ode_value = MoneyField(max_digits=10, decimal_places=2, default_currency=settings.CURRENCY, verbose_name='ODE value')


