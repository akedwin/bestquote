# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

from django.db import models
from django_countries.fields import CountryField
from django_countries import countries

#Function to list dropdown for number of people to be insured
NUMBER_OF_PEOPLE_TO_INSURE = {x:x for x in xrange(0, 11)}



# names of function used in algorithm and stored in DB
TYPE_OF_COVER_MAP = {
    1:      'Annual Multi-Trip',
    2:      'Pilgrimage',
    3:      'Single Trip',
}

# names of choices, grouped in categories, to present in SelectForm
TYPE_OF_COVER_SELECT_FORM = (
    (0, '...'),
    (1, 'Annual Multi-Trip'),
    (2, 'Pilgrimage'),
    (3, 'Single Trip'),           
)

# Map of Destination
DESTINATION_MAP = {
    1:      'Europe',
    2:      'Schengen',
    3:      'Worldwide',
    4:      'Worldwide Select',
}

# names of choices, grouped in categories, to present in SelectForm
DESTINATION_SELECT_FORM = (
    ('', '...'),
    (1, 'Europe'),
    (2, 'Schengen'),
    (3, 'Worldwide'),
    (4, 'Worldwide_Select*'),
            
)


# Map of TRAVELLERS
TRAVELLER_MAP = {
    1:      'Europe',
    2:      'Schengen',
    3:      'Worldwide',
    4:      'Worldwide-Select',
}

# names of choices, grouped in categories, to present in SelectForm
TYPE_TRAVELLER_SELECT_FORM = (
    ('', '...'),
    (1, 'Individual'),
    ('Family', (
            (2, 'Two members'),
            (3, 'Three members'),
            (4, 'Four members'),
            (5, 'Five members'),
            (6, 'Six members'),
            (7, 'Seven members'),
            (8, 'Eight members'),
            (9, 'Nine members'),
            (10, 'Ten members'),
        )
    ),

    ('Group', (
            (11, 'Two members'),
            (12, 'Three members'),
            (13, 'Four members'),
            (14, 'Five members'),
            (15, 'Six members'),
            (16, 'Seven members'),
            (17, 'Eight members'),
            (18, 'Nine members'),
            (19, 'Ten members'),
        )
    ),
    
)


class Trip(models.Model):
    class Meta:
        app_label = 'ghana'

    user = models.ForeignKey('core.User')
    trip_start = models.DateField(verbose_name='Trip Start Date',blank=True)
    trip_end = models.DateField(verbose_name='Trip End Date',blank=True)
    number_of_people_to_insure = models.IntegerField(choices=NUMBER_OF_PEOPLE_TO_INSURE.iteritems(), verbose_name='No. of People to Insure', default=0)
    #USandCanada = models.BooleanField()
    cover_type = models.IntegerField(choices=TYPE_OF_COVER_MAP.iteritems(), verbose_name='Type of Cover', default=0)
    # #destination = models.IntegerField(choices=DESTINATION_MAP.iteritems(), verbose_name='Destination', default=0)
    destination = CountryField()
    traveller_type = models.IntegerField(choices=TRAVELLER_MAP.iteritems(), verbose_name='Traveller Type', default=0)


