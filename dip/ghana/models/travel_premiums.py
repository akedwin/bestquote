# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
from djmoney.models.fields import MoneyField
from django.conf import settings
from django.db import models


class travel_premium(models.Model):
	class Meta:
		app_label = 'ghana'

	#typeof_policy = models.CharField(max_length=100, verbose_name='Policy Type')
	

	#old model code

	# policy_name = models.CharField(max_length=100, verbose_name='Policy Name')

	# duration = models.IntegerField(max_length=10, verbose_name='Duration of Trip', default=0)

	# travel_premium = MoneyField(max_digits=10, decimal_places=2, default_currency=settings.CURRENCY, verbose_name="Travel Premium")
	# company = models.ForeignKey('core.Company', related_name='company_travel_permium')
	# medical_cover = MoneyField(max_digits=10, decimal_places=2, default_currency=settings.CURRENCY, verbose_name="Medical Cover", null=True)
	# excess = MoneyField(max_digits=10, decimal_places=2, default_currency=settings.CURRENCY, verbose_name="Excess",null=True)
	# trip_type = models.CharField(max_length=100, verbose_name='Trip Type')
	# destinations_covered = models.CharField(max_length=100, verbose_name='Destination')
	# age_limit = models.IntegerField(max_length=10, verbose_name='Age Limit', default=0)

	policy_name = models.CharField(max_length=100, verbose_name='Policy Name')

	#duration = models.IntegerField(max_length=10, verbose_name='Duration of Trip', default=0)

	min_days = models.IntegerField(max_length=10, verbose_name='Min days', default=0)
	max_days = models.IntegerField(max_length=10, verbose_name='Max days', default=0)

	min_months = models.IntegerField(max_length=10, verbose_name='Min months', default=0)
	max_months = models.IntegerField(max_length=10, verbose_name='Max months', default=0)

	travel_premium = MoneyField(max_digits=10, decimal_places=2, default_currency=settings.CURRENCY, verbose_name="Travel Premium")
	company = models.ForeignKey('core.Company', related_name='company_travel_permium')
	medical_cover = MoneyField(max_digits=10, decimal_places=2, default_currency=settings.CURRENCY, verbose_name="Medical Cover", null=True)
	excess = MoneyField(max_digits=10, decimal_places=2, default_currency=settings.CURRENCY, verbose_name="Excess",null=True)
	trip_type = models.CharField(max_length=100, verbose_name='Trip Type')
	destinations_covered = models.CharField(max_length=100, verbose_name='Destination')
	age_limit = models.IntegerField(max_length=10, verbose_name='Age Limit', default=0)
	annual = models.BooleanField()



	#company = models.ForeignKey('core.models.Company')

	