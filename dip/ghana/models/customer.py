#coding: utf-8
from __future__ import unicode_literals
from django.db import models
import django_tables2 as tables
from dip.core.abstracts import CustomerAbstract

DRIVING_YEARS = {x:x for x in xrange(0, 66)}

class CustomerInfo(CustomerAbstract):
    class Meta:
        app_label = 'ghana'

    occupation = models.CharField(max_length=30, verbose_name='Business or occupation', blank=True)
    town = models.CharField(max_length=30, verbose_name='City/town', blank=True)
    address = models.CharField(max_length=256, verbose_name='Street address and number', blank=True)
    driver_license = models.IntegerField(choices=DRIVING_YEARS.iteritems(), verbose_name='Years FULL driving license held', null=True)
    birth = models.DateField(verbose_name='Date of birth', null=True)
    telephone = models.CharField(max_length=15, verbose_name='Mobile number', null=True)
    landline = models.CharField(max_length=15, verbose_name='Landline number', null=True)
    convictions = models.IntegerField(choices=[(x, x) for x in xrange(0,6)], verbose_name='Years of no claims', null=True)
    previous_insure = models.CharField(max_length=30, verbose_name='Name of your previous insurance company', blank=True)
    previous_insure_nr = models.CharField(max_length=30, verbose_name='Number of your previous insurance policy', blank=True)

    def __unicode__(self):
        return self.user.get_full_name()

    @property
    def customer_data(self):
        # This method contain verbose names of fields and thair values,
        # to show them in user profile
        return (
            ('Date of birth', self.birth),
            ('Street address and number', self.address),
            ('Town/City', self.town),
            ('Telephone', self.telephone),
            ('Business or occupation', self.occupation),
            ('Years FULL driving license held', self.driver_license),
            ('Years of no claims', self.convictions),
            ('Name of your previous insurance company', self.previous_insure),
            ('Number of your previous insurance policy', self.previous_insure_nr)
        )


class AnonymousCustomer(models.Model):
    class Meta:
        app_label = 'ghana'

    first_name = models.CharField(max_length=30, verbose_name='First name')
    last_name = models.CharField(max_length=30, verbose_name='Last name')
    email = models.EmailField(verbose_name='Email address')
    telephone = models.CharField(max_length=10, verbose_name='Telephone number')
    convictions = models.IntegerField(verbose_name='Years of no claims')
    previous_insure = models.CharField(verbose_name='Name of your previous insurance company', max_length=30, blank=True)
    previous_insure_nr = models.CharField(verbose_name='Number of your previous insurance policy', max_length=30, blank=True)
    birth = models.DateField(verbose_name='Date of birth', null=True)
    driver_license = models.IntegerField(choices=DRIVING_YEARS.iteritems(), verbose_name='Years FULL driving license held', null=True)

    def __unicode__(self):
        return '%s %s' % (self.first_name, self.last_name)


class AnonymousCustomerTable(tables.Table):
    class Meta:
        model = AnonymousCustomer
        attrs = {"class": "table"}
        exclude = ['id', 'session_key']
        empty_text = u'No data found'




