#coding: utf-8
from __future__ import unicode_literals
from django.db import models
import django_tables2 as tables
from dip.core.abstracts import CustomerAbstract

class TravellerInfo(CustomerAbstract):
    class Meta:
        app_label = 'ghana'

    occupation = models.CharField(max_length=30, verbose_name='Business or occupation', blank=True)
    birth = models.DateField(verbose_name='Date of birth', null=True)
    passport_num = models.CharField(max_length=10, verbose_name='Passport Number', blank=True)

    def __unicode__(self):
         return self.user.get_full_name()

    @property
    def traveller_data(self):
        # This method contain verbose names of fields and thair values,
        # to show them in user profile
        return (
            ('Date of birth', self.birth),
            ('Business or occupation', self.occupation),
            ('Passport Number', self.passport_num),
        )


class AnonymousTraveller(models.Model):
    class Meta:
        app_label = 'ghana'

    first_name = models.CharField(max_length=30, verbose_name='First name')
    last_name = models.CharField(max_length=30, verbose_name='Last name')
    email = models.EmailField(verbose_name='Email address')
    telephone = models.CharField(max_length=10, verbose_name='Telephone number')
    occupation = models.CharField(max_length=30, verbose_name='Occupation', blank=True)
    passport_num = models.CharField(max_length=10, verbose_name='Passport Number', blank=True)

    
    
    def __unicode__(self):
        return '%s %s' % (self.first_name, self.last_name)


class AnonymousTravellerTable(tables.Table):
    class Meta:
        model = AnonymousTraveller
        attrs = {"class": "table"}
        exclude = ['id', 'session_key']
        empty_text = u'No data found'

        