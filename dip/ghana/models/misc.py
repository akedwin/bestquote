#coding: utf-8
from __future__ import unicode_literals
from django.db import models
from .vehicle import MakeOfVehicle
from .vehicle import CarBodyType
from dip.core.abstracts import CompanyAddressAbstract
from dip.utils import YES_NO_MAP

# INSURANCE_TYPE_MAP = {
#     1: 'Comprehensive Policy',
#     2: 'Liability to Third Parties',
#     # depricated because of request in zoho
#     # 3: 'Motor Vehicles Act'
# }


class Ownership(models.Model):
    class Meta:
        app_label = 'ghana'

    vrp = models.CharField(max_length=20, verbose_name='Registration number')
    vin = models.CharField(max_length=17, verbose_name='Chassis number')
    make_of_vehicle = models.ForeignKey(MakeOfVehicle, related_name='vehicle_make', verbose_name='Make of Vehicle')
    body_type = models.ForeignKey(CarBodyType, related_name='body_type', verbose_name='Car Body Type')
    repair = models.IntegerField(choices=YES_NO_MAP.iteritems(), verbose_name='Is the vehicle at present in a state of repair?')
    changed = models.IntegerField(choices=YES_NO_MAP.iteritems(), verbose_name="Has the vehicle been altered or adapted from the original manufacturer's design in any way?")
    owner = models.IntegerField(choices=YES_NO_MAP.iteritems(), verbose_name='Are you the owner of the vehicle and is it registered in your name?')
    owner_address = models.CharField(max_length=200, blank=True, verbose_name='If you are not the owner state name and address of owner')
    loan = models.IntegerField(choices=YES_NO_MAP.iteritems(), verbose_name='Did you or the owner of the vehicle obtain a loan to purchase the vehicle?')
    loan_info = models.CharField(max_length=200, blank=True, verbose_name='If so please state the name and address of person/company from whom the loan was obtained')

    def __unicode__(self):
        return 'User: %s, Invoice: %s' % (self.user, self.user.invoice_order_id)


class Misc(models.Model):
    class Meta:
        app_label = 'ghana'

    accident = models.IntegerField(choices=YES_NO_MAP.iteritems(), verbose_name='Accidents or losses')
    defect = models.IntegerField(choices=YES_NO_MAP.iteritems(), verbose_name='Defective vision or hearing or other physical infirmity')
    previous_insure = models.IntegerField(choices=YES_NO_MAP.iteritems(), verbose_name='Previous motor insurance details')
    declined_proposal = models.IntegerField(choices=YES_NO_MAP.iteritems(), verbose_name='declined your proposal?')
    carry_loss = models.IntegerField(choices=YES_NO_MAP.iteritems(), verbose_name='required you to carry the first portion of any loss?')
    special_conditions = models.IntegerField(choices=YES_NO_MAP.iteritems(), verbose_name='required an increased premium or imposed special conditions?')
    refuse_renew = models.IntegerField(choices=YES_NO_MAP.iteritems(), verbose_name='refused to renew your license?')
    cancelled_policy = models.IntegerField(choices=YES_NO_MAP.iteritems(), verbose_name='cancelled your policy?')

    def __unicode__(self):
        return 'User: %s, Invoice: %s' % (self.user, self.user.invoice_order_id)


class CompanyAddress(CompanyAddressAbstract):

    contact_person = models.CharField(max_length=255, blank=True)
    po_box = models.CharField(max_length=100, blank=True)

    class Meta:
        app_label = 'ghana'