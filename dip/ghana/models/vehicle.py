#coding: utf-8
from __future__ import unicode_literals
from django.db import models
from dip.core.abstracts import MakeOfVehicleAbstract
from dip.core.abstracts import CarBodyTypeAbstract
from dip.utils import YES_NO_MAP
from djmoney.models.fields import MoneyField
from django.conf import settings
from moneyed import Money

# names of function used in algorithm and stored in DB
TYPE_OF_USAGE_MAP = {
    1:      'PrivateIndividual',
    2:      'PrivateCorporate',
    3:      'Taxi',
    4:      'RentalCar',
    5:      'Minibus',
    6:      'Maxibus',
    7:      'Motorcycle',
    8:      'AmbulanceHearse',
    9:      'OwnGoods',
    10:     'HireTruck',
    11:     'Articulated',
    12:     'HDEOnSite',
    13:     'HDEOnRoad',
}

# names of choices to present to user
TYPE_OF_USAGE_HUMAN_MAP = {
    1:      '[Private] Individual',
    2:      '[Private] Corporate',
    3:      '[Commercial] Taxi',
    4:      '[Commercial] RentalCar',
    5:      '[Commercial] Minibus (up to 15 seats)',
    6:      '[Commercial] Maxibus (above 15 seats)',
    7:      'Motorcycle',
    8:      'Ambulance / Hearse',
    9:      '[Goods] Own goods',
    10:     '[Goods] Hire truck',
    11:     '[Goods] Articulated lorry / tanker',
    12:     '[Heavy duty equipment] Use on site only',
    13:     '[Heavy duty equipment] Use on road',
}

# names of choices, grouped in categories, to present in SelectForm
TYPE_OF_USAGE_SELECT_FORM = (
    ('', '...'),
    ('Private', (
            (1, 'Individual'),
            (2, 'Corporate'),
        )
    ),
    ('Commercial', (
            (3, 'Taxi'),
            (4, 'Rental Car'),
            (5, 'Minibus (up to 15 seats)'),
            (6, 'Maxibus (above 15 seats)'),
        )
    ),
    ('Goods', (
            (9, 'Own goods'),
            (10, 'Hire truck'),
            (11, 'Articulated lorry / tanker'),
        )
    ),
    ('Heavy duty equipment', (
            (12, 'Use on site only'),
            (13, 'Use on road'),
        )
    ),
    ('Other', (
            (7, 'Motorcycle'),
            (8, 'Ambulance / Hearse'),
        )
    )
)

# names of function used in algorithm and stored in DB
TYPE_OF_INSURANCE_MAP = {
    1:      'Comprehensive',
    2:      'Third Party',
}

# names of choices, grouped in categories, to present in SelectForm
TYPE_OF_INSURANCE_SELECT_FORM = (
    ('', '...'),
    (1, 'Comprehensive'),
    (2, 'Third Party'),          
)


class Vehicle(models.Model):
    class Meta:
        app_label = 'ghana'

    insurance_type = models.IntegerField(choices=TYPE_OF_INSURANCE_MAP.iteritems(), verbose_name='Type of insurance', default=0)
    value = MoneyField(max_digits=10, decimal_places=2, default_currency=settings.CURRENCY, verbose_name="Estimated present value (including accesories) of vehicle")
    prod_year = models.PositiveSmallIntegerField(verbose_name='Year of manufacture')
    engine_vol = models.IntegerField(verbose_name='Engine cubic capacity')
    usage = models.IntegerField(choices=TYPE_OF_USAGE_MAP.iteritems(), verbose_name='Type of usage', default=0)
    seats = models.PositiveIntegerField(verbose_name='Vehicle seat capacity')
    user = models.ForeignKey('core.User')
    tpl = models.IntegerField(choices=YES_NO_MAP.iteritems(), verbose_name='Third Party Limit?')
    tpl_amount = MoneyField(max_digits=10, decimal_places=2, default_currency=settings.CURRENCY, verbose_name='Third Party Limit Amount', null=True)
    # ode = models.IntegerField(choices=YES_NO_MAP.iteritems(), verbose_name='Own Damage Excess?')

    def __unicode__(self):
        return 'Model: %s, vol %s' % (self.prod_year, self.engine_vol)

    @property
    def usage_human(self):
        return TYPE_OF_USAGE_HUMAN_MAP[self.usage]

    @property
    def tpl_amount_value(self):
        if self.tpl_amount:
            return self.tpl_amount
        return Money('0', settings.CURRENCY)


class MakeOfVehicle(MakeOfVehicleAbstract):
    class Meta:
        app_label = 'ghana'


class CarBodyType(CarBodyTypeAbstract):
    class Meta:
        app_label = 'ghana'
