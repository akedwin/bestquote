#coding: utf-8
from __future__ import unicode_literals
from uuid import uuid4
from moneyed import Money

from django.http import HttpResponseRedirect
from django.http import Http404
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.conf import settings

from dip.core.models import Company
from dip.core.models import Invoice
from dip.core.models import User

from dip.ghana.models import AnonymousCustomer
from dip.ghana.models import CustomerInfo
from dip.ghana.models import Vehicle as Car
from dip.ghana.models import Ownership as Own
from dip.ghana.models import Misc
from dip.ghana.models import QuoteDetails
from dip.ghana.models.vehicle import TYPE_OF_USAGE_HUMAN_MAP
from dip.ghana.models.misc import YES_NO_MAP

from dip.ghana.forms import Vehicle
from dip.ghana.forms import CompanyChoice
from dip.ghana.forms import CustomerBase
from dip.ghana.forms import CustomerAdditional
from dip.ghana.forms import OwnershipForm
from dip.ghana.forms import Miscellaneous
from dip.ghana.forms import Confirm

from dip.ghana.motor_algorithm import quotes
from dip.ghana.motor_algorithm import quote_values

from dip.utils import checksum
from dip.utils import no_admin_allowed
from dip.utils.common import hash_decode

import FQ


def cover_note(req, order_id=None, token=None):
    """
    Render motor cover note.
    For security reason, each request must be validated using token
    """
    cc = RequestContext(req)
    _pk = hash_decode(order_id, min_length=10, alphabet=settings.ALPHABET_UPPER)[0]
    invoice = get_object_or_404(Invoice, pk=_pk)
    customer = invoice.customer
    params = [order_id, str(invoice.amount.amount), customer.email]
    if token != checksum(params, settings.COVER_NOTE_SALT):
        raise Http404
    cc['invoice'] = invoice
    cc['company'] = invoice.company
    cc['caddress'] = invoice.company.address_data
    cc['ownership'] = invoice.quote_details.get().ownership
    cc['vehicle'] = invoice.quote_details.get().vehicle
    return render_to_response("pdf_templates/cover_note.html", cc)


# this function collect all user choices and eventually translate it to human readable form
def translate_user_choices(req, cc):
    # get vehicle cleaned data
    step1 = req.session['vehicle']
    # because vehicle value and TPL walue are integers we need to convert them into Money object
    if step1.get('tpl_amount', None):
        step1['tpl_amount'] = Money(step1['tpl_amount'], settings.CURRENCY)
    step1['value'] = Money(step1['value'], settings.CURRENCY)
    # update the dict with customer base data from the same step
    step1.update(req.session['customer_base'])
    step1['usage'] = TYPE_OF_USAGE_HUMAN_MAP[step1['usage']]
    # because this is form not model form, value are passed as string
    # therefore we need convert it to int.
    step1['tpl'] = YES_NO_MAP[int(step1['tpl'])]
    step1['tpl_amount'] = step1['tpl_amount']
    # step1['ode'] = YES_NO_MAP[int(step1['ode'])]
    # get quote selection data
    c_pk = req.session['quote']['company']
    user_select = int(req.session['quote']['q_type'])
    q_info = req.session['quote_info']
    step2 = {
        'company': Company.objects.get(pk=c_pk),
        # get Standard quote value if 1 or Excess quote when 2
        'value': q_info[1] if user_select == 2 else q_info[0]
    }
    # get user additional data
    step3 = req.session['customer_add']
    # update by remaining data from step 3
    step3.update(req.session['ownership'])
    step3.update(req.session['misc'])
    # convert all numeric values to YES/NO or empty walues to "-"
    step3['repair'] = YES_NO_MAP[step3['repair']]
    step3['changed'] = YES_NO_MAP[step3['changed']]
    step3['owner'] = YES_NO_MAP[step3['owner']]
    step3['loan'] = YES_NO_MAP[step3['loan']]
    if len(step3['landline']) == 0:
        step3['landline'] = '-'
    step3['accident'] = YES_NO_MAP[step3['accident']]
    step3['defect'] = YES_NO_MAP[step3['defect']]
    step3['previous_insure'] = YES_NO_MAP[step3['previous_insure']]
    if step3['previous_insure'] == 'Yes':
        step3['declined_proposal'] = YES_NO_MAP[step3['declined_proposal']]
        step3['carry_loss'] = YES_NO_MAP[step3['carry_loss']]
        step3['special_conditions'] = YES_NO_MAP[step3['special_conditions']]
        step3['refuse_renew'] = YES_NO_MAP[step3['refuse_renew']]
        step3['cancelled_policy'] = YES_NO_MAP[step3['cancelled_policy']]

    # update context
    cc['Step1'] = step1
    cc['Step2'] = step2
    cc['Step3'] = step3
    return cc


def finalize_proposal(req):
    # get or create user
    user = req.session['customer_base']
    user.update(req.session['customer_add'])
    _user, created_user = User.objects.get_or_create(email=user['email'])

    # if new user, fulfil profile using info from wizard
    # if user exist, skip this step - assum that data are the same
    if created_user:
        _user.is_active = False
        _user.first_name = user['first_name']
        _user.last_name = user['last_name']
        _user.save()

    # check if user has additional profile info and fulfil if necessary
    fields = {}
    for key, value in user.iteritems():
        fields.update({key: value})
    del fields['first_name'], fields['last_name'], fields['email']
    uinfo, created_info = CustomerInfo.objects.get_or_create(user=_user, defaults=fields)
    if not created_info:
        for key, value in fields.iteritems():
            setattr(uinfo, key, value)
        uinfo.save()


    # get company and insurrance package info
    company = req.session['quote']['company']
    user_select = int(req.session['quote']['q_type'])
    quote_info = req.session['quote_info']
    # get Standard quote value if 1 or Excess quote when 2
    if user_select == 2:
        amount = quote_info[1]
    else:
        amount = quote_info[0]

    # create invoice for collected data in proposal
    invoice = Invoice()
    invoice.company_id = company
    invoice.amount = amount
    # monthly payments was disable as requested, so only one annual charge is possible
    invoice.payment = 1
    # list of categories can be found in dip/invoice/models
    invoice.category = 1
    invoice.customer = _user
    invoice.save()


    # quote details are stored in list:
    # 0 - Total quote proce,
    # 1 - Total broker discount,
    # 2 - Broker commission,
    # 3 - Premium
    # 4 - No claims discount
    # 5 - Fleet discount
    # 6 - Other discount
    details = QuoteDetails()
    details.invoice_id = invoice.pk
    details.broker_discount = quote_info[2]
    details.broker_commision = quote_info[3]
    details.premium = quote_info[4]
    details.premium_excess = quote_info[5]
    details.ncd = quote_info[6]
    details.od = quote_info[7]
    details.fd = quote_info[8]
    details.tpl_value = quote_info[9]
    # details.ode_value = quote_info[10]

    # create other records in DB
    vehicle = req.session['vehicle']
    _vehicle = Car()
    for key, value in vehicle.iteritems():
        setattr(_vehicle, key, value)
    _vehicle.user_id = _user.pk
    _vehicle.save()

    ownership = req.session['ownership']
    _ownership = Own()
    for key, value in ownership.iteritems():
        setattr(_ownership, key, value)
    _ownership.save()

    misc = req.session['misc']
    _misc = Misc()
    for key, value in misc.iteritems():
        setattr(_misc, key, value)
    _misc.save()

    # Add Misc, Ownership and Vehicle to QuoteDetails
    details.vehicle = _vehicle
    details.ownership = _ownership
    details.misc = _misc
    details.save()

    # send emial
    if created_user:
        FQ.async.mail.send_mail(
            'account', 
            email=_user.email, 
            order_id=invoice.order_id,
            host=req.get_host(),
            url=_user.get_activation_url(req),
        )
    else:
        FQ.async.mail.send_mail(
            'proposal',
            email=_user.email,
            order_id=str(invoice.order_id),
            host=req.get_host(),
            url=req.build_absolute_uri('/'),
        )

    return {
        'amount': invoice.amount,
        'order_id': invoice.order_id,
        'email': _user.email,
        'firstname': _user.first_name,
        'lastname': _user.last_name,
        'telephone': uinfo.telephone,
        'town': uinfo.town,
        'address': uinfo.address,
    }


@no_admin_allowed
def motor_start(req):
    return HttpResponseRedirect(reverse('motor_step1'))


@no_admin_allowed
def motor_step1(req):
    cc = RequestContext(req)
    if req.method == 'POST':
        vehicle = Vehicle(req.POST)
        customer = CustomerBase(req.POST)
        if vehicle.is_valid() and customer.is_valid():
            # create Anonymous User record in case user will not complete all steps
            cdata = customer.cleaned_data
            if not req.user.is_authenticated():
                auser, created = AnonymousCustomer.objects.get_or_create(email=cdata['email'], defaults=cdata)
                if created:
                    for key, value in cdata.iteritems():
                        setattr(auser, key, value)
                auser.save()
            # save vehicle form to session (save is in last step to avoid creating fake records)
            vdata = vehicle.cleaned_data
            req.session['vehicle'] = vdata
            req.session['customer_base'] = cdata
            req.session['step2'] = True

            req.session['insurance_type'] = vdata['insurance_type']
            #print(req.session.get('reflink'))
            #print(req.session.get('vehicle'))
            return HttpResponseRedirect(reverse('motor_step2'))
    else:
        customer_base_data = req.session.get('customer_base', {})
        # here is a little tricky part, user who have empty profile, will override the session stored data
        # when user will click back button, to avoid this, check if user have session stored data
        if req.user.is_authenticated() and len(customer_base_data) == 0:
            user = req.user
            profile = user.profile
            initial = {
                'first_name': user.first_name,
                'last_name': user.last_name,
                'email': user.email,
                'telephone': profile.telephone,
                'convictions': profile.convictions,
                'previous_insure': profile.previous_insure,
                'previous_insure_nr': profile.previous_insure_nr,
            }
        else:
            initial = customer_base_data
        customer = CustomerBase(initial=initial)
        vehicle = Vehicle(initial=req.session.get('vehicle', {'prod_year': 1990, 'engine_vol': 1500, 'seats': 5}))

    cc['vehicle'] = vehicle
    cc['customer'] = customer
    return render_to_response("ghana/motor/step1.html", cc)


@no_admin_allowed
def motor_step2(req):
    cc = RequestContext(req)
    if not req.session.get('step2', None):
        return HttpResponseRedirect(reverse('motor_step1'))

    # here is a place where algorithm generate the order of
    # insurance company list, and also provide detailed data about
    # quotes prices
    # this need to be here, because this values are used both in
    # form generation, and validation
    #_quotes = companies + dict and _quotes_d = quotes

    _quotes, _quotes_d = quotes(req)
    #print(_quotes,type(_quotes))
    #print(_quotes_d,type(_quotes_d))

    if req.method == 'POST':
        # because this form is generic, again, choices need to be provided!!!
        
       #print(_quotes,type(_quotes))
        company = CompanyChoice(_quotes, req.POST)
        #print(company,type(company))

        
        if company.is_valid():
            data = company.cleaned_data
            req.session['quote'] = data
            #data contains dictionary of company ids company:id
            
            company = data['company']
           
            qdetails = req.session.get('quote_details')
            #print(qdetails,type(qdetails))
            req.session['quote_info'] = qdetails[int(company)]
            #print(qdetails[int(company)], type(qdetails[int(company)]))
            req.session['step3'] = True
            return HttpResponseRedirect(reverse('motor_step3'))
    else:
        company = CompanyChoice(_quotes, initial=req.session.get('quote', {}))

    cc['insurance_type'] = req.session['insurance_type']
    cc['company'] = company
    cc['quotes'] = zip(company['company'], quote_values(_quotes_d))
    #print(cc['quotes'])
    #print(cc['company'])
    return render_to_response("ghana/motor/step2.html", cc)



@no_admin_allowed
def motor_step3(req):
    cc = RequestContext(req)
    if not req.session.get('step3', None):
        return HttpResponseRedirect(reverse('motor_step1'))

    if req.method == 'POST':
        customer = CustomerAdditional(req.POST)
        ownership = OwnershipForm(req.POST)
        misc = Miscellaneous(req.POST)
        if customer.is_valid() and ownership.is_valid() and misc.is_valid():
            cdata = customer.cleaned_data
            odata = ownership.cleaned_data
            mdata = misc.cleaned_data
            req.session['customer_add'] = cdata
            req.session['ownership'] = odata
            req.session['misc'] = mdata
            req.session['step4'] = True
            return HttpResponseRedirect(reverse('motor_step4'))

    else:
        # same situation as in step 1
        customer_add_data = req.session.get('customer_add', {})
        if req.user.is_authenticated() and len(customer_add_data) == 0:
            profile = req.user.profile
            initial = {
                'landline': profile.landline,
                'address': profile.address,
                'town': profile.town,
                'occupation': profile.occupation,
                'birth':  profile.birth,
                'driver_license': profile.driver_license,
            }
        else:
            initial = customer_add_data
        customer = CustomerAdditional(initial=initial)
        ownership = OwnershipForm(initial=req.session.get('ownership', {}))
        misc = Miscellaneous(initial=req.session.get('misc', {}))

    cc['customer'] = customer
    cc['ownership'] = ownership
    cc['misc'] = misc
    return render_to_response("ghana/motor/step3.html", cc)


@no_admin_allowed
def motor_step4(req):
    cc = RequestContext(req)
    if not req.session.get('step4', None):
        return HttpResponseRedirect(reverse('motor_step1'))

    if req.method == 'POST':
       form = Confirm(req.POST)
       if form.is_valid():
            # save everythind into DB, delate sessions etc...
            finalized = finalize_proposal(req)
            # write to session all data needed to register payment in payall
            needed_data = {
                'amount': finalized['amount'],
                'order-id': finalized['order_id'],
                'user-mail': finalized['email'],
                'user-firstname': finalized['firstname'],
                'user-lastname': finalized['lastname'],
                'user-phone': finalized['telephone'],
                'user-city': finalized['town'],
                'user-street': finalized['address'],
            }

            # delate tokens from previous steps, clear sessions (leave only customer info)
            del req.session['step2'], req.session['step3'], req.session['step4'], req.session['customer_base'], req.session['customer_add']
            del req.session['vehicle'], req.session['ownership'], req.session['misc'], req.session['quote'], req.session['quote_info']
            # in this step, user is created in system and proposal is registered,
            # there is no need to store this user as Anonymous Customer any more
            if not req.user.is_authenticated():
                anonim = AnonymousCustomer.objects.get(email=finalized['email'])
                anonim.delete()

            # put token for final step
            req.session['payment_data'] = needed_data
            req.session['step5'] = True
            return HttpResponseRedirect(reverse('motor_step5'))
    else:
        form = Confirm()

    # generate human readable values from raw form choices!
    cc = translate_user_choices(req, cc)
    cc['form'] = form
    return render_to_response("ghana/motor/step4.html", cc)


@no_admin_allowed
def motor_step5(req):
    '''
        This view allow to pay with payall
    '''
    if not req.session.get('step5', None):
        return HttpResponseRedirect(reverse('motor_step1'))

    # remove token for this step
    del req.session['step5']

    cc = RequestContext(req)

    pay = req.session['payment_data']

    cc['store_id'] = settings.DIP_STORE_ID
    cc['order_id'] = pay['order-id']
    cc['amount'] = pay['amount'].amount
    cc['currency'] = 'GHS'
    cc['title'] = 'BestQuote - Invoice %s' % pay['order-id']
    cc['salt'] = uuid4().hex
    cc['user_firstname'] = pay['user-firstname']
    cc['user_lastname'] = pay['user-lastname']
    cc['user_phone'] = pay['user-phone']
    cc['user_city'] = pay['user-city']
    cc['user_street'] = pay['user-street']
    cc['user_mail'] = pay['user-mail']
    cc['payall_payment'] = settings.PAYALL_WEB_PAYMENT

    checksum_content = {
        'order-id': cc['order_id'],
        'amount': cc['amount'],
        'curr': cc['currency'],
        'title': cc['title'],
        'salt': cc['salt']
    }
    # delate payment_data
    del req.session['payment_data']
    cc['checksum'] = checksum(checksum_content, settings.DIP_CLIENT_SALT, invoice=True)
    return render_to_response("ghana/motor/step5.html", cc)
