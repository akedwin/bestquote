# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
#coding: utf-8
from __future__ import unicode_literals
from uuid import uuid4
from moneyed import Money

from django.http import HttpResponseRedirect
from django.http import Http404
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.conf import settings

from dip.core.models import Company
from dip.core.models import Invoice
from dip.core.models import User

from dip.ghana.models import AnonymousTraveller
from dip.ghana.models import TravellerInfo

from dip.ghana.models import Ownership as Own
from dip.ghana.models import Misc
from dip.ghana.models import QuoteDetails
#from dip.ghana.models import trip_form
#from dip.ghana.models.misc import YES_NO_MAP
#from dip.ghana.models import policy


from dip.ghana.forms import TravellerBase
from dip.ghana.forms import CustomerAdditional
from dip.ghana.forms import OwnershipForm
from dip.ghana.forms import Miscellaneous
from dip.ghana.forms import Confirm
from dip.ghana.models import Trip as mytrip

from dip.ghana.forms import Trip_Form
from dip.ghana.forms import TravelPolicy_Choices

from dip.ghana.travel_algorithm import getTQuote
from dip.ghana.travel_algorithm import policy_details

#from dip.ghana.travel_algorithm import getbank


#from dip.ghana.motor_algorithm import quote_values

from dip.utils import checksum
from dip.utils import no_admin_allowed
from dip.utils.common import hash_decode

import urllib
import urllib2
import hashlib
import hmac
import md5
from urlparse import urlparse
from urllib import urlencode
import random

from datetime import datetime

import FQ

#ammend this for travel
# this function collect all user choices and eventually translate it to human readable form
def translate_user_choices(req, cc):
    # get traveller cleaned data
    step1 = req.session['trip']
    
    # update the dict with customer base data from the same step
    step1.update(req.session['traveller_base'])

    #collect chosen insurane company and related data from step 2
    step2 = {
        'company': Company.objects.get(pk=req.session['quote_info'][4]),
        'value': req.session['quote_info'][0],
        'medical' : req.session['quote_info'][1]
    }
    vpc_MerchTxnRef = str(random.randint(0,9))

    #dictionary data for if payment data is to be posted into step4
    # trans_data = {
    #     'vpc_AccessCode' : '53378107',
    #     'vpc_Amount' : '100',
    #     'vpc_Command' : 'pay',
    #     'vpc_Locale' : 'en',
    #     'vpc_MerchTxnRef' : vpc_MerchTxnRef,
    #     'vpc_Merchant' : 'GTB109153A01',
    #     'vpc_OrderInfo' : 'bqt98',
    #     'vpc_ReturnURL' : 'http://bestquotehome.com',
    #     'vpc_Currency' : 'GHS',
    #     'vpc_Version' : '1',
    #     }

    # update context
    cc['Step1'] = step1
    cc['Step2'] = step2
    # cc['trans_data'] = trans_data
    return cc

# 
def finalize_proposal(req):
    # get or create user
    user = req.session['traveller_base']
    # print(user)
    #user.update(req.session['customer_add'])
    _user, created_user = User.objects.get_or_create(email=user['email'])

    # if new user, fulfil profile using info from wizard
    # if user exist, skip this step - assum that data are the same
    if created_user:
        _user.is_active = False
        _user.first_name = user['first_name']
        _user.last_name = user['last_name']
        _user.save()#

    # check if user has additional profile info and fulfil if necessary
    # fields = {}
    # for key, value in user.iteritems():
    #     fields.update({key: value})
    # del fields['first_name'], fields['last_name'], fields['email']
    # uinfo, created_info = CustomerInfo.objects.get_or_create(user=_user, defaults=fields)
    # if not created_info:
    #     for key, value in fields.iteritems():
    #         setattr(uinfo, key, value)
    #     uinfo.save()

    # get company and insurrance package info
    #old line company = req.session['trip_policy']['policy_id']
    policy = req.session['trip_policy']['policy_id']
    company_id = req.session['quote_info'][4]
    premium = req.session['quote_info'][0]
    #session -> req.session['quote_info']  result-> (65 USD, 300000 USD, 0 USD, u'IHI', 24)

    # user_select = int(req.session['quote']['q_type'])
    # quote_info = req.session['quote_info']
    # # get Standard quote value if 1 or Excess quote when 2
    # if user_select == 2:
    #     amount = quote_info[1]
    # else:
    #     amount = quote_info[0]

    # create invoice for collected data in proposal
    invoice = Invoice()
    invoice.company_id = company_id
    invoice.amount = premium
    # monthly payments was disable as requested, so only one annual charge is possible
    invoice.payment = 1
    # list of categories can be found in dip/invoice/models
    invoice.category = 2
    invoice.customer = _user
    invoice.save()

    # create trip records in DB
    trip = req.session['trip']
    _trip = mytrip()
    for key, value in trip.iteritems():
        setattr(_trip, key, value)
    _trip.user_id = _user.pk
    _trip.save()

    # send emial
    if created_user:
        FQ.async.mail.send_mail(
            'account', 
            email=_user.email, 
            order_id=invoice.order_id,
            host=req.get_host(),
            url=_user.get_activation_url(req),
        )
    else:
        FQ.async.mail.send_mail(
            'proposal',
            email=_user.email,
            order_id=str(invoice.order_id),
            host=req.get_host(),
            url=req.build_absolute_uri('/'),
        )

    return {
        'amount': invoice.amount,
        'order_id': invoice.order_id,
        'email': _user.email,
        'firstname': _user.first_name,
        'lastname': _user.last_name,
        'telephone': user['telephone'],
        #'town': uinfo.town,
        #'address': uinfo.address,
    }


@no_admin_allowed
def travel_start(req):
    return HttpResponseRedirect(reverse('travel_step1'))


#present user with form to fill for travel insurance 
@no_admin_allowed
def travel_step1(req):
    cc = RequestContext(req) 
    if req.method == 'POST':
        trip = Trip_Form(req.POST)
        traveller = TravellerBase(req.POST)
        if trip.is_valid() and traveller.is_valid():
            cdata = traveller.cleaned_data
            tdata = trip.cleaned_data

            #comment here
            if not req.user.is_authenticated():
                auser, created = AnonymousTraveller.objects.get_or_create(email=cdata['email'], defaults=cdata)
                if created:
                    for key, value in cdata.iteritems():
                        setattr(auser, key, value)
                auser.save()
            # save trip and traveller information to session (save is in last step to avoid creating fake records)
            # print(cdata,type(cdata))
            req.session['trip'] = tdata
            req.session['traveller_base'] = cdata
            req.session['step2'] = True
            print("this is the tdata", tdata)
            return HttpResponseRedirect(reverse('travel_step2'))

    else:
        traveller_base_data = req.session.get('traveller_base', {})
        #if user exists populate fields with stored information
        if req.user.is_authenticated() and len(traveller_base_data) == 0:
            user = req.user
            profile = user.profile
            initial = {
                'first_name': user.first_name,
                'last_name': user.last_name,
                'email': user.email,
                'telephone': profile.telephone,
                'occupation': profile.occupation,
                'passport_num': profile.passport_num,
            }
        else:
            initial = traveller_base_data
        traveller = TravellerBase(initial=initial)
        trip = Trip_Form(initial=req.session.get('trip', {}))
    
    cc['traveller'] = traveller
    cc['trip'] = trip
    return render_to_response("ghana/travel/step1.html", cc)

#present travel insurance results based on user choices
@no_admin_allowed
def travel_step2(req):
    cc = RequestContext(req)
    if not req.session.get('step2', None):
        return HttpResponseRedirect(reverse('travel_step1'))

    #function to retrieve selection of insurance companies and insurance details
    comp_list, policy_lis = getTQuote(req)
    #print(comp_list)
    #print(policy_lis)

    if req.method == 'POST':
        #pass list of companies and policy ids to form to create choices based on policy id
        travel_insurance = TravelPolicy_Choices(comp_list, req.POST)

        if travel_insurance.is_valid():
            travel_data = travel_insurance.cleaned_data
            #print(travel_data)
            
            # push the chosen policy into the session
            req.session['trip_policy'] = travel_data

            
            #obtain id of policy
            travel_insurance = travel_data['policy_id']
            tquote_details = req.session.get('policy_details')
            # print(tquote_details[int(travel_insurance)])
            
            
            req.session['quote_info'] = tquote_details[int(travel_insurance)]
            req.session['step3'] = True
            return HttpResponseRedirect(reverse('travel_step3_t'))

    else:
        travel_insurance = TravelPolicy_Choices(comp_list, initial=req.session.get('policy',{}))

    cc['company'] = travel_insurance
    cc['policies'] = zip(travel_insurance['policy_id'], policy_details(policy_lis))
    
    return render_to_response("ghana/travel/step2.html", cc)

@no_admin_allowed
def travel_step3(req):
    cc = RequestContext(req)
    if not req.session.get('step3', None):
        return HttpResponseRedirect(reverse('travel_step1'))

    if req.method == 'POST':
       form = Confirm(req.POST)
       #if form.is_valid():
        # save everythind into DB, delate sessions etc...
        #finalized = finalize_proposal(req)
   






    #if not req.session.get('step2', None):
     #   return HttpResponseRedirect(reverse('travel_step1'))
     
     # if req.method == 'POSt':
     #    form = Confirm(req.POST)
     #    if form.is_valid():
     #        finalized = finalize_proposal(req)
     #        travelpolicy_data = {}
                #    'amount':
                #    'order-id':
                #    'customer_email':
                #    'firstname':
                #    'lastname':
                #    'phone':
                #    'occupation':
               # }

     #}

    else:
        form = Confirm()

    cc = translate_user_choices(req, cc)
    cc['form'] = form
    return render_to_response("ghana/travel/step3_t.html",cc)

def travel_step3_t(req):
    cc = RequestContext(req)
    if not req.session.get('step3', None):
        return HttpResponseRedirect(reverse('travel_step1'))

    if req.method == 'POST':
       form = Confirm(req.POST)
       if form.is_valid:
          finalized = finalize_proposal(req)
          # write to session all data needed to register payment in payall
          needed_data = {
              'amount': finalized['amount'],
              'order-id': finalized['order_id'],
              'user-mail': finalized['email'],
              'user-firstname': finalized['firstname'],
              'user-lastname': finalized['lastname'],
              'user-phone': finalized['telephone'],
          }

          # delate tokens from previous steps, clear sessions (leave only customer info)
          del req.session['step2'], req.session['step3'], req.session['traveller_base']
          del req.session['trip'], req.session['quote_info'], req.session['trip_policy']

       # put token for final step
       req.session['payment_data'] = needed_data
       req.session['step4'] = True
       return HttpResponseRedirect(reverse('travel_step4'))

       

    else:
        form = Confirm()
    cc = translate_user_choices(req, cc)
    cc['form'] = form
    return render_to_response("ghana/travel/step3_t.html",cc)


@no_admin_allowed
def travel_step4(req):
    '''
        This view allow to pay with payall
    '''
    # if not req.session.get('step5', None):
    #     return HttpResponseRedirect(reverse('motor_step1'))

    


    cc = RequestContext(req)

    #live details
    migs_AccessCode = '66491971'
    migs_Merchant = 'GTB109153B01'
    migs_Secret = 'D2057A890B79D549EB44C9C82964FEA3'
    migs_Command = 'pay'
    migs_Locale = 'en'
    migs_Version = '1'

    #test details
    # migs_AccessCode = 'BC4BE10D'
    # migs_Merchant = 'GTB109153A04'
    # migs_Secret = '0F3EEB88A1E62820981DD496D00A96E1'
    # migs_Command = 'pay'
    # migs_Locale = 'en'
    # migs_Version = '1'

    pay = req.session['payment_data']
    vpc_MerchTxnRef = "TrIns_" + str(random.randint(400,7987))
    #vpc_OrderInfo = "TrIns_" + pay['user-lastname'] + datetime.now().month() + datetime.now().year()
    vpc_OrderInfo = "TrIns_" + pay['user-lastname'] + str(random.randint(400,7987))
    # remove token for this step
    cc['vpc_AccessCode'] = migs_AccessCode
    cc['vpc_Amount'] = pay['amount'].amount
    # cc['vpc_Amount'] = 100
    cc['vpc_Command'] = migs_Command
    cc['vpc_Currency'] = pay['amount'].currency
    # cc['vpc_Currency'] = "GHS"
    cc['vpc_Locale'] = migs_Locale
    cc['vpc_MerchTxnRef'] = vpc_MerchTxnRef
    cc['vpc_Merchant'] = migs_Merchant
    cc['vpc_OrderInfo'] = vpc_OrderInfo
    cc['vpc_ReturnURL'] = "http://www.bestquotehome.com/success"
    cc['vpc_Version'] = migs_Version


    

    # payment_data_dict = {
    #       #BQ Code
    #       'vpc_AccessCode' : settings.migs_AccessCode,
    #       'vpc_Amount' : pay['amount'].amount,
    #       'vpc_Command' : settings.migs_Command,
    #       'vpc_Currency' : pay['amount'].currency,
    #       'vpc_Locale' : settings.migs_Locale,
    #       'vpc_MerchTxnRef' : vpc_MerchTxnRef,
    #       'vpc_Merchant' : settings.migs_Merchant,
    #       'vpc_OrderInfo' : vpc_OrderInfo,
    #       'vpc_ReturnURL' : 'http://bestquotehome.com/travel_receipt',          
    #       'vpc_Version' : settings.migs_Version,
    #       }

    payment_key_list = [
            str(migs_Secret),
            str(migs_AccessCode),
            str(pay['amount'].amount),
            # '100',
            str(migs_Command),
            str(pay['amount'].currency),
            # 'GHS',
            str(migs_Locale),
            str(vpc_MerchTxnRef),
            str(migs_Merchant),
            str(vpc_OrderInfo),
            'http://www.bestquotehome.com/success',
            str(migs_Version)
       ]
    #vpc_SecureHash = settings.migs_Secret

    vpc_SecureHash=""
    test_url="https://migs.mastercard.com.au/vpcpay?"
    #pay_url_content = ""
    for value in payment_key_list:
        #pay_url_content += key + '=' + payment_data_dict[key] + '&'
        vpc_SecureHash += value
        #test_url +=        

    vpc_SecureHash =  hashlib.md5(vpc_SecureHash).hexdigest()
    #final_url = pay_url_content + 'vpc_SecureHash=' + vpc_SecureHash

    #create hash on this page and use form to generate url that will be sent - explore the possiblity of getting rid of the dict



    cc['vpc_SecureHash'] = vpc_SecureHash
    return render_to_response("ghana/travel/step4.html", cc)
