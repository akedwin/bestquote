#coding: utf-8
from decimal import Decimal as d
from dip.ghana.models.vehicle import TYPE_OF_USAGE_MAP
from dip.core.models import Company
from django.utils import timezone
from django.conf import settings
from moneyed import Money
from datetime import date
#from models.travel_premiums import travel_premium


class Premium(object):
    """
    5 data points are required from the customer to determine the premium:
        - Type of usage (TypeOfUsage)
        - Value of vehicle (Value)
        - Age of vehicle (Age)
        - Engine cubic capacity (ECC)
        - Number of seats (Seats)

    Note: the premium is not the same as the quote (the quote formulae use the premium as an input and are given later)
    """
    def __init__(self, Value, Age, ECC, Seats,driver_age,license_years,insurance_type):
        self.value = d(Value)
        self.age = d(timezone.now().year) - d(Age)
        self.ecc = d(ECC)
        self.seats = d(Seats)
        self.driver_age = driver_age
        self.license_years = license_years
        self.insurance_type = insurance_type


    def __premium (self, *args):
        # Premium = Basic Premium (BP) + Age Loading (AL) + Cubic Capacity Loading (CCL)
        # + Extra Seat Loading (ESL) + Additional Perils Loading (APL) + Emergency Treatment Loading (ETL)
        # + Personal Accident Benefit Loading (PABL)
        #
        # PremiumExcess = Basic Premium Excess (BPE) + Age Loading Excess (ALE) + Cubic Capacity Loading Excess (CCLE)
        # + Extra Seat Loading (ESL) + Additional Perils Loading (APL) + Emergency Treatment Loading (ETL)
        # + Personal Accident Benefit Loading (PABL)

        #NEW CALCULATION -> Premium = Basic Premium (BP) + Age Loading (AL) + Cubic Capacity Loading (CCL) + THIRD PARTY BASIC PREMIUM
        #this loop adds up all aspects above into the premium
        result = d(0)
        #print("these are args",args)
        
        for arg in args:
            result += arg
        return result

    def __ALConditions(self, BP, BPE):
        # If Age <= 5, AL = 0; ElseIf 6 <= Age <= 10, AL = 5.0% x BP; ElseIf Age > 10, AL = 7.5% x BP
        if self.age >= d(10):
            AL = BP*d(0.075)
            ALE = BPE*d(0.075)
        elif self.age >= d(6) and self.age <= d(10):
            AL = BP*d(0.05)
            ALE = BPE*d(0.05)
        else:
            AL = d(0)
            ALE = d(0)
        return AL, ALE

    def __ECCConditions(self, BP, BPE):
        # If ECC <= 1600, CCL = 0; ElseIf 1600 < ECC <= 2000, CCL = 5.0% x BP; ElseIf ECC > 2000, CCL = 7.5% x BP
        if self.ecc > d(2000):
            CCL = BP*d(0.1)
            CCLE = BPE*d(0.1)
        elif self.ecc > d(1600) and self.ecc <= d(2000):
            CCL = BP*d(0.05)
            CCLE = BPE*d(0.05)
        else:
            CCL = d(0)
            CCLE = d(0)
        return CCL, CCLE

    #commenting out extra seatloading
    def __ESLConditions(self, base):
        # If Seats <= 5, ESL = 0; ElseIf Seats > 5, ESL = GHS (2.00 or 1.00) x Max((Seats - 5),0)
        if self.seats <= d(5):
            ESL = d(0)
        else:
            ESL = base*max((self.seats-d(5)), d(0))
        return ESL

    #inexperienced driver loading
    def __InExpDriverConditions(self, BP):
        if self.driver_age == 18 and self.license_years<2:
            INEXLOADING = BP * d(.10)
        return INEXLOADING

    def PrivateIndividual(self):
        #include ThirdPartyBasicLimit
        BP = self.value*d(0.05)
        #print("basic prem",BP)
        ThirdPartyBasicPrem =210
        BPE = BP
        EXBB = BP * d(0.10)
        if self.insurance_type == 1:
            AL, ALE = self.__ALConditions(BP, BPE)
        else:
            AL, ALE = self.__ALConditions(ThirdPartyBasicPrem, ThirdPartyBasicPrem)
        CCL, CCLE = self.__ECCConditions(BP, BPE)
        ESL = self.__ESLConditions(d(5.00))
        APL = d(5.00)
        
        #emergency treatment loading
        ETL = self.seats*d(0.30)
        #Personal Accident Benefit Loading
        PABL = d(20.00)
        #NIC Motor Contributions
        NMC = d(7.0)
        ECOWAS_PERIL = d(5)
        TAXES = d(5)
        #INEXLOADING = self.__InExpDriverConditions(BP)
        # return Premium, PremiumExcess, NIC Motor Contributions
        #print("this is the insurance type - ", self.insurance_type)
        if(self.insurance_type == 1):
            return self.__premium(BP, AL, CCL, ThirdPartyBasicPrem), self.__premium(BPE, ALE, CCLE, ThirdPartyBasicPrem), NMC,ESL, APL, ETL, PABL,ECOWAS_PERIL,TAXES, EXBB
        else:
            return self.__premium(AL, ThirdPartyBasicPrem), self.__premium(ALE, ThirdPartyBasicPrem), NMC,ESL, APL, ETL, PABL,ECOWAS_PERIL,TAXES, EXBB


    def PrivateCorporate(self):
        #calculate basic own damage premium
        BP = (self.value*d(0.06))
        #print("basic prem",BP)
        BPE = BP
        EXBB = BP * d(0.10)
        AL, ALE = self.__ALConditions(BP, BPE)
        #calculate cc loading
        CCL, CCLE = self.__ECCConditions(BP, BPE) 
        #calculate extra seat loading
        ESL = self.__ESLConditions(d(8.00))
        #additional perils loading
        APL = d(5.00)
        ThirdPartyBasicPrem =210
        #emergency treatment loading
        ETL = self.seats*d(0.30)
        #Personal Accident Benefit Loading
        PABL = d(20.00)
        #NIC Motor Contributions
        NMC = d(7.0)
        ECOWAS_PERIL = d(5)
        TAXES = d(5)

        
        #return self.__premium(BP, AL, CCL, ESL, APL, ETL, PABL), self.__premium(BPE, ALE, CCLE, ESL, APL, ETL, PABL), NMC
        return self.__premium(BP, AL, CCL, ThirdPartyBasicPrem), self.__premium(BPE, ALE, CCLE, ThirdPartyBasicPrem), NMC,ESL, APL, ETL, PABL,ECOWAS_PERIL,TAXES, EXBB
        


    def Taxi(self):
        BP = self.value*d(0.07)
        BPE = BP
        EXBB = BP * d(0.10)
        AL, ALE = self.__ALConditions(BP, BPE)
        CCL, CCLE = self.__ECCConditions(BP, BPE)
        ESL = d(0.00)
        APL = d(5.00)
        ThirdPartyBasicPrem =310
        #emergency treatment loading
        ETL = self.seats*d(0.30)
        #Personal Accident Benefit Loading
        PABL = d(20.00)
        #NIC Motor Contributions
        NMC = d(7.0)
        ECOWAS_PERIL = d(5)
        TAXES = d(5)

        # return Premium, PremiumExcess, NIC Motor Contributions
        return self.__premium(BP, AL, CCL, ThirdPartyBasicPrem), self.__premium(BPE, ALE, CCLE, ThirdPartyBasicPrem), NMC,ESL, APL, ETL, PABL,ECOWAS_PERIL,TAXES, EXBB

    def RentalCar(self):
        BP = self.value*d(0.07)
        BPE = BP
        EXBB = BP * d(0.10)
        AL, ALE = self.__ALConditions(BP, BPE)
        CCL, CCLE = self.__ECCConditions(BP, BPE)
        ESL = self.__ESLConditions(d(8.00))
        APL = d(5.00)
        ThirdPartyBasicPrem =320
        #emergency treatment loading
        ETL = self.seats*d(0.30)
        #Personal Accident Benefit Loading
        PABL = d(20.00)
        #NIC Motor Contributions
        NMC = d(7.0)
        ECOWAS_PERIL = d(5)
        TAXES = d(5)

        # return Premium, PremiumExcess, NIC Motor Contributions
        return self.__premium(BP, AL, CCL, ThirdPartyBasicPrem), self.__premium(BPE, ALE, CCLE, ThirdPartyBasicPrem), NMC,ESL, APL, ETL, PABL,ECOWAS_PERIL,TAXES, EXBB

    def Minibus(self):
        BP = self.value*d(0.07)
        BPE = BP
        EXBB = BP * d(0.10)
        AL, ALE = self.__ALConditions(BP, BPE)
        CCL, CCLE = self.__ECCConditions(BP, BPE)
        ESL = self.__ESLConditions(d(8.00))
        APL = d(5.00)
        ThirdPartyBasicPrem =320
        #emergency treatment loading
        ETL = self.seats*d(0.30)
        #Personal Accident Benefit Loading
        PABL = d(20.00)
        #NIC Motor Contributions
        NMC = d(7.0)
        ECOWAS_PERIL = d(5)
        TAXES = d(5)

        # return Premium, PremiumExcess, NIC Motor Contributions
        return self.__premium(BP, AL, CCL, ThirdPartyBasicPrem), self.__premium(BPE, ALE, CCLE, ThirdPartyBasicPrem), NMC,ESL, APL, ETL, PABL,ECOWAS_PERIL,TAXES, EXBB


    def Maxibus(self):
        BP = self.value*d(0.07)
        BPE = BP
        EXBB = BP * d(0.10)
        AL, ALE = self.__ALConditions(BP, BPE)
        CCL, CCLE = self.__ECCConditions(BP, BPE)
        #ESL = d(0.60)*max((self.seats-d(5)), d(0))
        ESL = self.__ESLConditions(d(8.00))
        APL = d(5.00)
        ThirdPartyBasicPrem =320
        #emergency treatment loading
        ETL = self.seats*d(0.30)
        #Personal Accident Benefit Loading
        PABL = d(20.00)
        #NIC Motor Contributions
        NMC = d(7.0)
        ECOWAS_PERIL = d(5)
        TAXES = d(5)

        # return Premium, PremiumExcess, NIC Motor Contributions
        return self.__premium(BP, AL, CCL, ThirdPartyBasicPrem), self.__premium(BPE, ALE, CCLE, ThirdPartyBasicPrem), NMC,ESL, APL, ETL, PABL,ECOWAS_PERIL,TAXES, EXBB


    def Motorcycle(self):
        BP = self.value*d(0.03)
        BPE = BP
        EXBB = BP * d(0.10)
        AL, ALE = self.__ALConditions(BP, BPE)
        CCL, CCLE = self.__ECCConditions(BP, BPE)
        ESL = d(0.00)
        APL = d(2.00)
        ThirdPartyBasicPrem =110
        #emergency treatment loading
        ETL = (0.0)
        #Personal Accident Benefit Loading
        PABL = d(20.00)
        #NIC Motor Contributions
        NMC = d(7.0)
        ECOWAS_PERIL = d(3)
        TAXES = d(5)

        # return Premium, PremiumExcess, NIC Motor Contributions
        return self.__premium(BP, AL, CCL, ThirdPartyBasicPrem), self.__premium(BPE, ALE, CCLE, ThirdPartyBasicPrem), NMC,ESL, APL, ETL, PABL,ECOWAS_PERIL,TAXES, EXBB


    def AmbulanceHearse(self):
        BP = self.value*d(0.07)
        BPE = BP
        EXBB = BP * d(0.10)
        AL, ALE = self.__ALConditions(BP, BPE)
        CCL, CCLE = self.__ECCConditions(BP, BPE)
        ESL = d(0.60)*max((self.seats-d(5)), d(0))
        APL = d(2.00)
        ThirdPartyBasicPrem =210
        #emergency treatment loading
        ETL = (0.0)
        #Personal Accident Benefit Loading
        PABL = d(20.00)
        #NIC Motor Contributions
        NMC = d(7.0)
        ECOWAS_PERIL = d(5)
        TAXES = d(5)

        # return Premium, PremiumExcess, NIC Motor Contributions
        return self.__premium(BP, AL, CCL, ThirdPartyBasicPrem), self.__premium(BPE, ALE, CCLE, ThirdPartyBasicPrem), NMC,ESL, APL, ETL, PABL,ECOWAS_PERIL,TAXES, EXBB

    def OwnGoods(self):
        BP = self.value*d(0.04)
        BPE = BP
        EXBB = BP * d(0.15)
        AL, ALE = self.__ALConditions(BP, BPE)
        CCL, CCLE = self.__ECCConditions(BP, BPE)
        ESL = d(0.00)
        APL = d(5.00)
        ThirdPartyBasicPrem =320
        #emergency treatment loading
        ETL = (0.0)
        #Personal Accident Benefit Loading
        PABL = d(20.00)
        #NIC Motor Contributions
        NMC = d(7.0)
        ECOWAS_PERIL = d(15)
        TAXES = d(5)

        # return Premium, PremiumExcess, NIC Motor Contributions
        return self.__premium(BP, AL, CCL, ThirdPartyBasicPrem), self.__premium(BPE, ALE, CCLE, ThirdPartyBasicPrem), NMC,ESL, APL, ETL, PABL,ECOWAS_PERIL,TAXES, EXBB

    def HireTruck(self):
        BP = self.value*d(0.06)
        print("BP here ", BP)
        BPE = BP
        EXBB = BP * d(0.10)
        AL, ALE = self.__ALConditions(BP, BPE)
        print("this is age", AL)
        CCL, CCLE = self.__ECCConditions(BP, BPE)
        print("this is ccl", CCL)
        ESL = d(0.00)
        APL = d(5.00)
        
        ThirdPartyBasicPrem =440
        

        ETL = self.seats*d(0.30)
        PABL = d(20.00)
        NMC = d(7.0)
        ECOWAS_PERIL = d(15)
        TAXES = d(5)
        # return Premium, PremiumExcess, NIC Motor Contributions
        return self.__premium(BP, AL, CCL, ThirdPartyBasicPrem), self.__premium(BPE, ALE, CCLE, ThirdPartyBasicPrem), NMC,ESL, APL, ETL, PABL,ECOWAS_PERIL,TAXES, EXBB

    def Articulated(self):
        BP = self.value*d(0.08)
        BPE = BP
        EXBB = BP * d(0.15)
        AL, ALE = self.__ALConditions(BP, BPE)
        CCL, CCLE = self.__ECCConditions(BP, BPE)
        ESL = d(0.00)
        APL = d(5.00)
        ThirdPartyBasicPrem =550
        #emergency treatment loading
        ETL = (0.0)
        #Personal Accident Benefit Loading
        PABL = d(20.00)
        #NIC Motor Contributions
        NMC = d(7.0)
        ECOWAS_PERIL = d(15)
        TAXES = d(5)

        # return Premium, PremiumExcess, NIC Motor Contributions
        return self.__premium(BP, AL, CCL, ThirdPartyBasicPrem), self.__premium(BPE, ALE, CCLE, ThirdPartyBasicPrem), NMC,ESL, APL, ETL, PABL,ECOWAS_PERIL,TAXES, EXBB


    def HDEOnSite(self):
        BP = self.value*d(0.015)
        BPE = BP
        EXBB = BP * d(0.10)
        AL, ALE = self.__ALConditions(BP, BPE)
        CCL, CCLE = self.__ECCConditions(BP, BPE)
        ESL = d(0.00)
        APL = d(5.00)
        ThirdPartyBasicPrem =110
        #emergency treatment loading
        ETL = (0.0)
        #Personal Accident Benefit Loading
        PABL = d(20.00)
        #NIC Motor Contributions
        NMC = d(7.0)
        ECOWAS_PERIL = d(0.0)
        TAXES = d(5)

        # return Premium, PremiumExcess, NIC Motor Contributions
        return self.__premium(BP, AL, CCL, ThirdPartyBasicPrem), self.__premium(BPE, ALE, CCLE, ThirdPartyBasicPrem), NMC,ESL, APL, ETL, PABL,ECOWAS_PERIL,TAXES, EXBB


    def HDEOnRoad(self):
        BP = self.value*d(0.03)
        BPE = BP
        EXBB = BP * d(0.15)
        AL, ALE = self.__ALConditions(BP, BPE)
        CCL, CCLE = self.__ECCConditions(BP, BPE)
        ESL = d(0.00)
        APL = d(5.00)
        ThirdPartyBasicPrem =320
        #emergency treatment loading
        ETL = (0.0)
        #Personal Accident Benefit Loading
        PABL = d(20.00)
        #NIC Motor Contributions
        NMC = d(7.0)
        ECOWAS_PERIL = d(15)
        TAXES = d(5)

        # return Premium, PremiumExcess, NIC Motor Contributions
        return self.__premium(BP, AL, CCL, ThirdPartyBasicPrem), self.__premium(BPE, ALE, CCLE, ThirdPartyBasicPrem), NMC,ESL, APL, ETL, PABL,ECOWAS_PERIL,TAXES, EXBB



# def calculate_ode(value, tou):
#     """
#     Calculate Own Damage Excess
#     10% for private usage and 15% for other usage
#     """
#     if tou <= 2:
#         factor = d('0.1')
#     else:
#         factor = d('0.15')
#     return d(value) * factor


def calculate_tpl(tpl_amount, tou):
    """
    Calculate Third Party Limit
    1% for private individual, 2% for private corporate and 2.5% for all other
    """

    if tou == 1:
        #edited to remove tpl from .01 to 0
        factor = d('0.02')
    elif tou == 2:
        factor = d('0.02')
    elif tou == 7:
        factor = d('0.02')
    else:
        factor = d('0.025')
    return (d(tpl_amount) - d('2000')) * factor,(d(tpl_amount) - d('2000')) * d(0.10)


def quotes(req):
    #print(req,type(req))
    vehicle = req.session['vehicle']
    value = vehicle['value']
    age = vehicle['prod_year']
    ecc = vehicle['engine_vol']
    seats = vehicle['seats']
    tou = TYPE_OF_USAGE_MAP[vehicle['usage']]
    raw_tou = vehicle['usage']
    tpl = vehicle['tpl']
    tpl_amount = vehicle['tpl_amount']
    driver = req.session['customer_base']
    #print("this is driver yob",date.today().year - driver['birth'].year)
    driver_age = date.today().year - driver['birth'].year
    license_years = driver['driver_license']
    insurance_type = vehicle['insurance_type']
    
    
    
    # ode = vehicle['ode']
    #Type of insurance 1 = comprehensive 2 = Thirdparty
    
    premium_alg = Premium(value, age, ecc, seats,driver_age,license_years, insurance_type)
    #print("premium_alg",premium_alg)
    #extract full details of premium components from functions above
    premium, premium_excess, NMC,ESL, APL, ETL, PABL,ECOWAS_PERIL,TAXES,EXBB = getattr(premium_alg, tou)()
    print("Premium here:-",ESL)
    
    tpl_value = d('0')
    # ode_value = d('0')
    # 0 is yes

    if tpl == '0':
        tpl_value,excess_boughtbk = calculate_tpl(tpl_amount, raw_tou)
        #premium += tpl_value
        #premium_excess += tpl_value


    # if ode == '0':
    #     ode_value = calculate_ode(value, tou)
    #     premium += ode_value
    #     premium_excess += ode_value
    #no claim years
    ncy = int(req.session['customer_base']['convictions'])
    quote_details = []
    qdetails = {}
    company_qset = Company.objects.all()
   # print("THE premium here:-",premium) - premium ok to this point

    #print(company_qset)

    #print(Company.objects.all(),type(Company.objects.all()))
    #print(travel_premium.objects.all(),type(travel_premium.objects.all()))
    reflink_pk = req.session.get('reflink', None)


    # If user entered site using reflink, limit his choices 
    # to company from reflink.
    if reflink_pk:
        company_qset = company_qset.filter(pk=reflink_pk)
        #print(company_qset)

    for c in company_qset:
        # get years of no claim discount
        #print("this is c",c)
        ncd = c.noclaim_discount.get(years=ncy)
        ncd = ncd.get_discount(raw_tou)
        
        #print("this is NCD:-", ncd)
        # get years of extra no claims discount
        #cross check Extra no claims discount
        #encd = c.extra_noclaim_discount.get(years=ncy)
        #encd = encd.get_discount(raw_tou)
        # get other discount
        od = c.other_discount.get()
        #print("other discount",od)
        od = od.get_discount()
        #print("other discount-pt2",od)
        # get fleet discount
        # not implemented yet - default 0
        fd = d(0)
        # calculate total broker discount
        # InsCoDiscount% = NCDX#% + ENCDX#% + FD#% + OD%
        #fd=d(0.05)
        #InsCoDiscount = ncd + encd + od + fd
        InsCoDiscount = ncd + od + fd
        
        # calculate total quote price
        # InsCoQuote = (100.0% - InsCoDiscount%) x Premium + NIC Motor Contributions (NMC)
        
        

        
        #InsCoQuote = (d(1.00)-InsCoDiscount)*premium + NMC  + ESL + APL + ETL + PABL + d(1320) + tpl_value
        #print("components here-",premium , NMC , ESL , APL , PABL , ECOWAS_PERIL , TAXES,EXBB)
        InsCoQuote = (d(1.00)-InsCoDiscount)*premium + NMC + ESL + APL + PABL + ECOWAS_PERIL + TAXES + tpl_value 
        
        # InsCoQuoteExcess = ((100.0% - InsCoDiscount%) x PremiumExcess) + NIC Motor Contributions (NMC)

        InsCoQuoteExcess = (d(1.00)-InsCoDiscount)*premium_excess + NMC + ESL + APL + PABL + ECOWAS_PERIL + TAXES+ tpl_value  + EXBB

        # if(insurance_type != 1):
        #     InsCoQuoteExcess = 0

        #YOLO = (d(1.00)-InsCoDiscount)*premium_excess + NMC + ESL + APL + PABL + ECOWAS_PERIL + TAXES+ + tpl_value 

       # print ("YOLO", YOLO)

        # add details to list
        quote_details.append((
            c.pk,
            Money(InsCoQuote.quantize(d('1.00')), settings.CURRENCY),
            Money(InsCoQuoteExcess.quantize(d('1.00')), settings.CURRENCY)
        ))
        # claculate broker commision
        # Commission = InsCoCommission x (Premium - NMC)
        broker_commission = c.broker_commission.get().get_commission(raw_tou)
        commission = broker_commission*(premium-NMC)
        qdetails.update({c.pk: (
            Money(InsCoQuote.quantize(d('1.00')), settings.CURRENCY),
            Money(InsCoQuoteExcess.quantize(d('1.00')), settings.CURRENCY),
            InsCoDiscount,
            Money(commission.quantize(d('1.00')), settings.CURRENCY),
            Money(premium.quantize(d('1.00')), settings.CURRENCY),
            Money(premium_excess.quantize(d('1.00')), settings.CURRENCY),
            ncd,
            fd,
            od,
            Money(tpl_value.quantize(d('1.00')), settings.CURRENCY),
            # ode_value.quantize(d('1.00')),
        )})

    #print(qdetails)
    req.session['quote_details'] = qdetails
    # sort by lowest to highest
    quote_details = sorted(quote_details, key=lambda value: value[1])
    # this is the tricky part, we have a company pk and the quote value,
    # but we want return in form only a pk and name
    companies_list = []
    for pk, value1, value2 in quote_details:
        # values are not used here
        
        companies_list.append((pk, company_qset.get(pk=pk).name))
    #print(companies_list)
    #print(req.session.get('reflink',None))
    return companies_list, quote_details


def quote_values(values_list):
    values = []
    for item in values_list:
        values.append(({
            'nr': item[0], # this is needed, because two company needs to have disabled one value (done in template)
            'quote': item[1],
            'quote_excess': item[2],
            'logo': Company.objects.get(pk=item[0]).logo
        }))
    return values