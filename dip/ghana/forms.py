#coding: utf-8
from __future__ import unicode_literals
from django.forms.extras.widgets import SelectDateWidget
from datetime import date
from django import forms
from dip.utils import YES_NO_MAP
from dip.utils.forms_tools import PrefixPhoneNumber
from dip.ghana.models import Ownership
from dip.ghana.models import Misc
from dip.ghana.models.vehicle import TYPE_OF_USAGE_SELECT_FORM
from dip.ghana.models.vehicle import TYPE_OF_INSURANCE_SELECT_FORM
from dip.ghana.models.customer import DRIVING_YEARS
from moneyed import Money
from django.conf import settings

from bootstrap3_datetime.widgets import DateTimePicker

from dip.ghana.models.trip import TYPE_OF_COVER_SELECT_FORM
from dip.ghana.models.trip import DESTINATION_SELECT_FORM
from dip.ghana.models.trip import TYPE_TRAVELLER_SELECT_FORM

from django_countries.fields import CountryField
from django_countries import countries
from bootstrap3_datetime.widgets import DateTimePicker

class Vehicle(forms.Form):

	insurance_type = forms.IntegerField(widget=forms.Select(choices=TYPE_OF_INSURANCE_SELECT_FORM),label='Type of Insurance required')


	# forms.ChoiceField(
	# 	choices=TYPE_OF_INSURANCE_SELECT_FORM.iteritems(),
	# 	widget=forms.Select(
	# 			choices=TYPE_OF_INSURANCE_SELECT_FORM
	# 		),
	# 	label='Type of Insurance required?',
	# 	# help_text="Standard free limit: {}".format(Money(2000, settings.CURRENCY))
	# )
	value = forms.IntegerField(
		max_value=9999999,
		min_value=1,
		label="Estimated present value of vehicle",
		help_text="For example {}. Please do not use commas and dots.".format(Money(40000, settings.CURRENCY))
	)
	prod_year = forms.IntegerField(
		widget=forms.TextInput(attrs={
				'class': 'slider',
				'data-slider-min': 1970,
				'data-slider-max': date.today().year,
				'data-slider-step': 1,
				'data-slider-value': 1990,
			}),
		label='Year of manufacture')
	engine_vol = forms.IntegerField(
		widget=forms.TextInput(attrs={
				'class': 'slider',
				'data-slider-min': 500,
				'data-slider-max': 9900,
				'data-slider-step': 100,
				'data-slider-value': 1500,
			}),
		label='Engine cubic capacity')
	usage = forms.IntegerField(
		widget=forms.Select(
				choices=TYPE_OF_USAGE_SELECT_FORM
			),
		label='Type of usage')
	seats = forms.IntegerField(
		widget=forms.TextInput(attrs={
				'class': 'slider',
				'data-slider-min': 2,
				'data-slider-max': 50,
				'data-slider-step': 1,
				'data-slider-value': 5,
			}),
		label='Vehicle seat capacity')
	tpl = forms.ChoiceField(
		choices=YES_NO_MAP.iteritems(),
		widget=forms.RadioSelect(),
		label='Need Extra Third Party Limit?',
		help_text="Standard free limit: {}".format(Money(2000, settings.CURRENCY))
	)
	tpl_amount = forms.IntegerField(
		max_value=9999999,
		min_value=2000,
		label="Third Party Limit Amount",
		help_text='Amount must be bigger than standard limit.'
	)
	# ode = forms.ChoiceField(
	#     choices=YES_NO_MAP.iteritems(),
	#     widget=forms.RadioSelect(),
	#     label='Include Own Damage Excess?'
	# )

	def __init__(self, *args, **kwargs):
		super(Vehicle, self).__init__(*args, **kwargs)
		# validation will be performed only if "tpl" field value will be 0 (YES)
		if self.data and self.data.get('tpl') == u'0':
			self.fields.get('tpl_amount').required = True
		else:
			self.fields.get('tpl_amount').required = False


QUOTE_TYPE_MAP = {
	1: 'InsCoQuote',
	2: 'InsCoQuoteExcess',
}

class CompanyChoice(forms.Form):
	"""
	This form is only a template, the company field with choises is provided from view, when form is initialized
	"""

	q_type = forms.ChoiceField(
		choices=QUOTE_TYPE_MAP.iteritems(),
		widget=forms.RadioSelect(),
		label='Quote type',
	)
	def __init__(self, choices, *args, **kwargs):
		super(CompanyChoice, self).__init__(*args, **kwargs)
		# choices are provide from view!
		self.fields['company'] = forms.ChoiceField(choices=choices, widget=forms.RadioSelect())

		

def no_claim_years():
	years = []
	for x in xrange(0, 5):
		years.append((x, x))
	years.append((5, '5+'))
	return years

PHONE_PREFIXES = ('020', '023', '024', '026', '027', '028', '050', '054', '057')


class Customer(forms.Form):
	first_name = forms.CharField(label='First name', max_length=30)
	last_name = forms.CharField(label='Last name', max_length=30)
	address = forms.CharField(label='Street address and number', max_length=265)
	town = forms.CharField(label='City/town', max_length=30)
	occupation = forms.CharField(label='Business or occupation', max_length=50)
	birth = forms.DateField(label='Date of birth')
	telephone = PrefixPhoneNumber(label='Mobile number', prefix=PHONE_PREFIXES)
	landline = forms.CharField(label='Landline number', max_length=15, required=False)
	email = forms.EmailField(label='E-mail address')
	driver_license = forms.ChoiceField(label='Years FULL driving license held')
	convictions = forms.ChoiceField(choices=no_claim_years(), label='Years of no claims')
	previous_insure = forms.CharField(label='Name of your previous insurance company', max_length=30, required=False)
	previous_insure_nr = forms.CharField(label='Policy number of previous insurance', max_length=30, required=False)
	birth = forms.DateField(label='Date of birth')
	driver_license = forms.ChoiceField(label='Years FULL driving license held')

	def __init__(self, *args, **kwargs):
		super(Customer, self).__init__(*args, **kwargs)
		self.fields['birth'].widget = SelectDateWidget(years=(x for x in xrange(date.today().year-18, date.today().year-91, -1)), attrs={'style': 'width:8em'})
		self.fields['driver_license'].choices = [('', '---')]+DRIVING_YEARS.items()
		self.fields['address'].widget = forms.Textarea(attrs={'rows': 3})
		self.fields['birth'].widget = SelectDateWidget(years=(x for x in xrange(date.today().year-18, date.today().year-91, -1)), attrs={'class': 'form-control'})
		self.fields['driver_license'].choices = [('', '---')]+DRIVING_YEARS.items()

	def clean_landline(self):
		data = self.cleaned_data['landline']
		if len(data) == 0:
			pass
		elif not data.isdigit():
			raise forms.ValidationError('Landline number must contains only digits.')
		return data

	def clean_telephone(self):
		data = self.cleaned_data['telephone']
		if not data.isdigit():
			raise forms.ValidationError('Phone number must contains only digits.')
		elif len(data) != 10:
			raise forms.ValidationError('Phone number must have 10 digits (including prefix).')
		return data

	def clean(self):
		data = super(Customer, self).clean()
		birth = data.get('birth', None)
		_license = data.get('driver_license', None)

		if birth and _license:
			age = date.today().year - birth.year
			if int(_license) > age:
				raise forms.ValidationError('Driver license held can not be longer, than life of driver.')
			if int(_license) > (age-17):
				raise forms.ValidationError('Longer held driver license than it may result from driver age.')

		return data


# anonymous customer info (required also for quote)
class CustomerBase(forms.Form):
	first_name = forms.CharField(label='First name', max_length=30)
	last_name = forms.CharField(label='Last name', max_length=30)
	telephone = PrefixPhoneNumber(label='Mobile number', prefix=PHONE_PREFIXES)
	email = forms.EmailField(label='E-mail address', help_text=' We use email address to identify the customers. Please make sure that your email is correct!')
	convictions = forms.ChoiceField(choices=no_claim_years(), label='Years of no claims')
	previous_insure = forms.CharField(label='Name of your previous insurance company', max_length=30, required=False)
	previous_insure_nr = forms.CharField(label='Number of your previous insurance policy', max_length=30, required=False)
	birth = forms.DateField(label='Date of birth')
	driver_license = forms.ChoiceField(label='Years FULL driving license held')

	def __init__(self, *args, **kwargs):
		super(CustomerBase, self).__init__(*args, **kwargs)
		self.fields['convictions'].initial = ('', '---')
		self.fields['birth'].widget = SelectDateWidget(years=(x for x in xrange(date.today().year-18, date.today().year-91, -1)), attrs={'class': 'form-control'})
		self.fields['driver_license'].choices = [('', '---')]+DRIVING_YEARS.items()


	def clean_telephone(self):
		data = self.cleaned_data['telephone']
		if not data.isdigit():
			raise forms.ValidationError('Phone number must contains only digits ok!.')
		elif len(data) != 10:
			raise forms.ValidationError('Phone number must have 10 digits (including prefix).')
		return data

	def clean(self):
		data = super(CustomerBase, self).clean()
		convictions = data.get('convictions', None)
		previous_insure = data.get('previous_insure', None)
		previous_insure_nrons = data.get('previous_insure_nr', None)
		if convictions and int(convictions) > 0:
			if len(previous_insure) == 0 or len(previous_insure_nrons) == 0:
				raise forms.ValidationError('If you want to get "no claim" discount, you need to provide previous insurance company name and previous insurance policy number')
		return data

	def clean_dobnlic(self):
		#data = super(CustomerBase, self).clean()
		birth_data = self.cleaned_data['birth']
		license_data = self.cleaned_data['driver_license']

		#birth = data.get('birth', None)
		#license = data.get('driver_license', None)

		if birth_data and license_data:
			age = date.today().year - birth.year
			if int(license_data) > age:
				raise forms.ValidationError('Driver license held can not be longer, than life of driver.')
			if int(license_data) > (age-17):
				raise forms.ValidationError('Longer held driver license than it may result from driver age.')

		return data


class CustomerAdditional(forms.Form):
	landline = forms.CharField(label='Landline number', max_length=15, required=False)
	address = forms.CharField(label='Street address and number', max_length=265)
	town = forms.CharField(label='City/town', max_length=30)
	occupation = forms.CharField(label='Business or occupation', max_length=50)
	# birth = forms.DateField(label='Date of birth')
	# driver_license = forms.ChoiceField(label='Years FULL driving license held')

	def __init__(self, *args, **kwargs):
		super(CustomerAdditional, self).__init__(*args, **kwargs)
		# self.fields['birth'].widget = SelectDateWidget(years=(x for x in xrange(date.today().year-18, date.today().year-91, -1)), attrs={'class': 'form-control'})
		# self.fields['driver_license'].choices = [('', '---')]+DRIVING_YEARS.items()
		self.fields['address'].widget = forms.Textarea(attrs={'rows': 3})

	def clean_landline(self):
		data = self.cleaned_data['landline']
		if len(data) == 0:
			pass
		elif not data.isdigit():
			raise forms.ValidationError('Landline number must contains only digits.')
		return data

	def clean(self):
		data = super(CustomerAdditional, self).clean()
		birth = data.get('birth', None)
		license = data.get('driver_license', None)

		if birth and license:
			age = date.today().year - birth.year
			if int(license) > age:
				raise forms.ValidationError('Driver license held can not be longer, than life of driver.')
			if int(license) > (age-17):
				raise forms.ValidationError('Longer held driver license than it may result from driver age.')

		return data


class OwnershipForm(forms.ModelForm):
	class Meta:
		model = Ownership
		exclude = ['user']
		widgets = {
			'repair': forms.RadioSelect(),
			'changed': forms.RadioSelect(),
			'owner': forms.RadioSelect(),
			'loan': forms.RadioSelect(),
			'owner_address': forms.Textarea(attrs={'rows': 3}),
			'loan_info': forms.Textarea(attrs={'rows': 3}),
		}

	def __init__(self, *args, **kwargs):
		super(OwnershipForm, self).__init__(*args, **kwargs)
		self.fields['repair'].choices = self.fields['repair'].choices[1:]
		self.fields['changed'].choices = self.fields['changed'].choices[1:]
		self.fields['owner'].choices = self.fields['owner'].choices[1:]
		self.fields['loan'].choices = self.fields['loan'].choices[1:]

	def clean(self):
		data = super(OwnershipForm, self).clean()
		owner = data.get('owner', None)
		loan = data.get('loan', None)
		if owner == 1 and not len(data['owner_address']) > 0:
			raise forms.ValidationError('If you are not the owner please provide owner data.')
		if loan == 0 and not len(data['loan_info']) > 0:
			raise forms.ValidationError('If loan was obtained, please provide lender data.')

		return data

	def clean_vin(self):
		data = self.cleaned_data['vin']
		if len(data) != 17:
			raise forms.ValidationError('Invalid value! Chassis number must have 17 characters long.')
		return data


class Miscellaneous(forms.ModelForm):
	class Meta:
		model = Misc
		exclude = ['user']
		widgets = {
			'accident': forms.RadioSelect(),
			'defect': forms.RadioSelect(),
			'previous_insure': forms.RadioSelect(),
			'declined_proposal': forms.RadioSelect(),
			'carry_loss': forms.RadioSelect(),
			'special_conditions': forms.RadioSelect(),
			'refuse_renew': forms.RadioSelect(),
			'cancelled_policy': forms.RadioSelect(),
		}

	def __init__(self, *args, **kwargs):
		super(Miscellaneous, self).__init__(*args, **kwargs)
		self.fields['accident'].choices= self.fields['accident'].choices[1:]
		self.fields['defect'].choices = self.fields['defect'].choices[1:]
		self.fields['previous_insure'].choices = self.fields['previous_insure'].choices[1:]
		self.fields['declined_proposal'].choices = self.fields['declined_proposal'].choices[1:]
		self.fields['carry_loss'].choices = self.fields['carry_loss'].choices[1:]
		self.fields['special_conditions'].choices = self.fields['special_conditions'].choices[1:]
		self.fields['refuse_renew'].choices = self.fields['refuse_renew'].choices[1:]
		self.fields['cancelled_policy'].choices = self.fields['cancelled_policy'].choices[1:]

		# validation will be performed only if "previous_insure" field value will be 0 (YES)
		if self.data and self.data.get('previous_insure') == u'0':
			self.fields.get('declined_proposal').required = True
			self.fields.get('carry_loss').required = True
			self.fields.get('special_conditions').required = True
			self.fields.get('refuse_renew').required = True
			self.fields.get('cancelled_policy').required = True
		else:
			self.fields.get('declined_proposal').required = False
			self.fields.get('carry_loss').required = False
			self.fields.get('special_conditions').required = False
			self.fields.get('refuse_renew').required = False
			self.fields.get('cancelled_policy').required = False

	def clean(self):
		data = super(Miscellaneous, self).clean()
		if data.get('previous_insure', False):
			data['declined_proposal'] = 1
			data['carry_loss'] = 1
			data['special_conditions'] = 1
			data['refuse_renew'] = 1
			data['cancelled_policy'] = 1
		return data


class Confirm(forms.Form):
	correct = forms.BooleanField(required=True)


#Traveller form
class Traveller(forms.Form):
	first_name = forms.CharField(label='First name', max_length=30)
	last_name = forms.CharField(label='Last name', max_length=30)
	address = forms.CharField(label='Street address and number', max_length=265)
	occupation = forms.CharField(label='Business or occupation', max_length=50)
	birth = forms.DateField(label='Date of birth')
	telephone = PrefixPhoneNumber(label='Mobile number', prefix=PHONE_PREFIXES)
	email = forms.EmailField(label='E-mail address')
	passport_num = forms.CharField(label='Passport Number', max_length=30)
	

	def __init__(self, *args, **kwargs):
		super(Traveller, self).__init__(*args, **kwargs)
		self.fields['birth'].widget = SelectDateWidget(years=(x for x in xrange(date.today().year-18, date.today().year-91, -1)), attrs={'style': 'width:8em'})
		self.fields['address'].widget = forms.Textarea(attrs={'rows': 3})

	def clean_telephone(self):
		data = self.cleaned_data['telephone']
		if not data.isdigit():
			raise forms.ValidationError('Phone number must contains only digits.')
		elif len(data) != 10:
			raise forms.ValidationError('Phone number must have 10 digits (including prefix).')
		return data


# anonymous traveller info (required also for quote)
class TravellerBase(forms.Form):
	first_name = forms.CharField(label='First name', max_length=30)
	last_name = forms.CharField(label='Last name', max_length=30)
	passport_num = forms.CharField(label='Passport Number', max_length=30)
	occupation = forms.CharField(label='Occupation', max_length=30)
	telephone = PrefixPhoneNumber(label='Mobile number', prefix=PHONE_PREFIXES)
	email = forms.EmailField(label='E-mail address', help_text=' This will server as your login id.')
	 
	def __init__(self, *args, **kwargs):
		super(TravellerBase, self).__init__(*args, **kwargs)


	def clean_telephone(self):
		data = self.cleaned_data['telephone']
		if not data.isdigit():
			raise forms.ValidationError('Phone number must contains only digits.')
		elif len(data) != 10:
			raise forms.ValidationError('Phone number must have 10 digits (including prefix).')
		return data






class Trip_Form(forms.Form):
	COUNTRY_CHOICES = tuple(countries)
	#type_of_policy = forms.ChoiceField(choices=COVER,widget=forms.RadioSelect(),
		#help_text="Single Trip policies offer cover for one short-duration trip while Annual Multi-Trip policies cover multiple short-duration trips throughout the year.")
	#destination = forms.ChoiceField(choices=list(countries), initial='US')
	#residence = forms.ChoiceField(choices=list(countries), initial='GH')
	#currency = forms.CharField(label="Preferred Currency",max_length=30)

	#xCanada = forms.BooleanField(label="Type of trip" , required=False)
	
	#trip_type = forms.CharField(label='Cover Required', max_length=30)
	
	# destination = forms.IntegerField(widget=forms.Select(choices=DESTINATION_SELECT_FORM),label='Destination', help_text=' Worldwide select: excludes USA, Australia, Japan and Canada.')
	#destination = forms.IntegerField(widget=forms.Select(choices=DESTINATION_SELECT_FORM),label='Destination')
	destination = forms.ChoiceField(choices=COUNTRY_CHOICES, required=True)
	#destination = forms.IntegerField(widget=forms.Select(choices=COUNTRY_CHOICES),label='Destination')
	cover_type = forms.IntegerField(widget=forms.Select(choices=TYPE_OF_COVER_SELECT_FORM),label='Type of Cover')


	trip_start = forms.DateField(label='Trip Start Date',required=True)
	trip_end = forms.DateField(label='Trip End Date',required=False)
	
	#trip_end = forms.DateField(widget=DateTimePicker(options={"format": "YYYY-MM-DD", "pickTime": False}))	


	#number_of_travellers = forms.ChoiceField(choices=traveller_counter(), label='Number of Travellers')
	#dob = forms.DateField(label='Date of birth')
	traveller_type = forms.IntegerField(widget=forms.Select(choices=TYPE_TRAVELLER_SELECT_FORM),label='Traveller')

	dob = forms.DateField(label='Date of Birth',widget=DateTimePicker(options={"format": "YYYY-MM-DD","pickTime": False}))
	#USandCanada = forms.BooleanField(label="Trip includes destinations in USA/Canada?" , required=False)
	#USandCanada = forms.BooleanField(label="Trip includes destinations in USA/Canada?" , required=False)
	

	


	#checks of dates
	def __init__(self, *args, **kwargs):
		super(Trip_Form, self).__init__(*args, **kwargs)
		self.fields['trip_start'].widget = SelectDateWidget(years=(x for x in xrange(date.today().year+1, date.today().year-1, -1)), attrs={'class': 'form-control'})
		self.fields['trip_end'].widget = SelectDateWidget(years=(x for x in xrange(date.today().year+1, date.today().year-1, -1)), attrs={'class': 'form-control'})
		self.fields['dob'].widget = SelectDateWidget(years=(x for x in xrange(date.today().year-1, date.today().year-86, -1)), attrs={'class': 'form-control'})
		

	def clean(self):
		data = super(Trip_Form, self).clean()
		trip_start = data.get('trip_start', None)
		trip_end = data.get('trip_end', None)
		birth_date = data.get('dob',None)

		if trip_start and trip_end:
			if trip_start <= date.today():
				raise forms.ValidationError('Policy must be purchased ahead of trip.')

			elif trip_end <= trip_start:
				#print('there will be a problem')
				raise forms.ValidationError('Trip end date cannot be before trip start date.')

			return data






class TravelPolicy_Choices(forms.Form):

    # q_type = forms.ChoiceField(
    #     choices=QUOTE_TYPE_MAP.iteritems(),
    #     widget=forms.RadioSelect(),
    #     label='Quote type',
    # )

    def __init__(self, choices, *args, **kwargs):
        super(TravelPolicy_Choices, self).__init__(*args, **kwargs)
        print(forms.Form,type(forms.Form))

        # choices are provide from view!
        self.fields['policy_id'] = forms.ChoiceField(choices=choices, widget=forms.RadioSelect())

