#coding: utf-8
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

local_urls = 'dip.%s.urls' % settings.COUNTRY

urlpatterns = patterns('',
    url(r'^', include('dip.core.urls')),
    url(r'^', include('dip.admin.urls')),
    url(r'^%s/' % settings.COUNTRY, include(local_urls)),
)+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)