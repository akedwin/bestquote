from dip.settings import *
from FQ.conf import settings as FQ_SETTINGS

FQ_SETTINGS.TRANSPORT = 'redis'

#for local
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'bq',
        'USER': 'bq',
        'PASSWORD': 'Quiud2to',
        'HOST':'localhost',
        'PORT':'5432',
    }
}

#for Heroku
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': 'd7kgu7vucivhug',
#         'USER': 'wfnfcrdmgkljch',
#         'PASSWORD': '4P1EiZt7p6CAaKFWurX44I74lG',
#         'HOST':'ec2-54-197-255-248.compute-1.amazonaws.com',
#         'PORT':'5432',
#     }
# }


PAYALL_WEB_PAYMENT = 'https://pay.ghana.payall.ch/new-payment'

CURRENCY = 'GHS'
COUNTRY = 'ghana'

INSTALLED_APPS += ('ghana','django_countries')

DOMAIN = 'https://bestquotehome.com'

HASHIDS_SALT = "yux2tnWEKJG@R$iu4y5t98y6cn25x2xv28xr$!#q726e&^(5cv16"

ADMINS = (("Nana", "naofori@gmail.com"),)
DEBUG = True

STATIC_ROOT = '/home/bestquote/bestquote-static/static'
STATICFILES_DIRS = (
    '/home/bestquote/bestquote/static',
)

# PAYALL WEB PAYMENT MODULE URL
PAYALL_WEB_PAYMENT = 'https://pay.ghana.payall.ch/new-payment'

# WEB PAYMENT MODULE CREDENTIALS
DIP_STORE_ID = '8b47479ea0d04aaeb8143b1515c07968'
DIP_CLIENT_SALT = 'cefb4e19a85108b3c5a838956981e0d63edc7ad460a5c52550b6598b'
DIP_SERVER_SALT = '3593780f180919ba352fbd66c57d77a77f9d96468c4897750ae391c8'
