/*!
 * BestQuote javascript features
 */
$(function(){
	/*
	 * Filter only numbers
	 */
	$('#id_value').bind('keyup change click', function(){
		if (/\D/g.test(this.value)) {
			this.value = this.value.replace(/\D/g, '');
		}
	});
	/*
	 * Bootstrap tooltips
	 */
	$("[rel='tooltip']").tooltip();
	/*
	 * Insurance details inputs show/hide
	 */
	$('#id_convictions')
		.change(function(){
			if (parseInt($('option:selected', this).val()) > 0) {
				$('#id_previous_insure, #id_previous_insure_nr').parent().parent().fadeIn(250);
			} else {
				$('#id_previous_insure, #id_previous_insure_nr').parent().parent().hide();
			}
			return false;
		})
		.trigger('change');

	/*
	Trigger for date hider
	*/
	$('#id_cover_type')
		.change(function(){
			if (parseInt($('option:selected', this).val()) == 1) {
				$('#id_trip_start_day,#id_trip_start_month,#id_trip_start_year').parent().parent().fadeIn(250);
				$('#id_trip_end_day,#id_trip_end_month,#id_trip_end_year').parent().parent().hide();
			}

			else if (parseInt($('option:selected', this).val()) > 1) {
				$('#id_trip_start_day,#id_trip_start_month,#id_trip_start_year, #id_trip_end_day,#id_trip_end_month,#id_trip_end_year').parent().parent().fadeIn(250);

			}

			// else {
			// 	$('#id_trip_start_day,#id_trip_start_month,#id_trip_start_year, #id_trip_end_day,#id_trip_end_month,#id_trip_end_year').parent().parent().hide();
			// }
			return false;
		})
		.trigger('change');
	/*
	 * Optional form inputs show/hide
	 */
	$('.optional[rel]')
		.each(function(){
			var $optional = $(this);
			$('input[name="' + $optional.attr('rel') + '"]')
				.click(function(){
					var selected = parseInt($(this).val()) > 0;
					$optional.hide().filter(function(){
						if ((selected && $optional.is('.ifunchecked')) ||
								( ! selected && $optional.is('.ifchecked'))) {
							return true;
						}
					}).fadeIn(250);
				})
				.filter(':checked')
					.trigger('click');
		});
	/*
	 * Select boxes replacement plugin
	 */
	$('select').not('#id_make_of_vehicle,#id_body_type')
		.selectBoxIt();
	/*
	 * Radio buttons styling (replace with icons)
	 */
	$('.form-control')
		.change(function(){
			$(this)
				.closest('.form-group')
					.removeClass('has-error');
		});
	$(':radio')
		.hide()
		.parent('label')
			.addClass('clickable')
			.prepend('<span class="fa fa-circle-o fa-lg"></span>')
			.click(function(){
				$(this)
					.closest('.form-group')
						.removeClass('has-error')
						.find('label .fa')
							.attr('class', 'fa fa-circle-o fa-lg')
							.end()
						.end()
					.prev(':radio')
						.click()
						.end()
					.find('.fa')
						.attr('class', 'fa fa-check-circle fa-lg');
			})
			.end()
		.filter(':checked')
			.parent('label')
				.trigger('click');
	/*
	 * Checkbox buttons styling (replace with icons)
	 */
	$(':checkbox')
		.hide()
		.closest('.form-group')
			.find('label')
				.addClass('clickable')
				.prepend('<span class="fa fa-square-o fa-lg"></span> ')
				.click(function(){
					$(this)
						.closest('.form-group')
							.find(':checkbox')
								.prop('checked', function(){
									return $(this).prop('checked');
								})
								.end()
							.find('label .fa')
								.attr('class', function(i, value){
									if (value == 'fa fa-check-square fa-lg') {
										return 'fa fa-square-o fa-lg'
									} else {
										return 'fa fa-check-square fa-lg'
									}
								});
				})
				.end()
			.filter(':checked')
				.trigger('click');
	/*
	 * Sign in password field enable/disable
	 */
	$('.signin :radio')
		.parent('label')
			.click(function(){
				if ($('.signin :radio:checked').val() == 'registration') {
					console.log('enabled');
					$('#id_password').attr({disabled: true});
				} else {
					console.log('disabled');
					$('#id_password').attr({disabled: false});
				}
			})
			.filter(':checked')
				.trigger('click');
	/*
	 * Sliders
	 */
	$(':text.slider')
		.attr('data-slider-value', function(i, value){
			return $(this).prop('value');
		})
		.slider({
			'tooltip': 'hide'
		})
		.on('slide', function(ev){
			$(ev.target).parent().prev().find('span').text($(ev.target).prop('value'));
		})
		.trigger('slide');
	/*
	 * Rank table radio buttons replacement
	 */
	$('form .rank button')
		.click(function(){
			$(this)
				.closest('table')
					.find('.selected')
						.removeClass('selected')
						.end()
					.end()
				.closest('td')
					.addClass('selected')
						.closest('tr')
							.find(':radio')
								.prop('checked', 'checked')
								.end()
							.addClass('selected')
								.siblings()
									.removeClass('selected');
			option = ($(this).closest('.quote').index('.quote') % 2) + 1;
			$('.payment-option :radio[value="' + option + '"]')
				.prop('checked', 'checked');
		});
	$('form .rank :radio:checked').closest('tr').find('.quote').eq(parseInt($('.payment-option :checked').val()) - 1).find('button').trigger('click');
	$('input[name="tpl"]')
		.change(function(){
			if (parseInt($('input[name="tpl"]:checked').val()) == 0) {
				$('#id_tpl_amount').parent().parent().fadeIn(250);
			} else {
				$('#id_tpl_amount').parent().parent().hide();
			}
			return false;
		})
		.trigger('change');
	if ($('#id_make_of_vehicle').length && $('#id_body_type').length) {
		$('#id_make_of_vehicle,#id_body_type').select2();
	};
});