/*!
 * BestQuote javascript tool to operate with forms
 */

 $(function(){
	// initial fields
	var fields = {}; 
	// get current page url
	var currentPage = "";
	if (typeof this.href != "undefined") {
		currentPage = this.href.toString().toLowerCase(); 
	} else { 
		currentPage = document.location.toString().toLowerCase();
	}
	// sets of field's id in each step
	// true - required; false  not required
	if (currentPage.endsWith('step1')) {
		fields = {
			'#id_first_name': true,
			'#id_last_name': true,
			'#id_telephone_0': true,
			'#id_telephone_1': true,
			'#id_email': true,
			'#id_previous_insure': false,
			'#id_previous_insure_nr': false,
			'#id_value': true,
			'#id_usage': true,
			'#id_tpl_amount': false,
			'input[name="tpl"]:checked': true,
			'#id_birth_day': true,
			'#id_birth_month': true,
			'#id_birth_year': true,
			'#id_driver_license': true,
		};
	}
	if (currentPage.endsWith('step2')) {
		fields = {'input[name="company"]:checked': true};
	}
	if (currentPage.endsWith('step3')) {
		fields = {
			'#id_address': true,
			'#id_town': true,
			'#id_occupation': true,
			// '#id_birth_day': true,
			// '#id_birth_month': true,
			// '#id_birth_year': true,
			// '#id_driver_license': true,
			'#id_vrp': true,
			'#id_vin': true,
			'#id_make_of_vehicle': true,
			'#id_body_type': true,
			'input[name="repair"]:checked': true,
			'input[name="changed"]:checked': true,
			'input[name="owner"]:checked': true,
			'#id_owner_address': false,
			'input[name="loan"]:checked': true,
			'#id_loan_info': false,
			'input[name="accident"]:checked': true,
			'input[name="defect"]:checked': true,
			'input[name="previous_insure"]:checked': true,
			'input[name="declined_proposal"]:checked': false,
			'input[name="carry_loss"]:checked': false,
			'input[name="special_conditions"]:checked': false,
			'input[name="refuse_renew"]:checked': false,
			'input[name="cancelled_policy"]:checked': false,
		}
	}
	if (currentPage.endsWith('step4') || currentPage.endsWith('step3_t')) {
		fields = {'input[name="correct"]:checked': true};
	}
	// validate all defined fields in form
	function validateFields() { 
		$.each(fields, function(key, val) {
			if (val == true) {
				$('.next_step').attr('disabled', 'disabled');
				if ($(key).val() == "" || typeof ($(key).val()) == "undefined" ) return false;
				$('.next_step').removeAttr("disabled");
			}
		});
	}

	/*
	* Travel - Date hider logic
	*/
	$("#id_cover_type").change(function() {
		  // alert($("#id_cover_type").val());
		if ($("#id_cover_type").val().toString() == "1"){
			fields['#id_trip_start_day'] = true;
			fields['#id_trip_start_month'] = true;
			fields['#id_trip_start_year'] = true;
			// fields['#id_trip_end'] = true;
			fields['#id_trip_end_day'] = false;
			fields['#id_trip_end_month'] = false;
			fields['#id_trip_end_year'] = false;
			validateFields();
		}
		else{
			fields['#id_trip_start_day'] = true;
			fields['#id_trip_start_month'] = true;
			fields['#id_trip_start_year'] = true;
			fields['#id_trip_end_day'] = true;
			fields['#id_trip_end_month'] = true;
			fields['#id_trip_end_year'] = true;
			validateFields();
		}

		$('#id_cover_type').blur();
	});

	/*
	* Motor wizard step 1 logic
	*/
	$("#id_convictions").change(function() {
		if ($("#id_convictions").val().toString() != "0") {
			fields['#id_previous_insure'] = true;
			fields['#id_previous_insure_nr'] = true;
			validateFields();
		} else {
			fields['#id_previous_insure'] = false;
			fields['#id_previous_insure_nr'] = false;
			validateFields();
		}
		$('#id_convictions').blur();
	});
	$('input[name="tpl"]').change(function() {
		// in this case 0 means yes, 1 means no
		if ($('input[name="tpl"]:checked').val().toString() == "0") {
			fields['#id_tpl_amount'] = true;
			validateFields();
		} else {
			fields['#id_tpl_amount'] = false;
			validateFields();
		}
		$('input[name="tpl"]').blur();
	});
	/*
	* Motor wizard step 2 logic
	*/
	$('form .rank button').click(function() {
		$('.next_step').removeAttr("disabled");
	});
	/*
	* Motor wizard step 3 logic
	*/
	$('input[name="owner"]').change(function() {
		// in this case 1 means no
		if($('input[name="owner"]:checked').val().toString() == "1") {
			fields['#id_owner_address'] = true;
			validateFields();
		} else {
			fields['#id_owner_address'] = false;
			validateFields()
		}
		$('input[name="owner"]').blur();
	});
	$('input[name="loan"]').change(function() {
		if($('input[name="loan"]:checked').val().toString() == "0") {
			fields['#id_loan_info'] = true;
			validateFields();
		} else {
			fields['#id_loan_info'] = false;
			validateFields();
		}
		$('input[name="loan"]').blur();
	});
	$('input[name="previous_insure"]').change(function() {
		if($('input[name="previous_insure"]:checked').val().toString() == "0") {
			fields['input[name="declined_proposal"]:checked'] = true;
			fields['input[name="carry_loss"]:checked'] = true;
			fields['input[name="special_conditions"]:checked'] = true;
			fields['input[name="refuse_renew"]:checked'] = true;
			fields['input[name="cancelled_policy"]:checked'] = true;
			validateFields();
		} else {
			fields['input[name="declined_proposal"]:checked'] = false;
			fields['input[name="carry_loss"]:checked'] = false;
			fields['input[name="special_conditions"]:checked'] = false;
			fields['input[name="refuse_renew"]:checked'] = false;
			fields['input[name="cancelled_policy"]:checked'] = false;
			validateFields();
		}
		$('input[name="previous_insure"]').blur();
	});
	// validate fields after page load to disable button or
	// leave it active if fielda are already filled (refreshing page etc)
	validateFields();
	// check all fields in form and unlock button if they are valid
	$('form').change(validateFields);
});