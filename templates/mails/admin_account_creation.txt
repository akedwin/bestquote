{% extends "mails/base.txt" %}
{% block text %}
Hello {{ full_user_name }}

Your special administrator account has been created, but is inactive!
If you want check or manage your data, please activate your account using link provided below:

{{ activation_url }}

If you were not excepting this message, please contact: info@{{ domain }}

Best regards,
your BestQuote team
{% endblock %}