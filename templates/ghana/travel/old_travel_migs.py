# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
#coding: utf-8
from __future__ import unicode_literals
from uuid import uuid4
from moneyed import Money

from django.http import HttpResponseRedirect
from django.http import Http404
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.conf import settings

from dip.core.models import Company
from dip.core.models import Invoice
from dip.core.models import User

from dip.ghana.models import AnonymousTraveller
from dip.ghana.models import TravellerInfo

from dip.ghana.models import Ownership as Own
from dip.ghana.models import Misc
from dip.ghana.models import QuoteDetails
#from dip.ghana.models import trip_form
#from dip.ghana.models.misc import YES_NO_MAP
#from dip.ghana.models import policy


from dip.ghana.forms import TravellerBase
from dip.ghana.forms import CustomerAdditional
from dip.ghana.forms import OwnershipForm
from dip.ghana.forms import Miscellaneous
from dip.ghana.forms import Confirm

from dip.ghana.forms import Trip_Form
from dip.ghana.forms import TravelPolicy_Choices

from dip.ghana.travel_algorithm import getTQuote
from dip.ghana.travel_algorithm import policy_details

#from dip.ghana.travel_algorithm import getbank


#from dip.ghana.motor_algorithm import quote_values

from dip.utils import checksum
from dip.utils import no_admin_allowed
from dip.utils.common import hash_decode

import urllib
import urllib2
import hashlib
import hmac
import md5
from urlparse import urlparse
from urllib import urlencode
import random

import FQ

#ammend this for travel
# this function collect all user choices and eventually translate it to human readable form
def translate_user_choices(req, cc):
    # get vehicle cleaned data
    step1 = req.session['trip']
    # because vehicle value and TPL walue are integers we need to convert them into Money object
    # if step1.get('trip_start', None):
    #     step1['tpl_amount'] = Money(step1['tpl_amount'], settings.CURRENCY)
    #step1['value'] = Money(step1['value'], settings.CURRENCY)
    
    # update the dict with customer base data from the same step
    step1.update(req.session['traveller_base'])

    step2 = {
        'company': Company.objects.get(pk=req.session['quote_info'][4]),
        # 
        'value': req.session['quote_info'][0],
        'medical' : req.session['quote_info'][1]
    }
    vpc_MerchTxnRef = str(random.randint(0,9))

    trans_data = {
        'vpc_AccessCode' : '53378107',
        'vpc_Amount' : '100',
        'vpc_Command' : 'pay',
        'vpc_Locale' : 'en',
        'vpc_MerchTxnRef' : vpc_MerchTxnRef,
        'vpc_Merchant' : 'GTB109153A01',
        'vpc_OrderInfo' : 'bqt98',
        'vpc_ReturnURL' : 'http://bestquotehome.com',
        'vpc_Currency' : 'GHS',
        'vpc_Version' : '1',

    }

    #print(req.session['quote_info'][4])

# get quote selection data
    # c_pk = req.session['policies']['company']
    # user_select = int(req.session['quote']['q_type'])
    # q_info = req.session['quote_info']
    # step2 = {
    #     'company': Company.objects.get(pk=c_pk),
    #     # get Standard quote value if 1 or Excess quote when 2
    #     'value': q_info[1] if user_select == 2 else q_info[0]
    # }


    # update context
    cc['Step1'] = step1
    cc['Step2'] = step2
    cc['trans_data'] = trans_data
    
    #cc['Step3'] = step3
    return cc

def finalize_proposal(req):
    #get or create user
    user = req.session['traveller_base']
    #_user, created_user = User.objects.get_or_create(email=user['email'])
    print(user)


@no_admin_allowed
def travel_start(req):
    return HttpResponseRedirect(reverse('travel_step1'))


@no_admin_allowed
def travel_step1(req):
    cc = RequestContext(req) 
    if req.method == 'POST':
        trip = Trip_Form(req.POST)
        traveller = TravellerBase(req.POST)
        if trip.is_valid() and traveller.is_valid():
            cdata = traveller.cleaned_data
            tdata = trip.cleaned_data

            #comment here
            if not req.user.is_authenticated():
                auser, created = AnonymousTraveller.objects.get_or_create(email=cdata['email'], defaults=cdata)
                if created:
                    for key, value in cdata.iteritems():
                        setattr(auser, key, value)
                auser.save()
            # save vehicle form to session (save is in last step to avoid creating fake records)
            #print(tdata, type(tdata))
            req.session['trip'] = tdata
            req.session['traveller_base'] = cdata
            req.session['step2'] = True

            return HttpResponseRedirect(reverse('travel_step2'))

    else:
        #trip_data = req.session.get('trip', {})
        traveller_base_data = req.session.get('traveller_base', {})
        if req.user.is_authenticated() and len(traveller_base_data) == 0:
            user = req.user
            profile = user.profile
            initial = {
                'first_name': user.first_name,
                'last_name': user.last_name,
                'email': user.email,
                'telephone': profile.telephone,
                'occupation': profile.occupation,
                'passport_num': profile.passport_num,
                #'convictions': profile.convictions,
                #'previous_insure': profile.previous_insure,
                #'previous_insure_nr': profile.previous_insure_nr,
            }
        else:
            initial = traveller_base_data
        traveller = TravellerBase(initial=initial)
        trip = Trip_Form(initial=req.session.get('trip', {}))
    
    
    cc['traveller'] = traveller
    cc['trip'] = trip
    return render_to_response("ghana/travel/step1.html", cc)


@no_admin_allowed
def travel_step2(req):
    cc = RequestContext(req)
    if not req.session.get('step2', None):
        return HttpResponseRedirect(reverse('travel_step1'))

    comp_list, policy_lis = getTQuote(req)
    #print(comp_list)
    #print(policy_lis)

    if req.method == 'POST':
        travel_insurance = TravelPolicy_Choices(comp_list, req.POST)
        #print(travel_insurance)
        #print('hello')

        if travel_insurance.is_valid():
            travel_data = travel_insurance.cleaned_data
            
            # push the chosen policy into the session
            req.session['trip_policy'] = travel_data
            #print(travel_data)
            
            #obtain id of policy
            travel_insurance = travel_data['company']
            tquote_details = req.session.get('policy_details')
            
            
            req.session['quote_info'] = tquote_details[int(travel_insurance)]
            #print(req.session['quote_info'][4])
            req.session['step3'] = True
            return HttpResponseRedirect(reverse('travel_step3_t'))

    else:
        travel_insurance = TravelPolicy_Choices(comp_list, initial=req.session.get('policy',{}))
        #print(travel_insurance)


    cc['company'] = travel_insurance
    
    cc['policies'] = zip(travel_insurance['company'], policy_details(policy_lis))
    #print(cc['policies'])
    #print(cc['company'])
    return render_to_response("ghana/travel/step2.html", cc)

@no_admin_allowed
def travel_step3(req):
    cc = RequestContext(req)
    if not req.session.get('step3', None):
        return HttpResponseRedirect(reverse('travel_step1'))

    if req.method == 'POST':
       form = Confirm(req.POST)
    #if not req.session.get('step2', None):
     #   return HttpResponseRedirect(reverse('travel_step1'))
     
     # if req.method == 'POSt':
     #    form = Confirm(req.POST)
     #    if form.is_valid():
     #        finalized = finalize_proposal(req)
     #        travelpolicy_data = {}
                #    'amount':
                #    'order-id':
                #    'customer_email':
                #    'firstname':
                #    'lastname':
                #    'phone':
                #    'occupation':
               # }

     #}

    else:
        form = Confirm()

    cc = translate_user_choices(req, cc)
    cc['form'] = form
    return render_to_response("ghana/travel/step3_t.html",cc)

def travel_step3_t(req):
    cc = RequestContext(req)
    if not req.session.get('step3', None):
        return HttpResponseRedirect(reverse('travel_step1'))

    if req.method == 'POST':
       form = Confirm(req.POST)
      
       
       del req.session['step2'],req.session['step3']
       
       
       url = 'https://migs-mtf.mastercard.com.au/vpcpay'
       # vpc_Version = '1'
       # vpc_Command  = 'pay'
       # vpc_AccessCode = '53378107'
       # vpc_MerchTxnRef = 'bq0012'
       # vpc_Merchant = 'GTB109153A01'
       # vpc_OrderInfo = 'bqtest12'
       # vpc_Amount = '6000'
       # vpc_Currency = 'USD'
       # vpc_Locale = 'en'
       # vpc_ReturnURL = 'bestquotehome.com'
       # vpc_SecureHashType = 'SHA256'
       
       #bq secret 
       secret_key = '6A4497DDFFFD6BF8F1919C8C6C4181DB'

       #ICS secret
       # secret_key ='300AB3542CD2EE320E9DD54376152DE7'
       
       
       # url_vals = secret_key + vpc_AccessCode+vpc_Amount + vpc_Command + vpc_Currency + vpc_Locale + vpc_MerchTxnRef + vpc_Merchant + vpc_OrderInfo + vpc_ReturnURL + vpc_Version
         
       #print(url_vals)
       
       
       # hashed_stuff =  hashlib.md5(url_vals).hexdigest()
       
       #2vpc_SecureHash ='1fcc49b14131c7e5e2336452e22d731e5f6c7f28588e0596056a43ba0b6e508a'

       vpc_MerchTxnRef = str(random.randint(0,9))

       

       sample_payment_data = {
            #BQ Code
            'vpc_AccessCode' : '53378107',
            'vpc_Amount' : '100',
            'vpc_Command' : 'pay',
            'vpc_Locale' : 'en',
            'vpc_MerchTxnRef' : vpc_MerchTxnRef,
            'vpc_Merchant' : 'GTB109153A01',
            'vpc_OrderInfo' : 'bqt98',
            'vpc_ReturnURL' : 'http://bestquotehome.com',
            'vpc_Currency' : 'GHS',
            'vpc_Version' : '1',
            #ICS Details
            # 'vpc_AccessCode' : '889B404F',
            # 'vpc_Merchant' : 'GTB105583A01',    

       }
       key_list = [
            'vpc_AccessCode',
            'vpc_Amount',
            'vpc_Command',
            'vpc_Locale',
            'vpc_MerchTxnRef',
            'vpc_Merchant',
            'vpc_OrderInfo',
            'vpc_ReturnURL',
            'vpc_Currency',
            'vpc_Version'
       ]
  #      for key in keys:
  # print key, myDict[key]

  # url_vals = key + vpc_AccessCode+vpc_Amount + vpc_Command + vpc_Currency + vpc_Locale + vpc_MerchTxnRef + vpc_Merchant + vpc_OrderInfo + vpc_ReturnURL + vpc_Version
         
  #      #print(url_vals)
       
       
  #      hashed_stuff =  hashlib.md5(url_vals).hexdigest()

       hashed_stuff = secret_key
       test_data=""
       for key in key_list:
        #print(key, sample_payment_data[key])
        test_data += key + '=' + sample_payment_data[key] + '&'
        #test_data = urlencode(sample_payment_data[key])
        
        hashed_stuff += sample_payment_data[key]

       #print(test)
       #data = 'vpc_AccessCode=' + vpc_AccessCode + '&vpc_Amount=' + vpc_Amount + '&vpc_Command=' + vpc_Command + '&vpc_Currency=' +vpc_Currency + '&vpc_Locale=' + vpc_Locale + '&vpc_MerchTxnRef=' + vpc_MerchTxnRef + '&vpc_Merchant=' + vpc_Merchant + '&vpc_OrderInfo=' + vpc_OrderInfo + '&vpc_ReturnURL=' +vpc_ReturnURL + '&vpc_Version=' + vpc_Version
       # print(data)
       # print('divider')
       # print(test)


       hashed_stuff =  hashlib.md5(hashed_stuff).hexdigest()

       # final_url = url + '?' + data + '&' + 'vpc_SecureHash=' + hashed_stuff
       final_url = url + '?' + test_data + 'vpc_SecureHash=' + hashed_stuff
       #print(final_url)
       # request_url = urllib2.Request(url, test_data)

       # response = urllib2.urlopen(request_url)

       # the_page = response.read()

       print(final_url)
       
       return HttpResponseRedirect(travel_step4)

       

    else:
        form = Confirm()

    cc = translate_user_choices(req, cc)
    cc['form'] = form
    return render_to_response("ghana/travel/step3_t.html",cc)


@no_admin_allowed
def travel_step4(req):
    '''
        This view allow to pay with payall
    '''
    # if not req.session.get('step5', None):
    #     return HttpResponseRedirect(reverse('motor_step1'))

    # remove token for this step
    cc = RequestContext(req)
    cc = translate_user_choices(req, cc)
    return render_to_response("ghana/travel/step4.html", cc)
