from dip.settings import *
from FQ.conf import settings as FQ_SETTINGS

FQ_SETTINGS.TRANSPORT = 'redis'

#for local
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'bq',
        'USER': 'bq',
        'PASSWORD': 'test-bq',
        'HOST':'localhost',
        'PORT':'',
    }
}

#for Heroku
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': 'd7kgu7vucivhug',
#         'USER': 'wfnfcrdmgkljch',
#         'PASSWORD': '4P1EiZt7p6CAaKFWurX44I74lG',
#         'HOST':'ec2-54-197-255-248.compute-1.amazonaws.com',
#         'PORT':'5432',
#     }
# }


PAYALL_WEB_PAYMENT = 'https://test.pay.ghana.payall.ch/new-payment'

CURRENCY = 'GHS'
COUNTRY = 'ghana'

INSTALLED_APPS += ('ghana','django_countries')

DOMAIN = 'https://test-bestquote.payall.ch'

HASHIDS_SALT = "yux2tnWEKJG@R$iu4y5t98y6cn25x2xv28xr$!#q726e&^(5cv16"

ADMINS = (("Nana", "naofori@gmail.com"),)
DEBUG = True
