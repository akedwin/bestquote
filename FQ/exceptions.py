#coding: utf-8
from __future__ import unicode_literals


# base for all exceptions
class FQException(Exception): pass

# raised when async result is not ready yet
class ResultNotReady(FQException): pass

# Task not found
class FQTaskNotFound(FQException): pass

# internal FQ exceptions
class FQInternalException(FQException): pass

# external exception (coming from remote task)
class FQExternalException(FQException): pass


# remote exception handling


_EXCEPTIONS_DB = {}
from FQ.lib import SingletonCreator

class RemoteException(FQExternalException):
    def __init__(self, *args, **kwargs):
        FQExternalException.__init__(self, *args)
        if "name" in kwargs:
            self.name = kwargs['name']
        else:
            self.name = self.__class__.__name__


def __init__(self, *args):
    FQExternalException.__init__(self, *args)


def _get_remote_exception(service, name):
    global _EXCEPTIONS_DB
    key = str( service + "."+name )
    try:
        return _EXCEPTIONS_DB[key]
    except KeyError:
        pass
    # create new exception class
    classname = str( service+"_"+name )
    exc = type(classname, (RemoteException,), {
            "service":service,
            "name":name,
            "__init__": __init__
        })
    _EXCEPTIONS_DB[ key ] = exc
    return exc


class ExceptionCreator(object):

    def __getattr__(self, name):
        global _EXCEPTIONS_DB
        if name.startswith("_"):
            raise AttributeError("%s is not exist" % name)

        if hasattr(self, "_service"):
            return _get_remote_exception(self._service, name)
        else:
            E = ExceptionCreator()
            E._service = name
            return E

RX = ExceptionCreator()
