#coding: utf-8
from __future__ import unicode_literals, absolute_import
from . import SingletonCreator
from FQ.conf import settings
from .logger import LOG


class Serializer(object):

    __metaclass__ = SingletonCreator

    def __init__(self, method=None):
        if method is None:
            s = settings.SERIALIZER
        else:
            s = method
        if s==None:
            raise Exception("Serialization method is not defined in config")

        if s.lower()=="cbor":
            global cbor
            import cbor
            setattr(self, "serialize", cbor.dumps )
            setattr(self, "deserialize", cbor.loads )

        else:
            raise Exception("Unknown serialization method defined %r", s)


def init_serializer():
    global serializer
    serializer = Serializer()

