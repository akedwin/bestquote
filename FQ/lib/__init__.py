#coding: utf-8

class SingletonCreator(type):
    """
    Convert class into singleton, by using __metaclass__ attribute.

    Example of use:

    :: code-block: python
        class MyClass(object):
            __metaclass__ = SingletonCreator
            def __init__(self,...):
                ...
    """
    def __call__(cls, *args, **kwargs):
        try:
            return cls.__instance
        except AttributeError:
            cls.__instance = super(SingletonCreator, cls).__call__(*args, **kwargs)
            return cls.__instance
