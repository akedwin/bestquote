#coding: utf-8
from __future__ import unicode_literals, absolute_import
from FQ.conf import settings
from FQ.lib import SingletonCreator
import logging, sys


_levels = {
    'DEBUG':logging.DEBUG,
    'INFO':logging.INFO,
    'WARNING':logging.WARNING,
    'ERROR':logging.ERROR,
    'CRITICAL':logging.CRITICAL,
}


class stdLogOut(object):
    """
    Catch std out/err and redirects it to log
    """
    def __init__(self, logger, level):
        self.logger = logger
        self.level = _levels[level]

    def write(self, msg):
        msg = msg.strip()
        if len(msg)>0:
            self.logger.log(self.level, "> " + msg)

    def flush(self):
        for handler in self.logger.handlers:
            handler.flush()



class LogProxy(object):

    __metaclass__ = SingletonCreator

    def __init__(self):
        self.__STDERR = sys.stderr
        self.__STDOUT = sys.stdout
        self.stetupLogger()

    def stetupLogger(self, name=None):
        if name is None:
            name = settings.LOGGER_NAME

        sys.stderr = self.__STDERR
        sys.stdout = self.__STDOUT
        logger = logging.getLogger(name)

        # log level
        ll = settings.LOG_LEVEL.upper()
        try:
            logger.setLevel(_levels[ll])
        except KeyError:
            raise Exception ("Invalid log level in config %s" % ll)

        # wyjście
        if settings.LOG_TO_FILE:
            # logowanie do pliku
            ch = logging.FileHandler(settings.LOG_FILE_NAME, encoding="utf-8")
        else:
            # logowanie na wyjście
            ch = logging.StreamHandler(stream=sys.stderr)

        # format wyjścia
        formatter = logging.Formatter('%(asctime)s [%(name)s] %(levelname)s: %(message)s')
        ch.setFormatter(formatter)

        logger.addHandler(ch)
        self._log = logger

        # catching stdout
        if settings.LOG_CATCH_STDOUT:
            self.__stdout_catcher = stdLogOut(self._log, settings.LOG_STDOUT_LEVEL)
            sys.stdout = self.__stdout_catcher

        # catching stderr
        if settings.LOG_CATCH_STERR:
            self.__stderr_catcher = stdLogOut(self._log, settings.LOG_STDERR_LEVEL)
            sys.stderr = self.__stderr_catcher

    def __getattr__(self, a):
        return self._log.__getattribute__(a)

    def mute(self):
        self._log.disabled = True

    def unmute(self):
        self._log.disabled = False

LOG = LogProxy()
