#coding: utf-8
from __future__ import unicode_literals, absolute_import
from decimal import Decimal
from datetime import date, time, datetime, timedelta
#_IS_PY3 = sys.version_info[0] >= 3


__all__ = ("encode_types", "decode_types")


def encode_types(data):
    # check incoming data type
    dt = type(data)

    # encode dict, list, tuple
    if dt==dict:
        res = {}
        for k,v in data.iteritems():
            res[k] = encode_types(v)
        return res
    elif dt==list:
        res = []
        for v in data:
            res.append( encode_types(v) )
        return res
    elif dt==tuple:
        res = []
        for v in data:
            res.append( encode_types(v) )
        return tuple(res)

    # encodable data types
    try:
        label, funcin, funcout = TYPES[dt]
    except KeyError:
        return data
    # convert data
    data = funcin(data)
    prf = PREFIX % label
    return prf+data

def decode_types(data):
    dt = type(data)
    # encode dict, list, tuple
    if dt==dict:
        res = {}
        for k,v in data.iteritems():
            res[k] = decode_types(v)
        return res
    elif dt==list:
        res = []
        for v in data:
            res.append( decode_types(v) )
        return res
    elif dt==tuple:
        res = []
        for v in data:
            res.append( decode_types(v) )
        return tuple(res)

    # data not convertable
    if not dt is str:
        return data
    if len(data)<6:
        return

    # check data prefix
    if (data[0]!=PREFIX[0]) or (data[5]!=PREFIX[3]):
        return data
    # get type label
    name = data[1:5]
    # convert back to type
    try:
        label, funcin, funcout = TYPES[ TYPES_LABELS[name] ]
    except KeyError:
        return data
    try:
        return funcout( data[6:] )
    except:
        return data


# conversion functions
# --------------------


def _decode_date(data):
    d = data.split("-",3)
    try:
        return date( int(d[0]), int(d[1]), int(d[2]) )
    except:
        return data

def _decode_time(data):
    h,m,s = data.split(":",2)
    try:
        s,ms = s.split(".",1)
    except ValueError:
        ms = 0
    return time( int(h), int(m), int(s), int(ms) )

def _encode_datetime(data):
    return data.isoformat()

def _decode_datetime(data):
    return datetime.strptime(data, "%Y-%m-%dT%H:%M:%S.%f")

def _encode_timedelta(td):
    return b"%i:%i:%i" % (td.days, td.seconds, td.microseconds)

def _decode_timedelta(td):
    d,s,ms = td.split(":",2)
    return timedelta( int(d), int(s), int(ms))




# list of convertable structures
# key - type of data
# value - tuple of 3 items
#   - 4 bytes long data type label
#   - function converting data to string (or bytes)
#   - function converting data back to required type
TYPES = {
    Decimal   : ( b"dcml", str, Decimal ),
    date      : ( b"date", str, _decode_date ),
    time      : ( b"time", str, _decode_time ),
    datetime  : ( b"dtim", _encode_datetime, _decode_datetime ),
    timedelta : ( b"tdlt", _encode_timedelta, _decode_timedelta ),
}
# -----------------------------
TYPES_LABELS = {}
for k,v in TYPES.iteritems(): TYPES_LABELS[v[0]] = k
PREFIX = bytes( chr(0x01) + "%s" + chr(0x01) )
