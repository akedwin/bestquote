#!/usr/bin/env python
#coding: utf-8
from __future__ import unicode_literals
from FQ import exceptions
from FQ.lib.typeconverter import decode_types


class Message(object):

    __slots__ = ("context","message","data")

    def __init__(self):
        pass


# serialization of tasks requests and results


def make_job_request_message(ctx, args, kwargs):
    """
    Create task request passed to workers
    """
    return {
        b'ctx':ctx,
        b'args':args,
        b'kwargs':kwargs,
    }

def decode_request_message(data):
    """
    Decode task request from client
    """
    return data[ b'ctx' ], data[ b'args' ], data[ b'kwargs' ]

def make_result_message(data, result_flags=None):
    """
    Serialize result of task/
    """
    res = {
        b'type' : b'res',
        b'data' : data,
    }
    if result_flags:
        res[b'flags'] = result_flags
    return res

def make_error_message(exc, tback, servicename):
    """
    Store exception data raised when processing task.
    """
    res = {
        b'type':b'err',
        b'data':{
            b'internal':False,
            b'traceback':tback,
            b'message':exc.message,
            b'name':exc.__class__.__name__,
            b'code':getattr(exc, "code", None),
        }
    }
    # RemoteException is special type exception
    # with will be rebuilded on client side dynamically
    # as separate class, before raising
    if isinstance(exc, exceptions.RemoteException):
        srv = getattr(exc, 'service', servicename )
        try:
            name = exc.name
        except AttributeError:
            pass
        else:
            res[b'data'][b'fq-service'] = srv
            res[b'data'][b'fq-name'] = name
    return res

def make_internal_error_message(msg):
    """
    Make message struct
    """
    return {
        b'type':b'err',
        b'data' : {
            b'internal':True,
            b'message':msg,
        }
    }

def decode_result_message(msg):
    """
    Decode result of task.
    Result can be exception or regular response
    """
    # additional result flags
    flags = msg.get(b'flags', () )

    # extendend data types was used for creating result
    if 'extypes' in flags:
        msg[b'data'] = decode_types( msg[b'data'] )

    # plain result
    if msg[b'type'] == b"res":
        return False, msg[b'data']

    # exceptions
    edata = msg[b'data']
    # internal exception
    if edata[b'internal']:
        exc = exceptions.FQInternalException( edata[b'message'] )
        exc.remote = True
        exc.code = None
        exc.traceback = None
        return True, exc

    # external exception
    if b'fq-service' in edata:
        # autmatically generated exception class
        ExcClass = exceptions._get_remote_exception(
            service = edata[b'fq-service'],
            name = edata[b'fq-name'])
        exc = ExcClass( edata[b'message'] )
        exc.name = edata[b'fq-name']
    else:
        # normal remote exception
        exc = exceptions.FQExternalException( edata[b'message'] )
        exc.name = edata[b'name']
    exc.remote = True
    exc.code = edata[b'code']
    exc.traceback = edata[b'traceback']

    return True, exc

