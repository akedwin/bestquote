#coding: utf-8
from __future__ import unicode_literals, absolute_import
#


def send_finish_request():
    request_finished.send(None)

def prepare_django():
    import django
    try:
        django.setup()
    except AttributeError:
        # django.setup is new in 1.7 version
        return

    from FQ.worker.task_proc import _add_callback_on_finish

    global request_finished
    from django.core.signals import request_finished
    _add_callback_on_finish( send_finish_request )
