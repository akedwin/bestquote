#coding: utf-8
from __future__ import unicode_literals
import codecs, os, sys
try:
    from ConfigParser import SafeConfigParser
except ImportError:
    from configparser import SafeConfigParser

__all__ = ("ModuleParser","SectionConfigParser","EnvParser","ArgumentParser","DjangoParser")


class DjangoConfNotFound(Exception): pass


class SectionConfigParser(SafeConfigParser):
    """
    ini file config parser
    """

    def __init__(self, filename, section):
        self.section = section
        SafeConfigParser.__init__(self)
        self.casesensitive = False
        try:
            with codecs.open(filename, "r", "utf_8") as f:
                self.readfp(f)
            self.file_loaded = True
        except IOError:
            self.file_loaded = False

    def __getitem__(self, key):
        return SafeConfigParser.get(self, self.section, key )

    def __setitem__(self, key, value):
        try:
            SafeConfigParser.set(self, self.section, key, value )
            return
        except:
            self.add_section(self.section)
            SafeConfigParser.set(self, self.section, key, value )

    # preserve case in key names
    def optionxform(self, optionstr):
        if self.casesensitive:
            return optionstr.lower()
        else:
            return optionstr

class EnvParser(object):
    """
    Settings provided by environment variables.
    """

    def __getitem__(self, k):
        k = "FQ_"+k.upper().strip()
        return os.environ[k]


class ArgumentParser(object):
    """
    Settings provided by args
    """
    def __init__(self):
        self.args = {}
        for arg in sys.argv[1:]:
            try:
                k,v = arg.split("=",1)
            except ValueError:
                continue
            # only uppercase values are settings
            if k.upper()!=k:
                continue
            self.args[ k.strip() ] = v.strip()

    def __getitem__(self, k):
        return self.args[k]


class ModuleParser(object):
    """
    Settings stored in python module (only uppercase values are used)
    """

    def __init__(self, modulename):
        # load module
        modname = modulename.split(".").pop()
        mod = __import__(bytes(modulename), fromlist=bytes(modname) )

        # loading default settings
        self.data = {}
        for k,v in mod.__dict__.items():

            if k in mod.__builtins__:
                continue
            if k.startswith("_"):
                continue
            if k.upper()!=k:
                continue
            self.data[k] = v

    def __getitem__(self, k):
        return self.data[k]


class DjangoParser(object):
    """
    Settings readed from django settings
    """
    django_conf_not_found = DjangoConfNotFound

    def __init__(self):
        from django.conf import settings
        import os
        try:
            env = os.environ['DJANGO_SETTINGS_MODULE']
        except KeyError:
            raise DjangoConfNotFound("Django configuration not found")
        self._dj = settings

    def __getitem__(self, k):
        return getattr( self._dj, k )
