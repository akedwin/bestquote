#coding: utf-8
#
#  Default settings
#
from __future__ import unicode_literals

# serializer used
# currently only CBOR is available
SERIALIZER = "CBOR"

# default log level
LOG_LEVEL = "debug"

# catch stdout and redirect it to log
LOG_CATCH_STDOUT = False
LOG_STDOUT_LEVEL = "DEBUG"
# catch stderr and redirect it to log
LOG_CATCH_STERR = False
LOG_STDERR_LEVEL = "ERROR"

# standard logger name
# how change logger name
# from FQ import LOG
# LOG.stetupLogger("my logger name")
LOGGER_NAME = "fq"

# Maximun depth of inter-worker calls until exception will be raised
REQUEST_MAX_DEPTH = 20


# other settings
# TRANSPORT = "gearman"

# address of gearman server
GEARMAN_ADDR = "localhost:4730"


# redis
REDIS_SERVER = "localhost:6379"

# How many seconds async task result will be stored in database
# this setting can be overwritten per task individually by
# result_lifetime parameter
ASYNC_RESULT_LIFETIME = 60 * 60 * 48
