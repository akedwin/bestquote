#coding: utf-8
from __future__ import division, absolute_import, unicode_literals
from .config_sources import *

SERVICE_CONFIG_NAME = "service.conf"


__all__ = ("settings","set_value")



class SettingsProxy(dict):

    def __init__(self):
        self.__confs = []
        self.__intern = {}

    def set_value(self, k, v):
        """
        Set value of settings using existing value type
        """
        k=k.strip().upper().replace(" ","_")
        if k in self:
            typ = type( self[k] )
            # boolean has some special values
            if typ is bool:
                self[k] = v.lower() in ("1", "y", "yes","true")
            else:
                self[k] = typ(v)
        else:
            self[k] = v

    def add_django_config(self):
        try:
            djp = DjangoParser()
        except (ImportError, DjangoParser.django_conf_not_found):
            self.DJANGO_SETTINGS_LOADED = False
            return
        self.__confs.append( djp )
        self.DJANGO_SETTINGS_LOADED = True

    def add_module_config(self, modulename):
        self.__confs.insert(0, ModuleParser(modulename) )

    def add_file_config(self, filename, section):
        scp = SectionConfigParser(filename, section)
        self.__confs.insert(0, scp )
        # service.conf is special file which contain
        # additional data than regular config,
        # we add special handler to it
        if scp.file_loaded:
            self._service_conf_post_load(scp)

    def add_env_config(self):
        self.__confs.insert(0, EnvParser() )

    def add_args_config(self):
        self.__confs.insert(0, ArgumentParser() )

    def __getattr__(self, k):
        # Internal attributes are not served as settings
        if k.startswith("_"):
            raise AttributeError("Settings has no attribute %r" % k)
        # only uppercase keys are possible
        if k.upper()!=k:
            raise AttributeError("Settings are stored in uppercase keys")

        # manual overwritten value
        try:
            return self.__intern[k]
        except KeyError:
            pass

        # check all available config sources
        for cnf in self.__confs:
            try:
                return cnf[k]
            except:
                pass
        return None

    def __setattr__(self,k,v):
        if k.startswith("_") or (k.upper()!=k):
            return dict.__setattr__(self, k, v)
        self.__intern[k]=v

    def _service_conf_post_load(self, cnf):
        """
        After loading service.conf, we should load environment variables
        """
        # copy environment variables
        if cnf.has_section("env"):
            import os
            cnf.casesensitive = False
            for k,v in cnf.items("env"):
                os.environ[k.upper()] = v
            cnf.casesensitive = True
        # special settings
        if cnf.has_section("service"):
            pairs = {
                "name"   : "SERVICE_NAME",
                "module" : "SERVICE_MODULE",
            }
            for n,s in pairs.items():
                try:
                    v = cnf.get("service", n)
                except:
                    continue
                self.__intern[s] = v


settings = SettingsProxy()
settings.add_module_config("FQ.conf.defaults")
settings.add_file_config(SERVICE_CONFIG_NAME,"system")
settings.add_django_config()
settings.add_env_config()
settings.add_args_config()
