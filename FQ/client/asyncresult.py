#coding: utf-8
from __future__ import absolute_import, unicode_literals
from FQ.exceptions import ResultNotReady
from FQ.transport import get_client_transport


class AsyncResult(object):

    def __init__(self, taskid):
        if not type(taskid) is str:
            raise Exception("Invalid task id: %r" % taskid)
        self.__id = taskid
        self.__err = None
        self.__data = None

    def __unicode__(self):
        return unicode( self.__id )

    def __str__(self):
        return self.__id

    def __repr__(self):
        return "<FQResult:%s>" % self.__id

    def __hash__(self):
        return self.__id.__hash__()

    def __get_result(self):
        global transport
        try:
            t = transport
        except:
            transport = get_client_transport()
            t = transport
        self.__err, self.__data = t.get_async_result(self.__id)
        return not self.__err is None

    @property
    def is_ready(self):
        """
        Return True if result is ready, False if not
        """
        if not self.__err is None:
            return True
        return self.__get_result()

    @property
    def result(self):
        """
        Return task result or raise ResultNotReady exception
        if result is not available or task is not processed yet
        """
        if self.__err is None:
            if not self.__get_result():
                raise ResultNotReady("Result <%s> is not available" % self.__id)
        if self.__err:
            raise self.__data
        return self.__data


    @property
    def result_or_none(self):
        """
        Return task result or None if result is not available
        or task is not processed yet
        """
        try:
            return self.result
        except ResultNotReady:
            return None
