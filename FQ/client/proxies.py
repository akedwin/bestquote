#coding: utf-8
from __future__ import division, absolute_import, unicode_literals
#from FQ.conf import settings
from FQ import LOG
from FQ.conf import settings
from FQ.worker.task_proc import get_current_context
from FQ.transport import get_client_transport
from FQ import exceptions
from FQ.client.asyncresult import AsyncResult


__all__ = ("Context", "SyncProxy", "AsyncProxy")


class Context(object):
    """
    Task context
    """
    def __init__(self, initial_data=None, max_depth=None):
        if initial_data is None:
            self.__data = {}
            # maximum request depth
            if max_depth:
                self.__data['depth'] = max_depth
            else:
                self.__data['depth'] = settings.REQUEST_MAX_DEPTH
        elif type(initial_data)==dict:
            self.__data = initial_data

    # dictionary-like access
    def __setitem__(self, k,v):
        self.__data.__setitem__(k,v)
    def __getitem__(self, k):
        return self.__data.__getitem__(k)
    def __delitem__(self, k):
        self.__data.__delitem__(k)
    def __iter__(self):
        return self.__data.__iter__()
    def __len__(self):
        return self.__data.__len__()
    def __in__(self, k):
        return self.__data.__in__(k)
    def get(self, k, *args):
        return self.__data.get(k,*args)
    def keys(self):
        return self.__data.keys()
    def items(self):
        return self.__data.items()
    def __repr__(self):
        return repr(self.__data)

    # data pickling
    def __getstate__(self):
        return self.__data
    def __setstate__(self, state):
        self.__data = state


    # calling task in current context
    @property
    def sync(self):
        return SyncExec(self)

    @property
    def async(self):
        return AsyncExec(self)

    def __enter__(self):
        return self

    def __exit__(self, typ, val, tback):
        pass



class BaseProxy(object):
    """
    Catch calls separated by dots:
    module.submodule.function(...)
    """
    __slots__ = ("_names","_method","_context")

    def __init__(self):
        self._names = []
        self._method = None

    def _my_context(self):
        # current context
        try:
            if self._context:
                return self._context
        except AttributeError:
            pass
        # try to get current context from stack
        self._context = get_current_context()
        if not self._context is None:
            return self._context
        # there is no context, create new one
        self._context = Context()
        return self._context

    def __getattr__(self, itemname):
        if itemname.startswith("_"):
            raise AttributeError("Task names cannot start with underscore character")
        self._names.append(itemname)
        return self

    @staticmethod
    def _call_task_sync(service, task, context, args, kwargs):
        trns = get_client_transport()
        err, data = trns.sync_request( service, task, context, args, kwargs )
        # normal result
        if not err:
            return data
        # exception ready to raise
        if isinstance(data, Exception):
            raise data
        raise exceptions.FQInternalException("Unknown result type")

    @staticmethod
    def _call_task_async(service, task, context, args, kwargs):
        trns = get_client_transport()
        task_id = trns.async_request( service, task, context, args, kwargs )
        return AsyncResult(task_id)

class SyncProxy(BaseProxy):

    def __call__(self, *args, **kwargs):
        ctx = self._my_context()
        # check inter-workers call depth
        if ctx['depth'] == 0:
            raise exceptions.MaximumDepthLevelReached("Maximum level of requests depth reached")

        # decrease maximum call depth
        ctx['depth'] -= 1
        try:
            return self._call_task_sync(
                self._names[0],
                ".".join( self._names[1:] ),
                ctx,
                args, kwargs
            )
        finally:
            # always return depth level
            self._context['depth'] += 1


class AsyncProxy(BaseProxy):

    def __call__(self, *args, **kwargs):
        ctx = self._my_context()
        # check inter-workers call depth
        if ctx['depth'] == 0:
            raise exceptions.MaximumDepthLevelReached("Maximum level of requests depth reached")

        # decrease maximum call depth
        ctx['depth'] -= 1
        try:
            return self._call_task_async(
                self._names[0],
                ".".join( self._names[1:] ),
                ctx,
                args, kwargs
            )
        finally:
            # always return depth level
            self._context['depth'] += 1




class _ExecBase(object):
    """
    Creates first level Proxy object and makes new context
    """

    def __init__(self, context=None):
        """
        Context parameter is used internally by Context object.
        """
        self.__ctx = context

    def _create_proxy(self):
        """
        This function should return instance of object
        derived from BaseProxy object
        """
        raise NotImplementedError

    def __getattr__(self, itemname):
        """
        create proxy instance and return it back as result
        """
        if itemname.startswith("_"):
            raise AttributeError("No attribute %s in %r" % (itemname,self) )
        proxy = self._create_proxy()
        proxy._context = self.__ctx
        proxy._names.append(itemname)
        return proxy


class SyncExec(_ExecBase):
    def _create_proxy(self):
        return SyncProxy()

class AsyncExec(_ExecBase):
    def _create_proxy(self):
        return AsyncProxy()
