#coding: utf-8
from __future__ import unicode_literals, absolute_import, print_function
from .proxies import Context, SyncExec, AsyncExec
from .asyncresult import AsyncResult

__all__ = ("sync", "async", "Context")

sync = SyncExec()
async = AsyncExec()

## remove unneccessary imports
del SyncExec, AsyncExec
