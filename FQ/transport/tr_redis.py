#coding: utf-8
"""
Transport backend used with redis.
Require python redis module.
pip install redis
"""
from __future__ import unicode_literals, absolute_import
from .base import BaseWorkerTransport, BaseClientTransport
from FQ.lib import SingletonCreator
from FQ.conf import settings
from FQ.worker.task_proc import TASKS
from FQ.lib import messages as msg
from FQ.lib.serializer import Serializer
from FQ import exceptions
from FQ import LOG
from time import sleep

__all__ = ("TransportRedisWorker","TransportRedisClient")


class RedisConnector( object ):

    __metaclass__ = SingletonCreator

    def __init__(self):
        #try:
        self.addr, self.port = settings.REDIS_SERVER.split(":",1)
        #except:
        #    raise Exception("REDIS_ADDRESS should be stored as 'address_or_ip:port' format")
        self._redis = None

    def connect(self):
        global serializer
        serializer = Serializer()
        if self._redis is None:
            LOG.info("Connecting to redis server: %s" % settings.REDIS_SERVER)
            self._redis = redis.StrictRedis(
                host=self.addr,
                port=int(self.port) )
        return self._redis

    def close(self):
        self._redis.close()


def _result_name(service, task, id):
    return b"FQ_RES|"+service+b"|"+task+b"|"+str( id )

def _result_channel(service):
    return b"FQ_" + service

def _task_queue_name(service):
    return b"FQ_" + service + b"_tasks"


class TransportRedisWorker( BaseWorkerTransport ):

    __metaclass__ = SingletonCreator

    def __init__(self):
        # import dependencies
        global redis
        import redis
        self._conn = RedisConnector()
        self.__online = True
        BaseWorkerTransport.__init__(self)

    def register_tasks(self):
        # Redis doesn's require any registering,
        # job dispathing is done locally in worker.
        # Only simple logging is done here
        for name,task in TASKS.items():
            LOG.info("Registered task: %s" % name)
            del task[ b'task_external' ]
        self.tasks_register_summary( len(TASKS) )

    def setup(self, name):
        BaseWorkerTransport.setup(self, name)
        self.channel_name = _result_channel(name)

    def connect(self):
        if self.service_name is None:
            raise Exception("Transport is not yet initialised by setup method.")
        # connect and register tasks
        self.R = self._conn.connect()
        BaseWorkerTransport.connect(self)
        self.register_tasks()


    def run(self):
        while self.__online:
            msg = self.R.brpop( [_task_queue_name(self.service_name)] )
            if msg is None:
                sleep(0.1)
                continue
            self.__process_task( msg[1] )


    def __process_task(self, msg):
        msg = serializer.deserialize(msg)
        try:
            priv = msg[ b'_redis_' ]
            taskname, id, is_sync = priv['task'], priv['id'], priv['sync']
            del msg[ b'_redis_' ]
        except:
            return

        # run task
        try:
            task = TASKS[ priv[b'task'] ]
            func = task[b'task_internal']
        except KeyError:
            # task not found will be raised on client side
            func = None
            LOG.debug("Rejected request for not existing task in worker: %s" % priv[b'task'])

        try:
            if func is None:
                result = self.task_not_found_result( priv[b'task'] )
            else:
                result = func( msg )

            # if task is asynchronous,
            # and ignore_result flag is set
            # then don't store anything on exit
            if not is_sync:
                if task[b'ignore_result']:
                    return

            # store result in DB
            result = serializer.serialize( result )
            result_id = _result_name( self.service_name, taskname, id )
            self.R.set(result_id, result)

            if is_sync:
                # 5 seconds for sync result should be enough to
                # retrieve result by client
                self.R.expire(result_id, 5)
            else:
                # for async tasks we use per-task
                # result lifetime in seconds
                self.R.expire(result_id, task[b'result_lifetime'] )
        finally:
            if not is_sync: return
            # send signal when result is ready
            signal = taskname+b"|"+str( id )
            self.R.publish(self.channel_name, signal )



    # message in / out processing

    @staticmethod
    def process_incoming_job(data):
        return data

    @staticmethod
    def process_job_result(data):
        return data







class TransportRedisClient( BaseClientTransport ):

    __metaclass__ = SingletonCreator

    def __init__(self):
        global redis
        import redis
        self._conn = RedisConnector()
        BaseClientTransport.__init__(self)

    def connect(self):
        self.R = self._conn.connect()
        BaseClientTransport.connect(self)
        self.RESQ = self.R.pubsub( ignore_subscribe_messages=True )

    def process_request(self, service, method, message, is_sync):
        # task counter name
        cname = "FQ_"+service+"_id"
        taskid = self.R.incr( cname )
        # extras
        message['_redis_'] = {
            b'task' : str(method),
            b'id' : taskid,
            b'sync' : is_sync
        }
        return (taskid, is_sync)

    def __result_loop(self, channel, signal):
        """
        This is result loop which uses pub/sub channel
        to send and receive message about task execution.
        Unfortunately this method in multithreaded environment
        result in hanging thread forever. Result are not received
        and task is not properly terminated."""
        self.RESQ.subscribe(channel)
        for msg in self.RESQ.listen():
            if msg['data']==signal:
                break
        self.RESQ.unsubscribe(channel)


    def send_sync_request(self, service, method, payload, extras):
        method = bytes(method)
        # add task to list
        self.R.lpush( _task_queue_name( service ), payload)
        # wait for signal when task is finished
        signal = method+"|"+str( extras[0] )
        #self.__result_loop( _result_channel(service), signal)
        resultname = _result_name( service, method, extras[0] )

        # primitive waiting loop implementation
        waittime = 0.0001
        sleep(waittime*2)
        result = self.R.get(resultname)
        while result is None:
            if waittime<0.5:
                waittime*=2
            sleep(waittime)
            result = self.R.get(resultname)
        # retrieve result
        result = self.R.get(resultname)
        self.R.delete(resultname)
        # TODO:
        # if sending or serializing result fail on worker side
        # there will be no result, so we should do something
        # special to avoid client raising deserializatin errors
        return result


    def send_async_request(self, service, method, payload, extras):
        method = bytes(method)
        # add task to list
        self.R.lpush( _task_queue_name( service ), payload)
        # result is just keyname
        resultname = _result_name( service, method, extras[0] )
        return resultname


    def _get_async_result(self, task_id):
        result = self.R.get(task_id)
        if not result is None:
            self.R.delete(task_id)
        return result