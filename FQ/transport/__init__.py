#coding: utf-8
from __future__ import unicode_literals, absolute_import
from FQ.conf import settings

class UnknownTransportError(Exception):
    def __init__(self, name):
        self.message = "Unknown transport: %r" % name


def load_transport(name):
    """
    Import and init transport class
    """
    global __transport_wrkr, __transport_clnt
    if name=="gearman":
        # gearman transport layer, using python gearman module
        from .tr_gearman import TransportGearmanWorker, TransportGearmanClient
        __transport_wrkr = TransportGearmanWorker()
        __transport_clnt = TransportGearmanClient()
        return

    if name=="pygear":
        # gearman transport layer, using python gearman module
        from .tr_pygear import TransportPygearWorker, TransportPygearClient
        __transport_wrkr = TransportPygearWorker()
        __transport_clnt = TransportPygearClient()
        return

    if name=="redis":
        # gearman transport layer, using python gearman module
        from .tr_redis import TransportRedisWorker, TransportRedisClient
        __transport_wrkr = TransportRedisWorker()
        __transport_clnt = TransportRedisClient()
        return

    raise UnknownTransportError(name)


def available_transports():
    """
    Return available transport list
    """
    return ("gearman","pygear", "redis")



def get_worker_transport():
    global __transport_wrkr
    try:
        return __transport_wrkr
    except:
        pass
    load_transport( settings.TRANSPORT )
    return __transport_wrkr


def get_client_transport():
    global __transport_clnt
    try:
        return __transport_clnt
    except:
        pass
    load_transport( settings.TRANSPORT )
    return __transport_clnt

