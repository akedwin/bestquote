#coding: utf-8
"""
Transport backend used with gearman.
Require python gearman module.
pip install gearman
"""
from __future__ import unicode_literals, absolute_import
from .base import BaseWorkerTransport, BaseClientTransport
from FQ.lib import SingletonCreator
from FQ.conf import settings
from FQ.worker.task_proc import TASKS
from FQ.lib import messages as msg
from FQ import exceptions
from FQ import LOG


class TransportGearmanWorker( BaseWorkerTransport ):

    __metaclass__ = SingletonCreator

    def __init__(self):
        BaseWorkerTransport.__init__(self)
        # import dependencies
        global gearman
        import gearman

    def register_tasks(self):
        if self.service_name is None:
            raise Exception("Transport is not yet initialised by setup method.")
        for name,task in TASKS.items():
            taskname = bytes(self.service_name+"."+name)
            self.GW.register_task( taskname, task[ b'task_external' ] )
            LOG.info("Registered task: %s" % name)
            # remove unused functions
            del task[ b'task_external' ]
            del task[ b'task_internal' ]
        self.tasks_register_summary( len(TASKS) )

    def connect(self):
        LOG.info("Connecting to gearman server: %s" % settings.GEARMAN_ADDR)
        BaseWorkerTransport.connect(self)
        addr = str(settings.GEARMAN_ADDR)
        self.GW = gearman.GearmanWorker([addr])
        self.register_tasks()

    def run(self):
        self.GW.work()

    # message in / out processing

    @staticmethod
    def process_incoming_job(worker, job):
        return job.data

    @staticmethod
    def process_job_result(data):
        return data



class TransportGearmanClient( BaseClientTransport ):

    __metaclass__ = SingletonCreator

    def __init__(self):
        BaseClientTransport.__init__(self)
        # import dependencies
        global gearman
        import gearman

    def connect(self):
        BaseClientTransport.connect(self)
        addr = str(settings.GEARMAN_ADDR)
        self.GC = gearman.GearmanClient([addr])


    @staticmethod
    def process_response(data):
        return msg.decode_result_message( data.result )


    def send_sync_request(self, service, method, payload, extras):
        jobname = bytes(service+"."+method)
        result = self.GC.submit_job(jobname, payload, background=False)

        # task timeout
        if result.timed_out:
            return msg.make_internal_error_message( "Task time out" )

        # task is not completed
        if result.state!=b"COMPLETE":
            return msg.make_internal_error_message(
                "Task returned status %s" % result.state )

        return result.result


    # async task handling


    def get_job_status_handle(self, handle, poll_timeout=None):
        job = gearman.job.GearmanJob(self.GC.connection_list[0], handle, None, None, None)
        job_request = gearman.job.GearmanJobRequest(job)
        job_request.state = 'CREATED'
        return self.GC.get_job_status(job_request, poll_timeout)


    def send_async_request(self, service, method, payload):
        jobname = bytes(service+"."+method)
        result = self.GC.submit_job(jobname, payload, background=True)
        taskid = result.job.handle
        #del result

        print self.get_job_status_handle(taskid)


        #from gearman.client_handler.
        #print check_request_status(result)

        #print type(result)
        #print result
        #print
        #print result.job.unique
        #print result.complete
        #print result.state
        #print ">>",result.result

        #print self.GC.get_job_status(result)

        #print self.GC.get_job_status(result)
        #import sys
        #sys.stdout.flush()
        from time import sleep
        sleep(1)
        print self.get_job_status_handle(taskid)
        #print check_request_status(result)
        #print

        #print self.GC.get_job_status(result)
        #print result.result
        #print result.status
        # submitted_job_request


        # task timeout
        #if result.timed_out:
        #    return msg.make_internal_error_message( "Task time out" )

        # task is not completed
        #if result.state!=b"COMPLETE":
        #    return msg.make_internal_error_message(
        #        "Task returned status %s" % result.state )

        return None #result.result
