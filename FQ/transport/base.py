#!/usr/bin/env python
#coding: utf-8
"""

    Worker data flow:
    -----------------
    Transport - process_request
        Queue - deserialize
    Transport - preprocess_message
        Queue - run task
    Transport - result_postprocess
        Queue - serialize
    Transport - send

    Client data flow
    ----------------
    Queue - build message
    Transport - preprocess_message
    Queue - serialize
    Transport - Send and receive result
    Queue - desierialize
    Transport - preprocess_result
    Queue - return result

"""
from __future__ import unicode_literals
from FQ.lib import messages as msg
from FQ.lib import serializer as ser
from FQ.lib.typeconverter import encode_types
from FQ import exceptions
from FQ import LOG


class BaseWorkerTransport(object):

    def __init__(self):
        self.service_name = None

    def setup(self, service):
        """
        step 1.
        Initialize transport parameters.
        """
        self.service_name = service

    def register_tasks(self):
        """
        step 2.
        register all tasks

        This method should be called by transport implementation by self,
        because some backend may require another order of initialisation
        (eg. registering tasks after connection, not before).
        """
        pass

    def tasks_register_summary(self, howmany):
        if howmany>0:
            LOG.info("Number of registered tasks: %i" % howmany)
        else:
            LOG.warning("No tasks registered, probably module is not loaded.")

    def connect(self):
        """
        step 3.
        connect to server
        """
        ser.init_serializer()

    def run(self):
        """
        step 4.
        start working
        """
        pass


    # worker task preprocessors

    def process_request(self, *args, **kwargs):
        raise NotImplemented()

    def process_incoming_job(self):
        """
        This function should process incoming data from worker and return enclosed data in message:
        context - job context
        args - positional parameters
        kwargs - keyword parameters
        """
        raise NotImplemented()

    def process_result(self):
        """
        Process task result before sending back to client
        """
        raise NotImplemented()

    # helper methods used by some transports
    @staticmethod
    def task_not_found_result(taskname):
        """
        Build response with FQTaskNotFound exception.
        It is used by transports without own joob dispatchers.
        """
        return msg.make_internal_error_message( "Task '%s' not found" % taskname )


class BaseClientTransport( object ):

    def __init__(self):
        self._connected = False

    def connect(self):
        ser.init_serializer()
        """
        When client is initialised this method will be invoked once per client instance.
        """
        pass

    def disconnect(self):
        """
        This method is called once when client finishes working.
        """
        pass

    def close():
        """
        close client
        """
        self.disconnect()


    def send_sync_request(self, service, method, payload):
        """
        transport layer function for sending tasks
        """
        raise NotImplemented()

    def send_async_request(self, service, method, payload):
        """
        transport layer function for asynchronous task calling
        """
        raise NotImplemented()


    def process_request(self, service, method, payload, is_sync):
        """
        transport layer can add internally used fields to message here
        """
        pass


    def sync_request(self, service, method, ctx, args, kwargs):
        """
        Perform synchronous request
            service - service name (worker name)
            method - task name
            ctx - context
            args, kwargs - parameters for task
        """
        if not self._connected: self.connect()
        # prepare message
        if ctx.get('extended_params', False):
            # prepare extended parameters before serialization
            args = encode_types(args)
            kwargs = encode_types(kwargs)
        payload = msg.make_job_request_message(
            ctx.__getstate__(),
            args, kwargs)
        extras = self.process_request(service, method, payload, True)
        # serialize
        payload = ser.serializer.serialize(payload)
        # send request
        result = self.send_sync_request(service, method, payload, extras)
        # decode message
        result = ser.serializer.deserialize(result)
        err, data = msg.decode_result_message(result)
        return err, data


    def async_request(self, service, method, ctx, args, kwargs):
        if not self._connected: self.connect()
        # prepare message
        if ctx.get('extended_types', False):
            # prepare extended parameters before serialization
            args = encode_types(args)
            kwargs = encode_types(kwargs)
        payload = msg.make_job_request_message(
            ctx.__getstate__(),
            args, kwargs)
        extras = self.process_request(service, method, payload, False)
        # serialize
        payload = ser.serializer.serialize(payload)
        # send request
        result = self.send_async_request(service, method, payload, extras)
        return result
        #result = ser.serializer.deserialize(result)
        # decode message
        #err, data = msg.decode_result_message(result)
        #return err, data

    # async result

    def _get_async_result(self, taskid):
        raise NotImplementedError("async result is not implemented")

    def get_async_result(self, taskid):
        payload = self._get_async_result(taskid)
        if payload is None:
            return None, None
        # deserialize
        payload = ser.serializer.deserialize(payload)
        err, data = msg.decode_result_message(payload)
        return err,data
