#coding: utf-8
from __future__ import unicode_literals
from .task_proc import add_task
from FQ.transport import get_worker_transport
from FQ.conf import settings


__all__ = ("task",)


class task(object):

    def __init__(self, name=None, ignore_result=True, result_lifetime=None, extended_types=False):
        self.name = name
        self.ext_types = extended_types
        # asynchronous task result lifetime
        if result_lifetime is None:
            result_lifetime = int( settings.ASYNC_RESULT_LIFETIME )
        self.result_lifetime = result_lifetime
        # ignore result
        self.ignore_result = ignore_result

    def __call__(self, func):
        # function name
        if self.name is None:
            name = func.__name__
        else:
            name = self.name

        # register locally
        task = add_task(
                name,
                func,
                self.ignore_result,
                self.result_lifetime,
                self.ext_types,
        )

        # return func instead of registered task
        return func



