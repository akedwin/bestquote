#coding: utf-8
"""
Tasks management functions
"""
from __future__ import unicode_literals
from FQ.transport import get_worker_transport
from FQ.worker.worker import get_current_worker_name
from FQ.lib import messages as msg
from FQ.lib import serializer as ser
from FQ.lib.typeconverter import encode_types, decode_types
from FQ import LOG
import inspect
import weakref
import traceback

__all__ = ("get_current_context", "add_task")

TASKS = {}
CONTEXT_STACK = []

def get_current_context():
    # return current context stored on context stack
    global CONTEXT_STACK
    if len(CONTEXT_STACK)>0:
        ctx = CONTEXT_STACK[-1]
        return weakref.proxy(ctx)
    return None


def add_task(name, func, ignore_result, result_lifetime, ext_types):
    """
    Register worker tasks and return functions prepared to be tasks
    name - exposed task name
    func - function called as task
    result_lifetime - time in seconds for async task until result will be ignored
    ext_types - if True, task result can contain extended data types (Decimals, datetime, etc...)
    """
    global TASKS
    from FQ.client.proxies import Context

    # check if func is real function or callable class instance
    if inspect.isclass(func) or not callable(func):
        raise Exception("Only functions or callable classes can be registered as tasks")
    # check if task is available
    if name in TASKS:
        raise Exception("Task %r is already registered")

    # check task name
    if len(name)<1:
        raise Exception("Task name is empty")
    for c in name.lower():
        if c not in "abcdefghijklmnopqrstuvwxyz0123456789_.":
            raise Exception( "Task names can contain only letters, digits, underscores and dots. Name %r is prohibited." % name )
    if c.startswith("_"):
        raise Exception( "Task names cannot start with underscore" % name )

    # check result_lifetime parameter
    if (type( result_lifetime ) is not int) or (result_lifetime<1):
        raise Exception("result_lifetime parameter can be only positive int value")

    # check ignore_result
    if type(ignore_result) is not bool:
        raise Exception("ignore_result parameter can be only True or Fasle")

    # transport layer is used to prepare tasks parameters
    transport = get_worker_transport()


    # task caller for transports dispatching jobs internally
    def task_caller_extern(*args, **kwargs):
        try:
            message = transport.process_incoming_job(*args, **kwargs)
        except Exception as e:
            # error processing incoming message
            m = "Failed processing incoming data"
            LOG.exception(m)
            result = msg.make_internal_error_message(m)
            result = ser.serializer.serialize(result)
            result = transport.process_result(result)
            request_finished()
            return result

        # deserialize message
        message = ser.serializer.deserialize(message)

        try:
            # decode deserialized message
            context, args, kwargs = msg.decode_request_message(message)
            context = Context(context)
        except:
            m = "Incoming message is not valid"
            LOG.exception(m)
            result = msg.make_internal_error_message(m)
            result = ser.serializer.serialize(result)
            result = transport.process_result(result)
            request_finished()
            return result

        # call task
        # result is ready to send message (error or regular result)
        result = process_task(name, context, args, kwargs)

        # serialize data
        result = ser.serializer.serialize(result)
        # process result
        result = transport.process_job_result(result)
        request_finished()
        return result



    # task caller for transports without job dispatchers
    def task_caller_intern(message):
        try:
            # decode deserialized message
            context, args, kwargs = msg.decode_request_message(message)
            context = Context(context)
        except:
            m = "Incoming message is not valid"
            LOG.exception(m)
            result = msg.make_internal_error_message(m)
            result = ser.serializer.serialize(result)
            result = transport.process_result(result)
            request_finished()
            return result

        result = process_task(name, context, args, kwargs)
        request_finished()
        return result


    # build task structure
    task = {}
    task[ b"func" ] = func
    task[ b"task_external" ] = task_caller_extern
    task[ b"task_internal" ] = task_caller_intern
    task[ b"result_lifetime" ] = result_lifetime
    task[ b"ignore_result" ] = ignore_result
    task[ b"ext_types"] = ext_types
    # stats for task
    task[ b"calls_ok" ] = 0
    task[ b"calls_err" ] = 0
    TASKS[str(name)] = task

    return task


def process_task(tname, context, args, kwargs):
    """
    Execute registered worker task.
    """
    global TASKS, CONTEXT_STACK
    task = TASKS[tname]
    func = task[ b'func' ]

    LOG.debug("Executing task [%s]" % tname)

    # prepare extended parameters
    if context.get("extended_params",False):
        print "PARAMS"
        args = decode_types(args)
        kwargs = decode_types(kwargs)
        print args
        print kwargs
        del context["extended_params"]

    CONTEXT_STACK.append(context)
    try:
        # execute task
        result = func(*args, **kwargs)
        task['calls_ok'] += 1
    except Exception as e:
        task['calls_err'] += 1
        LOG.exception("Task raised exception")
        tback = traceback.format_exc()
        return msg.make_error_message(e, tback, get_current_worker_name() )

    finally:
        try:
            CONTEXT_STACK.pop()
        except IndexError:
            pass
            # TODO: Raise serious alert
            # This exception means that multithreading or concurrency is used, but worker
            # is currently not prepared to work with it. Context stack is messed up.

    # additional result encoding
    flags = None
    if task[b'ext_types']:
        result = encode_types(result)
        flags = ("extypes",)

    return msg.make_result_message( result, flags )


_finish_callbacks = []

def request_finished():
    #self.DJANGO_SETTINGS_LOADED
    #from django.core.signals.request_finished
    global _finish_callbacks
    for c in _finish_callbacks:
        c()


def _add_callback_on_finish(func):
    global _finish_callbacks
    _finish_callbacks.append(func)



'''
Copied from kasaya to implement in future:

def before_worker_start(func):
    """
    Register function which will be executed before worker start listening for tasks.
    Registered function will be called with single parameter cointaining worker ID.

    This function can be used for worker initialisation tasks, like connecting to database.

    Example:

    .. code-block:: python

        from kasaya import before_worker_start

        @before_worker_start
        def initialize(ID):
            print "My worker id is:", ID

    """
    _func_only(func)
    worker_methods_db.register_before_start(func)
    return func

def after_worker_stop(func):
    """
    Register function to be executed after worker is stopped.
    Function will not receive any parameters.

    Example:

    .. code-block:: python

        from kasaya import after_worker_stop

        @after_worker_start
        def worker_stop():
            print "stopped working"
            close_db_connection()
    """
    _func_only(func)
    worker_methods_db.register_after_stop(func)
    return func
'''
