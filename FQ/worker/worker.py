#!/usr/bin/env python
#coding: utf-8
from __future__ import unicode_literals
from FQ.conf import settings
from FQ.transport import get_worker_transport, UnknownTransportError
from FQ import LOG
import os,sys

__all__ = ("Worker",)

class Worker(object):

    def __init__(self, worker_name=None, catch_streams=True):
        # worker name is most important thing
        if worker_name is None:
            worker_name = settings.SERVICE_NAME
            if worker_name is None:
                print "Unable to determine worker name.",
                print "Use 'name' setting in service.conf file in 'service' section,",
                print "or set it manually initialising Worker class with parameter 'worker_name'."
                sys.exit(1)
        # catching streams to log
        if catch_streams:
            settings.LOG_CATCH_STDOUT = True
            settings.LOG_CATCH_STDOUT = True

        LOG.stetupLogger(worker_name)
        LOG.info("Initializing worker: %s" % worker_name)
        try:
            self.transport = get_worker_transport()
        except UnknownTransportError as e:
            LOG.critical(e.message)
            sys.exit(1)
        self.transport.setup(worker_name)
        self.worker_name = worker_name
        self.autoload_module()

    def _prepare_django(self):
        """
        Try to initialize django ORM in Django 1.7
        if django settings are loaded
        """
        if settings.DJANGO_SETTINGS_LOADED:
            from FQ.lib.django_compat import prepare_django
            prepare_django()


    def autoload_module(self):
        modname = settings.SERVICE_MODULE
        if modname is None:
            return
        cwd = os.getcwd()
        if not cwd in sys.path:
            sys.path.append(cwd)
        try:
            __import__(modname)
        except ImportError:
            LOG.critical("Failed loading module: %s" % modname)
            sys.exit(1)
        LOG.debug("Succesfully loaded module: %s" % modname)

    def start(self):
        self._prepare_django()
        global CURRENT_SERVICE_NAME
        self.transport.connect()
        LOG.info("Start listening for tasks")
        CURRENT_SERVICE_NAME = self.worker_name
        self.transport.run()

def get_current_worker_name():
    global CURRENT_SERVICE_NAME
    return CURRENT_SERVICE_NAME
